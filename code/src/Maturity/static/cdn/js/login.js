/**
 * Created by dawxy on 2017/5/9.
 */

$(document).ready(function(){
    //检查是否已经登录
    if (IsUserLogined()){
        BacktoUrl();
        return;
    }
    var _GET = GET();
    //回车触发事件
    document.onkeydown = function (e) {
        var theEvent = window.event || e;
        var code = theEvent.keyCode || theEvent.which;
        if (code == 13) {
            $("#login").click();
        }
    }
    //$.support.transition = true
    $('[data-toggle="popover"]').popover()
    $("#login").click(function() {
        JobNumber = $('#JobNumber').val()
        if (JobNumber == ""){
            mAlert("请输入工号")
            return
        }
        Password = $('#Password').val()
        if (Password == ""){
            mAlert("请输入密码")
            return
        }
        remember = $("#remember").is( ":checked" );
        //登录请求
        mAjax(
            "User/Login/",
            {JobNumber:JobNumber,Password:Password},
            function(info){
                if (info.status){
                    if(info.data.Token == ""){//为初始密码,跳转到修改密码
                        $.cookie("JobNumber", JobNumber, {path:"/"});
                        $.cookie("OldPassword", Password, {path:"/"});
                        window.location.href = "uppassword.html?backurl="+_GET["backurl"];
                        return;
                    }
                    day = 0
                    if (remember){
                        day= 365
                        $.cookie("Token", info.data.Token,  { expires: day,path:"/"});
                    }else{
                        $.cookie("Token", info.data.Token, {path:"/"});
                    }
                    //获取所有权限列表
                    mAjax(
                        "Power/CheckAllowPower/",
                        {Token:$.cookie("Token")},
                        function(info){
                            if (day != 0){
                                $.cookie("powers",  JSON.stringify(info.data), {expires: day, path:"/"});
                            }else{
                                $.cookie("powers",  JSON.stringify(info.data), {path:"/"});
                            }
                            BacktoUrl();
                        }
                    );
                }else{
                    mAlert(info.message);
                }
            }
        );
    });
});

function BacktoUrl() {
    tourl = "/";
    _GET = GET();
    //console.log(GET["backurl"]);
    if (_GET["backurl"] != undefined){
        tourl = _GET["backurl"];
    }
    window.location.href = decodeURIComponent(tourl);
}
