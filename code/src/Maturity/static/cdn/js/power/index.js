/**
 * Created by dawxy on 2017/5/12.
 */

var powerindex = undefined;
$(document).ready(function(){
    if (CheckAllowPower(["给用户添加角色"]) && CheckAllowPower(["查询角色以及对应权限列表"])){
        $("#addpower").show();
    }
    powerindex = newpowerindex();
    powerindex.init();
    bop = {
        id:$('#mytable'),
        url:"Power/QueryPowerList/",
        columns:[{
            field:"JobNumber",
            title: '工号',
        },{
            field: 'UserName',
            title: '姓名'
        },{
            field: 'RoleName',
            title: '角色名称'
        },{
            field: 'CreatUser',
            title: '创建人'
        }, {
            field: 'CreatTime',
            title: '创建时间',
        },
        ]
    };
    if (CheckAllowPower(["删除用户的某个角色"])){
        bop.columns.push({
            field: 'Id',
            title: '删除',
            formatter:function(value, row, index){
                return "<button onclick='powerindex.Del("+value+")' class='btn btn-danger'>删除</button>";
            }
        });
    }
    loadBootstrapTable(bop);
});

var newpowerindex = function() {
    var o = new Object();
    var AutoUpdate = undefined;
    o.init = function () {
        AutoUpdate = NewAutoUpdate('AddPowerForm');
        AutoUpdate.UpdateUrl = "Power/AddUserRole/";
        AutoUpdate.messageBox = $('#AddPowermessageBox');
        AutoUpdate.data = {
            Token: $.cookie("Token"),
        };
        $('#AddPowerDialog').on('hidden.bs.modal', function (e) {//隐藏后重置
            AutoUpdate.reset();
            $('#mytable').bootstrapTable('refresh');//刷新table
        });

    }

    //添加对话框
    o.AddPower = function (id) {
        $('#AddPowerDialog').modal('show');
        loadSelect2({
            selectId:$("#UserId"),
            placeholder:"请选择一个用户(可按照工号或者姓名搜索)",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#RoleId"),
            placeholder:"请选择一个角色(可按照角色名称搜索)",
            url: "Role/QueryRoleList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
    }
    //确认添加权限
    o.OnAddPowerClick = function (){
        AutoUpdate.UpdateData();
    }
    var delId = undefined;
    //点击弹出删除部门对话框
    o.Del = function (id) {
        delId = id;
        $('#DelDialog').modal('show');
    }
    //删除角色
    o.OnDelClick = function () {
        mAjax(
            "Power/DelUserRole/",
            {Token: $.cookie("Token"),UserRoleId:delId},
            function(info){
                if (!info.status){
                    mAlert(info.message);
                }
                $('#mytable').bootstrapTable('refresh');//刷新table
            }

        );
    }

    return o;
};