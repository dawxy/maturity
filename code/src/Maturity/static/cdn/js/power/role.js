/**
 * Created by dawxy on 2017/5/12.
 */
var role = undefined;
$(document).ready(function(){
    if (CheckAllowPower(["添加角色以及对应的权限"])){
        $("#addrole").show();
    }
    role = newrole();
    role.init();
    var canUpdate = CheckAllowPower(["添加角色以及对应的权限"]) && CheckAllowPower(["删除角色以及对应的权限"])
    bop = {
        id:$('#mytable'),
        url:"Role/QueryRoleList/",
        columns:[{
            field: 'Name',
            title: '角色名称<i class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="点击可管理角色具体权限"></i>',
            formatter:function(value, row, index){
                if (!canUpdate){
                    return value;
                }
                return "<a href='rolepower.html?id="+row.Id+"&name="+value+"'>"+value+"</a>";
            }
        },
        ]
    };
    if (CheckAllowPower(["修改角色信息"])){
        bop.columns.push({
            field: 'Id',
            title: '修改',
            formatter:function(value, row, index){
                return "<button onclick='role.Update("+value+")' class='btn btn-primary'>修改</button>";
            }
        });
    }
    if (CheckAllowPower(["删除角色以及对应的权限"])){
        bop.columns.push({
            field: 'Id',
            title: '删除',
            formatter:function(value, row, index){
                return "<button onclick='role.Del("+value+")' class='btn btn-danger'>删除</button>";
            }
        });
    }
    loadBootstrapTable(bop);
});

var newrole = function() {
    var o = new Object();
    var AutoUpdate = undefined;
    var UpdateAutoUpdate = undefined;
    o.init = function () {
        AutoUpdate = NewAutoUpdate('AddForm');
        AutoUpdate.UpdateUrl = "Role/AddRole/";
        AutoUpdate.messageBox = $('#AddmessageBox');
        AutoUpdate.data = {
            Token: $.cookie("Token"),
        };
        $('#AddDialog').on('hidden.bs.modal', function (e) {//隐藏后重置
            AutoUpdate.reset();
            $('#mytable').bootstrapTable('refresh');//刷新table
        });

        UpdateAutoUpdate = NewAutoUpdate('UpdateForm');
        UpdateAutoUpdate.GetUrl = "Role/QueryRoleInfo/";
        UpdateAutoUpdate.UpdateUrl = "Role/UpdateRole/";
        UpdateAutoUpdate.messageBox = $('#UpdatemessageBox');
        UpdateAutoUpdate.data = {
            Token: $.cookie("Token"),
        };
        $('#UpdateDialog').on('hidden.bs.modal', function (e) {//隐藏后重置
            UpdateAutoUpdate.reset();
            $('#mytable').bootstrapTable('refresh');//刷新table
        });
    }

    //添加对话框
    o.Add = function (id) {
        $('#AddDialog').modal('show');
    }
    //确认添加权限
    o.OnAddClick = function (){
        AutoUpdate.UpdateData();
    }
    var delId = undefined;
    //点击弹出删除部门对话框
    o.Del = function (id) {
        delId = id;
        $('#DelDialog').modal('show');
    }
    //删除角色
    o.OnDelClick = function () {
        mAjax(
            "Role/DelRole/",
            {Token: $.cookie("Token"),RoleId:delId},
            function(info){
                if (!info.status){
                    mAlert(info.message);
                }
                $('#mytable').bootstrapTable('refresh');//刷新table
            }

        );
    }

    //点击弹出修改对话框
    o.Update = function (id) {
        UpdateAutoUpdate.data.RoleId = id;
        $('#UpdateDialog').modal('show');
        UpdateAutoUpdate.GetData();
    }
    //点击确认修改
    o.OnUpdate = function () {
        UpdateAutoUpdate.UpdateData();
    }

    return o;
};