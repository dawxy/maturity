/**
 * Created by dawxy on 2017/5/12.
 */

var rolepower = undefined;
var RoleId = undefined;
var name = undefined;
$(document).ready(function(){
    _GET = GET();
    RoleId = parseInt(_GET["id"],10);
    if (RoleId == undefined){
        mAlert("缺少参数id!");
    }
    //设置h小标题
    $('#h_title').text("["+decodeURI(_GET["name"])+"]权限管理");
    rolepower = newrolepower();
    rolepower.init();
    loadBootstrapTable({
        id:$('#mytable'),
        url:"Role/QueryRoleAllPower/",
        data: {RoleId:RoleId},
        columns:[{
            field: 'Name',
            title: '权限名称',
            formatter:function(value, row, index){
                return "<a href='rolepower.html?id="+row.Id+"'>"+value+"</a>";
            }
        },{
            field: 'Id',
            title: '删除',
            formatter:function(value, row, index){
                return "<button onclick='rolepower.Del("+value+")' class='btn btn-danger'>删除</button>";
            }
        },
        ]
    });

    loadBootstrapTable({
        id:$('#mytable2'),
        url:"Role/QueryRoleNotHasPower/",
        data: {RoleId:parseInt(RoleId, 10)},
        columns:[{
            field: 'Name',
            title: '权限名称',
            formatter:function(value, row, index){
                return "<a href='rolepower.html?id="+row.Id+"'>"+value+"</a>";
            }
        },{
            field: 'Id',
            title: '添加',
            formatter:function(value, row, index){
                return "<button onclick='rolepower.Add("+value+")' class='btn btn-primary'>添加</button>";
            }
        },
        ]
    });
});

var newrolepower = function() {
    var o = new Object();
    o.init = function () {

    }

    //添加对话框
    o.Add = function (id) {
        mAjax(
            "Role/AddPower/",
            {Token: $.cookie("Token"),RoleId:RoleId, PowerId:id},
            function(info){
                if (!info.status){
                    mAlert(info.message);
                }
                $('#mytable').bootstrapTable('refresh');//刷新table
                $('#mytable2').bootstrapTable('refresh');//刷新table
            }

        );
    }

    //点击弹出删除部门对话框
    o.Del = function (id) {
        mAjax(
            "Role/DelPower/",
            {Token: $.cookie("Token"),RoleId:RoleId, PowerId:id},
            function(info){
                if (!info.status){
                    mAlert(info.message);
                }
                $('#mytable').bootstrapTable('refresh');//刷新table
                $('#mytable2').bootstrapTable('refresh');//刷新table
            }

        );
    };

    return o;
};