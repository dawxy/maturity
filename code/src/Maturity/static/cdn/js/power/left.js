/**
 * Created by dawxy on 2017/5/10.
 */

$(document).ready(function(){
    if (CheckAllowPower(["查询角色以及对应权限列表"])){
        $("#LeftRole").show();
    }
    var leftJs = newleft();
    leftJs.checkLeftStatus();
});

var newleft = function () {
    var o = new Object();
    //检查左部菜单
    o.checkLeftStatus = function () {
        if (isPathnamePre("/power/index.html") || window.location.pathname == "/power/"){
            $("#LeftAllPower").attr("class","active").children().attr("href","#");
        }
        if (isPathnamePre("/power/role.html")){
            $("#LeftRole").attr("class","active").children().attr("href","#");
        }
        if (isPathnamePre("/power/rolepower.html")){
            $("#LeftRole").attr("class","active");
        }
    }
    return o;
}
