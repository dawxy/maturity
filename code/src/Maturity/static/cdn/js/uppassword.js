/**
 * Created by dawxy on 2017/5/9.
 */

$(document).ready(function(){
    //回车触发事件
    document.onkeydown = function (e) {
        var theEvent = window.event || e;
        var code = theEvent.keyCode || theEvent.which;
        if (code == 13) {
            $("#login").click();
        }
    }
    _GET = GET();
    //$.support.transition = true
    $('[data-toggle="popover"]').popover()
    $("#login").click(function() {
        NewPassword = $('#NewPassword').val()
        if (NewPassword == ""){
            mAlert("新密码")
            return
        }
        NewPassword2 = $('#NewPassword2').val()
        if (NewPassword2 == ""){
            mAlert("请输入重复新密码")
            return
        }
        if(NewPassword != NewPassword2){
            mAlert("两次输入的密码不一致")
            return
        }
        //登录请求
        mAjax(
            "User/ChangePassword/",
            {JobNumber:$.cookie("JobNumber"),OldPassword:$.cookie("OldPassword"),NewPassword:NewPassword},
            function(info){
                if (info.status){
                    $.removeCookie("JobNumber", { path: '/' });
                    $.removeCookie("OldPassword", { path: '/' });
                    window.location.href = "login.html?backurl="+_GET["backurl"];
                    return;
                }else{
                    mAlert(info.message);
                }
            }
        );
    });
});

function BacktoUrl() {
    tourl = "/";
    _GET = GET();
    //console.log(GET["backurl"]);
    if (_GET["backurl"] != undefined){
        tourl = _GET["backurl"];
    }
    window.location.href = decodeURIComponent(tourl);
}
