/**
 * Created by dawxy on 2017/5/15.
 */
var page = undefined;
var bop = undefined;
$(document).ready(function(){
    page = newpage();
    page.init();
});

var newpage = function() {
    var o = new Object();

    var wheresAuto = undefined;
    o.init = function () {
        wheresAuto = NewAutoUpdate('wheres');//获取所有datakey
        //初始化表格
        bop = {
            id:$('#mytable'),
            url:"Statistics/QuerySystemLogs/",
            search: false,
            showRefresh:false,
            columns: [{
                field: 'Type',
                title: '操作'
            },{
                field: 'TargetUserJobName',
                title: '目标用户',
                formatter:function(value, row, index){
                    if (value == ""){
                        return "";
                    }
                    return value+"("+row.TargetUserJobNum+")";
                }
            },{
                field: 'ProjectName',
                title: '操作项目',
                formatter:function(value, row, index){
                    if (value == ""){
                        return "";
                    }
                    return value+"("+row.ProjectId+")";
                }
            },{
                field: 'Value',
                title: '操作值'
            },{
                field: 'UserName',
                title: '操作人',
                formatter:function(value, row, index){
                    return value+"("+row.UpJobNumber+")";
                }
            },{
                field: 'Ip',
                title: '操作IP'
            },{
                field: 'Time',
                title: '操作时间'
            },
            ]
        }
        loadBootstrapTable(bop);
        //初始化筛选条件
        loadSelect2({
            selectId:$("#UpUserJobNumber"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "User/QueryUserList/",
            data: {},
            dataMapFunc:function (val) {
                val.id = val.JobNumber;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        $("#Type").select2();
        //初始化时间范围
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 1, //时间间隔(秒)
            timePicker24Hour: true,
            timePickerSeconds:true,
            showDropdowns:true,
            autoApply:true,//自动应用选择值
            linkedCalendars:false,//是否始终显示连续的两个月
            locale:{
                applyLabel: '确认',
                cancelLabel: '取消',
                fromLabel : '起始时间',
                toLabel : '结束时间',
                customRangeLabel : '自定义',
                firstDay : 1,
                format: 'YYYY-MM-DD HH:mm:ss',
                separator : ' 至 '
            },
            ranges : {
                '最近1小时': [moment().subtract(1, 'hours'), moment()],
                '今日': [moment().startOf('day'), moment()],
                '昨日': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                '最近7日': [moment().subtract(6, 'days'), moment()],
                '最近30日': [moment().subtract(29, 'days'), moment()],
                '本月': [moment().startOf("month"),moment().endOf("month")],
                '上个月': [moment().subtract(1,"month").startOf("month"),moment().subtract(1,"month").endOf("month")]
            },

            opens : 'right',	// 日期选择框的弹出位置
            showWeekNumbers : false,		// 是否显示第几周
        });
        o.reservationtimeClear();
    }
    o.reservationtimeClear = function () {
        $('#reservationtime').val("");
    }

    o.Reset = function () {
        $.each(wheresAuto.bind, function(key, value) {//遍历
            $(value).val("");
            $(value).change();
        });
    };
    o.Search = function () {//请求查询
        var data = {};
        $.each(wheresAuto.bind, function(key, value){//获取所有筛选条件
            var val = $(value).val();
            if(val == null || val == "") {
                if (key == "RangeTime"){
                    data["StartTime"] = data["EndTime"] = null;
                }
                data[key] = null;
                return;
            }
            if (key == "RangeTime"){
                str = val.split(" 至 ");
                data["StartTime"] = str[0];
                data["EndTime"] = str[1];
            }else{
                data[key] = StringToDataType($(value).val(), $(value).attr("datatype"));
            }

        });
        //console.log(data);
        //$('#mytable').bootstrapTable("load", data);
        $('#mytable').bootstrapTable('refreshOptions', {pageNumber:1}).bootstrapTable("refresh", {query:data});
    };
    return o;
};