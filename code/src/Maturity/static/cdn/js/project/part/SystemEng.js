/**
 * Created by dawxy on 2017/5/23.
 */

var page = undefined;
var bop = undefined;
var ProjectId = undefined;
$(document).ready(function(){
    _GET = GET();
    ProjectId = parseInt(_GET["id"],10);
    page = newPage();
    page.init();
    mAjax(
        "Part/QueryProfessionList/",
        {Token: $.cookie("Token"),Start:0,Length:100},
        function(info){
            if (!info.status){
                mAlert(info.message);
            }
            var titleP = "<option >全部</option>";
            for (var i = 0; i < info.data.Elems.length; i++){
                titleP += "<option value = '"+info.data.Elems[i].Id+"' >"+info.data.Elems[i].Name+"</option>";
            }
            //先获取专业列表在加载表格
            bop = {
                id:$('#mytable'),
                url:"Project/QuerySystemEngList/",
                data:{
                    Token: $.cookie("Token"),
                    ProjectId:ProjectId,
                },
                columns:[{
                    field:"JobNumber",
                    title: '工号',
                },{
                    field: 'Name',
                    title: '姓名',
                },{
                    field: 'Profession',
                    title: "专业 <select id = 'selectProfession' onchange='page.OnChangeProfession($(this).val())'> "+titleP+"</select>",
                },{
                    field: 'Id',
                    title: '删除',
                    formatter:function(value, row, index){
                        return "<button onclick='page.Del("+value+")' class='btn btn-danger'>删除</button>";
                    }
                },
                ]
            };
            loadBootstrapTable(bop);
        }
    );
});

var newPage = function() {
    var o = new Object();
    var AutoUpdate = undefined;
    o.init = function () {
        AutoUpdate = NewAutoUpdate('AddForm');
        AutoUpdate.UpdateUrl = "Project/AddSystemEng/";
        AutoUpdate.messageBox = $('#AddmessageBox');
        AutoUpdate.data = {
            Token: $.cookie("Token"),
            ProjectId: ProjectId,
        };
        $('#AddDialog').on('hidden.bs.modal', function (e) {//隐藏后重置
            AutoUpdate.reset();
            $('#mytable').bootstrapTable('refresh');//刷新table
        });


    }

    //添加对话框
    o.Add = function () {
        $('#AddDialog').modal('show');
        loadSelect2({
            selectId:$("#UserId"),
            placeholder:"请选择一个用户(可按照工号或者姓名搜索)",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#ProfessionId"),
            url: "Part/QueryProfessionList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
    }
    //确认添加
    o.OnAdd = function (){
        AutoUpdate.UpdateData();
    }
    var delId = undefined;
    //点击弹出删除部门对话框
    o.Del = function (id) {
        delId = id;
        $('#DelDialog').modal('show');
    }
    //删除
    o.OnDel= function () {
        mAjax(
            "Project/DelSystemEng/",
            {Token: $.cookie("Token"),Id:delId, ProjectId:ProjectId},
            function(info){
                if (!info.status){
                    mAlert(info.message);
                }
                $('#mytable').bootstrapTable('refresh');//刷新table
            }

        );
    }

    //当专业选择修改时触发
    o.OnChangeProfession = function (val) {
        bop.data.ProfessionId = parseInt(val,10);
        $('#mytable').bootstrapTable('refresh',{query:{Start:0}});//刷新table
    }

    return o;
};