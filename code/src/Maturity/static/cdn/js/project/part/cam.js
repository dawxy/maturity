/**
 * Created by dawxy on 2017/5/15.
 */
var page = undefined;
var ProjectId = undefined;
$(document).ready(function(){

});

//定义初始化字段函数(会被infoleft调用)
LoadedFields = function (cam) {
    var FieldIndex = {};//字段对应的信息
    var _GET = GET();
    ProjectId = parseInt(_GET["ProjectId"],10);
    $("#BaseForm").html("");
    $("#htitle").html(cam.CamName); //设置标题
    //渲染所有字段
    cam.Tag.forEach(function(tag, index){
        var str = '<div id = "p'+(index+1)+'" class="modal-content"><div class="box box-default"><div class="box-header with-border">'+
            '<h3 class="box-title" data-widget="collapse" style="cursor:pointer"><i class="fa fa-minus"></i>'+tag.TagName+'</h3></div>'+
            '<div class="box-body"><div class="row">';
        //先填入4个空列
        var colLen = 4;
        var col = [];
        for(var i = 0; i < colLen; i++){
            col.push('<div class="col-md-3">');
        }
        var idx = 0;
        //遍历所有字段
        tag.Fields.forEach(function(Field){
            var Zname = Field.FieldName.split("_")[1];
            var fname = Field.Field.split("__")[1];
            FieldIndex[fname] = Field;
            var tmp = "";
            switch(Field.FieldType){
                case "string":
                    tmp = '<div class="form-group">' +
                        '<label>'+Zname+'</label>' +
                        '<input DataKey = "'+fname+'" class="form-control" type="text">' +
                        '</div>';
                    break;
                case "int8":
                    tmp = '<div class="form-group">\n' +
                        '<label>'+Zname+'</label>\n' +
                        '<select DataKey = "'+fname+'" datatype="int" OnchangeUpdate = "true"  onchange="page.selectOnchange(this)" type="text" class="form-control">\n';
                    Field.Int8List.forEach(function (val, i) {
                        tmp += '<option value="'+val+'" style="'+page.getInt8Color(val)+'">'+Field.Int8ListName[i]+'</option>\n';
                    });
                    tmp += '</select></div>';
                    break;
                case "time.Time":
                    tmp = '<div class="form-group">\n' +
                        '<label>'+Zname+'</label>\n' +
                        '<div class="input-group date">\n' +
                        '<div class="input-group-addon">\n' +
                        '<i class="fa fa-calendar"></i>\n' +
                        '</div>\n' +
                        '<input DataKey = "'+fname+'" OnchangeUpdate="true" type="text" class="form-control pull-right datepicker" >\n' +
                        '</div>\n' +
                        '</div>';
                    break;
                default:
                    tmp = '<div class="form-group">\n' +
                        '<label>'+Zname+'</label>\n' +
                        '<select DataKey = "'+fname+'Id" data-to-view-func = "page.DataToView" OnchangeUpdate = "true" datatype="int" class="form-control select2" type="text"></select>\n' +
                        '</div>';
            }
            col[idx] += tmp;
            idx = (idx+1)%colLen;
        });
        for(var i = 0; i < colLen; i++){
            col[i] += '</div>';
            str += col[i];
        }
        str += '</div></div></div></div>';
        $("#BaseForm").append(str);
    })

    page = newpage(cam, FieldIndex);
    page.init();
    //设置滚动区高度
    var heg = document.body.scrollHeight-$("#scrollContent").offset().top-(document.body.scrollHeight-$("#footer").offset().top);
    $("#scrollContent").attr("style","height:"+heg+"px;width:100%;overflow:auto;");
}
var newpage = function(cam, FieldIndex) {
    var o = new Object();
    var BaseAutoUpdate = undefined;
    o.init = function () {
        BaseAutoUpdate = NewAutoUpdate('BaseForm',FieldIndex);
        BaseAutoUpdate.GetUrl = "Part/QueryPart_Cam/";
        if(_GET["history"] != "true"){
            BaseAutoUpdate.GetPowerUrl = "Part/CheckUpFieldPower/";
        }else{
            BaseAutoUpdate.GetPowerUrl = "#";
        }
        BaseAutoUpdate.UpdateUrl = "Part/UpdatePart_Cam/";
        if (Cam == "Part"){ //特殊处理Part
            BaseAutoUpdate.GetUrl = "Part/QueryPart_Base/";
            BaseAutoUpdate.UpdateUrl = "Part/UpdatePart_Base/";
        }
        //BaseAutoUpdate.OnblurUpdate = true;
        BaseAutoUpdate.messageBox = $('#UpdatemessageBox');
        BaseAutoUpdate.data = {
            Token: $.cookie("Token"),
            Cam:Cam,
        };
        BaseAutoUpdate.InfoMsg = function(msg, status){
            if (!status){
                mAlert(msg);
                return;
            }
            BaseAutoUpdate.GetData();
        }
        BaseAutoUpdate.resetSuccessBackcall = function () {
            $('.datepicker').datepicker({
                language: 'zh-CN',
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
            });
        };
        BaseAutoUpdate.reset();
        //获取内容
        BaseAutoUpdate.data.PartId = PartId;
        BaseAutoUpdate.data.ApiName = "UpdatePart_"+cam.CamId;
        BaseAutoUpdate.GetDataCallBack = function (info) {
            if(info.status && info.data.Deled){
                $("#htitle").append("<span style='color: red'>(已删除)</span>");
            }
        }
        BaseAutoUpdate.PowerCallBack = function (info) {
            LeftPowerCallBack(info);//回调infoleft内的函数
            //console.log(info)
            if(info.status && info.data.Fields != null && info.data.Fields.length > 0){
                $("#btnSave").show();
            }
        }
        BaseAutoUpdate.GetData();
    }
    o.Save = function () {
        BaseAutoUpdate.UpdateData();
    }

    o.getInt8Color = function (val) {
        if (val == 1){
            return "background-color: green;color: white"
        }else if(val == 0){
            return "background-color: red;color: white";
        }else if (val == 4){
            return "background-color: green;color: white";
        }else if(val == 2){
            return "background-color: red;color: white";
        }else if (val == 3){
            return "background-color: yellow";
        }else{
            return "background-color: white";
        }
    }
    o.selectOnchange = function (v, noocolour) {
        var val = $(v).val();
        if (noocolour == undefined){
            $(v).attr("style",o.getInt8Color(val));
        }
    }
    //字段获取数据时对应的页面展示
    o.DataToView = function (bind, val, data) {
        var DataKey = $(bind).attr("DataKey");
        DataKey = DataKey.substr(0,DataKey.length-2);
        if(DataKey.length > 3 && DataKey.substr(DataKey.length-3,DataKey.length) == "Eng"){//处理各协同开发小组
            var RoleId = 1;
            switch(DataKey){
                case "NewqualityEng":
                    RoleId++;
                case "SizeEng":
                    RoleId++;
                case "TestEng":
                    RoleId++;
                case "CrefftauEng":
                    RoleId++;
                case "SQEEng":
                    RoleId++;
                case "BuyerEng":
                    RoleId++;
                case "DesignerEng":
                    RoleId++;
            }
            // console.log(DataKey, RoleId);
            //设置加载选项方式
            loadSelect2({
                data:{RoleId:RoleId},
                selectedRow:{id:val, text: data[DataKey]+"("+data[DataKey+'JobNumber']+")"},
                selectId:$(bind),
                url: "User/QueryUserList/",
                dataMapFunc:function (val) {
                    val.id = val.Id;
                    val.text = val.Name+"("+val.JobNumber+")";
                }
            });
        }else{
            if (DataKey == "Profession"){ //特殊处理专业
                //设置加载选项方式
                loadSelect2({
                    selectedRow:{id:val, text: data.Profession},
                    data:{ProjectId:ProjectId},
                    selectId:$(bind),
                    url: "Part/QuerySystemEngProfessionList/",
                    dataMapFunc:function (val) {
                        val.id = val.Id;
                        val.text = val.Name;
                    }
                });
            }else{
                //设置加载选项方式
                loadSelect2({
                    selectedRow:{id:val, text: data[DataKey]},
                    selectId:$(bind),
                    url: url = "Part/Query"+DataKey+"List/",
                    dataMapFunc:function (val) {
                        val.id = val.Id;
                        val.text = val.Name;
                    }
                });
            }

        }

    }

    //KPC是否隐藏
    o.NeedKPCToView = function (bind, val, data) {
        if (val == 1){
            $("#NeedKPCView").show();
        }else{
            $("#NeedKPCView").hide();
        }
    }

    return o;
};