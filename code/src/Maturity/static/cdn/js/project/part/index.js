/**
 * Created by dawxy on 2017/5/15.
 */
var partindex = undefined;
var ProjectId  = undefined;
var bop = undefined;
$(document).ready(function(){
    var _GET = GET();
    ProjectId = parseInt(_GET["id"],10);
    partindex = newpartindex();
    partindex.init();

});

var newpartindex = function() {
    var o = new Object();
    var AutoUpdate = undefined;
    var UpdateAutoUpdate = undefined;
    var ids = undefined;
    o.init = function () {
        bop = {
            id:$('#mytable'),
            search: false,
            showRefresh:false,
            url:"Part/QueryPartList/",
            data:{
                ProjectId: ProjectId,
            },
            columns:[{
                field:"PartCode",
                title: '零件号',
                formatter:function(value, row, index){
                    return "<a href='cam.html?ProjectId="+ProjectId+"&partid="+row.Id+"&empty=0' target='_blank'>"+value+"</a>";
                }
            },{
                field: 'Name',
                title: '零件名称',
                formatter:function(value, row, index){
                    return "<a href='cam.html?ProjectId="+ProjectId+"&partid="+row.Id+"&empty=0' target='_blank'>"+value+"</a>";
                }
            },{
                field: 'QualityLevel',
                title: '质量等级'
            }, {
                field: 'DevelopmentType',
                title: '开发类型',
            },{
                field: 'Profession',
                title: "所属专业",
            },{
                field:"DesignerEng",
                title: '设计工程师'
            },{
                field:"BuyerEng",
                title: '采购工程师'
            },{
                field:"SQEEng",
                title: 'SQE工程师'
            },{
                field:"TestEng",
                title: '试验工程师'
            },{
                field:"CrefftauEng",
                title: '工艺工程师'
            },
                //     {
                //     field: 'Id',
                //     title: '修改',
                //     formatter:function(value, row, index){
                //         return "<button onclick='partindex.Update("+value+")' class='btn btn-primary'>修改</button>";
                //     }
                // },
                {
                    field: 'Id',
                    title: '删除<i class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="只有拥有这个专业的系统集成经理才可删除(删除后还是可以在操作历史看到删除时的所有数据)"></i>',
                    formatter:function(value, row, index){
                        if (HasEngs["SystemEng__"+row.Profession]){
                            return "<button onclick='partindex.Del("+value+")' class='btn btn-danger'>删除</button>";
                        }
                        return "";
                    }
                },
            ]
        }

        HasEngsFinish = function () { //left.js中查询权限回调
            if (HasEngs["SystemEng"] || HasEngs["CdtEng"]){
                if(HasEngs["SystemEng"]){
                    $("#addOrImport").show();
                }
                if (HasEngs["CdtEng"]){//隐藏没有权限的DateKey
                    //获取批量修改窗口所有DateKey
                    var AllDateKey = GetAllDateKey('#UpdateForm');
                    $.each(AllDateKey, function(key, value) {
                        if (!HasEngs["CdtEng__"+key.substr(0,key.length-2)]){
                            $(value).parent().hide();
                        }
                    });
                }
                $("#btnUpdate").show();
                bop.columns.unshift({
                    //field:"Id",
                    checkbox:true,
                    formatter:function(value, row, index){
                        if (HasEngs["SystemEng__"+row.Profession] || HasEngs["CdtEng"]){
                            return {
                                disabled: false
                            };
                        }
                        return {
                            disabled: true
                        };
                    },
                    // cellStyle:function (row, index, field) {
                    //     console.log(row, index, field);
                    // }
                });
            }
            loadBootstrapTable(bop);

            UpdateAutoUpdate = NewAutoUpdate('UpdateForm');
            UpdateAutoUpdate.UpdateUrl = "Part/BatchUpdateParts";
            UpdateAutoUpdate.messageBox = $('#UpdatemessageBox');
            UpdateAutoUpdate.data = {
                Token: $.cookie("Token"),
                ProjectId:ProjectId,
            };
            $('#UpdateDialog').on('hidden.bs.modal', function (e) {//隐藏后重置
                UpdateAutoUpdate.reset();
                $('#mytable').bootstrapTable('refresh');//刷新table
            });

            $('#ImportDialog').on('hidden.bs.modal', function (e) {//隐藏后重置
                $("#excel").val("");
                $('#mytable').bootstrapTable('refresh');//刷新table
            });
            $("#downloaduser").attr("href","/api/Part/DownloadAllUser/?Token="+$.cookie("Token"));
        };

        AutoUpdate = NewAutoUpdate('AddForm');
        AutoUpdate.UpdateUrl = "Part/AddPart/";
        AutoUpdate.messageBox = $('#AddmessageBox');
        AutoUpdate.data = {
            Token: $.cookie("Token"),
            ProjectId:ProjectId,
        };
        $('#AddDialog').on('hidden.bs.modal', function (e) {//隐藏后重置
            AutoUpdate.reset();
            $('#mytable').bootstrapTable('refresh');//刷新table
        });


        wheresAuto = NewAutoUpdate('wheres');//获取所有datakey
        //初始化筛选条件
        loadSelect2({
            selectId:$("#ProfessionId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "Part/QueryProfessionList/",
            data: {ProjectId:ProjectId},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
        loadSelect2({
            selectId:$("#DevelopmentTypeId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "Part/QueryDevelopmentTypeList/",
            data: {ProjectId:ProjectId},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
        loadSelect2({
            selectId:$("#DesignerEngId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "User/QueryUserList/",
            data: {RoleId:2},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#BuyerEngId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "User/QueryUserList/",
            data: {RoleId:3},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#SQEEngId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "User/QueryUserList/",
            data: {RoleId:4},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#TestEngId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "User/QueryUserList/",
            data: {RoleId:6},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#CrefftauEngId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "User/QueryUserList/",
            data: {RoleId:5},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
    }

    //添加对话框
    o.Add = function (id) {
        $('#AddDialog').modal('show');
        loadSelect2({
            selectId:$("#AddProfessionId"),
            url: "Part/QuerySystemEngProfessionList/",
            data: {ProjectId:ProjectId},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
        loadSelect2({
            selectId:$("#AddArrowSystemId"),
            url: "Part/QueryArrowSystemList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
        loadSelect2({
            selectId:$("#AddQualityLevelId"),
            url: "Part/QueryQualityLevelList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
        loadSelect2({
            selectId:$("#AddDevelopmentTypeId"),
            url: "Part/QueryDevelopmentTypeList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
        loadSelect2({
            selectId:$("#AddIntegreiddioTechnolegEngId"),
            placeholder:"请选择一个用户(可按照工号或者姓名搜索)",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#AddDesignerEngId"),
            data:{RoleId:2},
            placeholder:"请选择一个用户(可按照工号或者姓名搜索)",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#AddBuyerEngId"),
            data:{RoleId:3},
            placeholder:"请选择一个用户(可按照工号或者姓名搜索)",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#AddSQEEngId"),
            data:{RoleId:4},
            placeholder:"请选择一个用户(可按照工号或者姓名搜索)",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#AddCrefftauEngId"),
            data:{RoleId:5},
            placeholder:"请选择一个用户(可按照工号或者姓名搜索)",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#AddTestEngId"),
            data:{RoleId:6},
            placeholder:"请选择一个用户(可按照工号或者姓名搜索)",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });

    }
    //确认添加
    o.OnAdd = function (){
        AutoUpdate.UpdateData();
    }
    var delId = undefined;
    //点击弹出删除对话框
    o.Del = function (id) {
        delId = id;
        $('#DelDialog').modal('show');
    }
    //删除
    o.OnDel= function () {
        mAjax(
            "Part/DelPart/",
            {Token: $.cookie("Token"),PartId:delId},
            function(info){
                if (!info.status){
                    mAlert(info.message);
                }
                $('#mytable').bootstrapTable('refresh');//刷新table
            }

        );
    }



    o.Import = function () {
        $('#ImportmessageBox').hide();
        $("#ImportToken").hide();
        $("#ImportProjectId").hide();
        $('#ImportDialog').modal('show');
    }
    o.OnImport = function () {
        $(".overlay").show();//显示等待
        $("#ImportToken").val($.cookie("Token"));
        $("#ImportProjectId").val(ProjectId);
        $.ajax({
            type: "POST",
            url: syprefix+"Part/ImportPart/",
            data: new FormData($('#ImportForm')[0]),
            dataType: "json",
            processData: false,
            contentType: false,
            cache:false,
            //contentType:"multipart/form-data",
            success:function(info){
                if (info.status){
                    if (o.updateSuccessBackcall != undefined){
                        o.updateSuccessBackcall(info);
                    }
                    $('#ImportmessageBox').attr("style", "text-align:center;color:green");
                    $('#ImportmessageBox').html(info.message);
                    $('#ImportmessageBox').show();
                }else{
                    $('#ImportmessageBox').attr("style", "text-align:center;color:red");
                    $('#ImportmessageBox').html(info.message);
                    $('#ImportmessageBox').show();
                }
            },
            error:function(jqXHR, textStatus,errorThrown) {
                $("#ImportDialog").html(errorThrown);
            },
            complete:function () {
                $(".overlay").hide();//关闭/显示等待
            }
        });
    }

    o.Reset = function () {
        $.each(wheresAuto.bind, function(key, value) {//遍历
            $(value).val("");
            $(value).change();
        });
        $("#s2base").val("");
        $("#s2base").change();
    };
    o.Search = function () {//请求查询
        var data = {ProjectId:ProjectId};
        $.each(wheresAuto.bind, function(key, value){//获取所有筛选条件
            var val = $(value).val();
            if(val == null || val == "") {
                data[key] = null;
                return;
            }
            data[key] = StringToDataType($(value).val(), $(value).attr("datatype"));
        });
        //console.log(data);
        //$('#mytable').bootstrapTable("load", data);
        $('#mytable').bootstrapTable('refreshOptions', {pageNumber:1}).bootstrapTable("refresh", {query:data});
    };
    o.Update = function () {
        var selectList = $('#mytable').bootstrapTable('getAllSelections');
        ids = [];
        for (var i = 0; i < selectList.length; i++){
            ids.push(selectList[i].Id);
        }
        if (ids.length == 0){
            mAlert("请选择零件!")
            return;
        }
        $('#UpdateDialog').modal('show');
        loadSelect2({
            selectId:$("#UpdateDesignerEngId"),
            data:{RoleId:2},
            placeholder:"不选择不会修改",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#UpdateBuyerEngId"),
            data:{RoleId:3},
            placeholder:"不选择不会修改",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#UpdateSQEEngId"),
            data:{RoleId:4},
            placeholder:"不选择不会修改",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#UpdateCrefftauEngId"),
            data:{RoleId:5},
            placeholder:"不选择不会修改",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#UpdateTestEngId"),
            data:{RoleId:6},
            placeholder:"不选择不会修改",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#UpdateSizeEngId"),
            data:{RoleId:7},
            placeholder:"不选择不会修改",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#NewqualityEngId"),
            data:{RoleId:8},
            placeholder:"不选择不会修改",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        UpdateAutoUpdate.data.PartIds = ids;
        $('#mytable').bootstrapTable('getAllSelections');
        $("#updateHint").text("您选择了"+ids.length+"个零件");
    }
    o.OnUpdate = function (){
        UpdateAutoUpdate.UpdateData();
    }
    return o;
};