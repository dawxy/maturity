/**
 * Created by dawxy on 2017/5/23.
 */

var page = undefined;
var ProjectId = undefined;
$(document).ready(function(){
    ProjectId = parseInt(_GET["id"],10);
    page = newPage();
    page.init();
    loadBootstrapTable({
        id:$('#mytable'),
        url:"Project/QueryTecEngList/",
        data:{
            Token: $.cookie("Token"),
            ProjectId:ProjectId,
        },
        columns:[{
            field:"JobNumber",
            title: '工号',
        },{
            field: 'Name',
            title: '姓名',
        },{
            field: 'Id',
            title: '删除',
            formatter:function(value, row, index){
                return "<button onclick='page.Del("+value+")' class='btn btn-danger'>删除</button>";
            }
        },
        ]
    });
});

var newPage = function() {
    var o = new Object();
    var AutoUpdate = undefined;
    o.init = function () {
        AutoUpdate = NewAutoUpdate('AddForm');
        AutoUpdate.UpdateUrl = "Project/AddTecEng/";
        AutoUpdate.messageBox = $('#AddmessageBox');
        AutoUpdate.data = {
            Token: $.cookie("Token"),
            ProjectId:ProjectId,
        };
        $('#AddDialog').on('hidden.bs.modal', function (e) {//隐藏后重置
            AutoUpdate.reset();
            $('#mytable').bootstrapTable('refresh');//刷新table
        });


    }

    //添加对话框
    o.Add = function () {
        $('#AddDialog').modal('show');
        loadSelect2({
            selectId:$("#UserId"),
            placeholder:"请选择一个用户(可按照工号或者姓名搜索)",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
    }
    //确认添加
    o.OnAdd = function (){
        AutoUpdate.UpdateData();
    }
    var delId = undefined;
    //点击弹出删除对话框
    o.Del = function (id) {
        delId = id;
        $('#DelDialog').modal('show');
    }
    //删除
    o.OnDel= function () {
        mAjax(
            "Project/DelTecEng/",
            {Token: $.cookie("Token"),Id:delId},
            function(info){
                if (!info.status){
                    mAlert(info.message);
                }
                $('#mytable').bootstrapTable('refresh');//刷新table
            }

        );
    }

    return o;
};