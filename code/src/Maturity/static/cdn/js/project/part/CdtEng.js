/**
 * Created by dawxy on 2017/5/23.
 */

var page = undefined;
var bop = undefined;
var ProjectId = undefined;
$(document).ready(function(){
    _GET = GET();
    ProjectId = parseInt(_GET["id"],10);
    page = newPage();
    page.init();

});

var newPage = function() {
    var o = new Object();
    var AutoUpdate = undefined;
    o.init = function () {
        var engNames = [{Id:"DesignerEng",Name:"设计工程师"},{Id:"BuyerEng",Name:"采购工程师"},{Id:"SQEEng",Name:"SQE工程师"},{Id:"CrefftauEng",Name:"工艺工程师"},{Id:"TestEng",Name:"试验工程师"}, {Id: "SizeEng", Name: "尺寸工程师"}, {Id: "NewqualityEng", Name: "新品质量工程师"}];
        var titleP = "<option >全部</option>";
        for (var i = 0; i < engNames.length; i++){
            titleP += "<option value = '"+engNames[i].Id+"' >"+engNames[i].Name+"</option>";
        }
        //先获取专业列表在加载表格
        bop = {
            id:$('#mytable'),
            url:"Project/QueryCdtEngList/",
            data:{
                Token: $.cookie("Token"),
                ProjectId:ProjectId,
            },
            columns:[{
                field:"JobNumber",
                title: '工号',
            },{
                field: 'Name',
                title: '姓名',
            },{
                field: 'EngName',
                title: "工程师 <select onchange='page.OnChangeEngs($(this).val())'> "+titleP+"</select>",
            },{
                field: 'Id',
                title: '删除',
                formatter:function(value, row, index){
                    return "<button onclick='page.Del("+value+")' class='btn btn-danger'>删除</button>";
                }
            },
            ]
        };
        loadBootstrapTable(bop);
        var tmpSelectVals = "<option >请选择...</option>";
        for (var i = 0; i < engNames.length; i++){
            tmpSelectVals += "<option value = '"+engNames[i].Id+"' >"+engNames[i].Name+"</option>";
        }
        $("#EngName").html(tmpSelectVals);
        //$("#EngName").select2();

        AutoUpdate = NewAutoUpdate('AddForm');
        AutoUpdate.UpdateUrl = "Project/AddCdtEng/";
        AutoUpdate.messageBox = $('#AddmessageBox');
        AutoUpdate.data = {
            Token: $.cookie("Token"),
            ProjectId: ProjectId,
        };
        $('#AddDialog').on('hidden.bs.modal', function (e) {//隐藏后重置
            AutoUpdate.reset();
            $('#mytable').bootstrapTable('refresh');//刷新table
        });


    }

    //添加对话框
    o.Add = function () {
        $('#AddDialog').modal('show');
        loadSelect2({
            selectId:$("#UserId"),
            placeholder:"请选择一个用户(可按照工号或者姓名搜索)",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });

    }
    //确认添加
    o.OnAdd = function (){
        AutoUpdate.UpdateData();
    }
    var delId = undefined;
    //点击弹出删除部门对话框
    o.Del = function (id) {
        delId = id;
        $('#DelDialog').modal('show');
    }
    //删除
    o.OnDel= function () {
        mAjax(
            "Project/DelCdtEng/",
            {Token: $.cookie("Token"),Id:delId, ProjectId:ProjectId},
            function(info){
                if (!info.status){
                    mAlert(info.message);
                }
                $('#mytable').bootstrapTable('refresh');//刷新table
            }

        );
    }

    //当专业选择修改时触发
    o.OnChangeEngs = function (val) {
        if (val == "全部"){
            bop.data.EngName = undefined;
        }else{
            bop.data.EngName = val;
        }
        $('#mytable').bootstrapTable('refresh',{query:{Start:0}});//刷新table
    }

    return o;
};