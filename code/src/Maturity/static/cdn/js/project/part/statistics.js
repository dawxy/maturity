/**
 * Created by dawxy on 2017/5/15.
 */
var page = undefined;
var ProjectId  = undefined;
var bop = undefined;
$(document).ready(function(){
    var _GET = GET();
    ProjectId = parseInt(_GET["id"],10);
    page = newpage();
    page.init();
});

var mpfieldName = {};//字段对应的中文描述
var newpage = function() {
    var o = new Object();

    var wheresAuto = undefined;
    var s2tablebase = "";
    var stackedBar = undefined;//完成数
    var config = undefined;
    o.init = function () {
        s2tablebase = $("#s2base").html();
        wheresAuto = NewAutoUpdate('wheres');//获取所有datakey
        o.s2tableRefresh(true);
        //初始化筛选条件
        $("#Table").change(function () {
            o.s2tableRefresh();
        });

        //初始化分布图
        config = {
            type: 'bar',
            data: {
                labels: [],
                datasets: []
            },
            options: {
                title:{
                    display:true,
                    text:"阶段 完成数统计图",
                    fontSize:22
                },
                tooltips: {
                    enabled: true,
                    mode: 'index',
                    intersect: false,
                    callbacks: {
                        afterBody: function(tooltipItem, data) {
                            if(tooltipItem.length == 0){
                                return;
                            }
                            var ret = [];
                            //遍历所有专业
                            for (var j = 0; j < ProfessionList.length; j++){
                                if (tooltipItem[j] == undefined){
                                    continue;
                                }
                                var index = tooltipItem[j].index;
                                var x = +data.datasets[j+ProfessionList.length].data[index];
                                var all = data.datasets[j].data[index];
                                //console.log(ProfessionList[j].Name, x,all)
                                if (all == undefined){
                                    continue;
                                }
                                var c = undefined;
                                if (x == undefined || all == 0){
                                    c = "-";
                                }else{
                                    c = (x/all*100).toFixed(1)+"%";
                                }
                                ret.push(ProfessionList[j].Name+"完成率:"+c);
                            }
                            return ret;
                        }
                    },
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true,
                    }]
                }
            }
        };
        stackedBar = new Chart(document.getElementById("stackedBar").getContext("2d"),config);
    }
    var ProfessionList = undefined; //所有专业列表
    o.stackedBarRefresh = function (datalist) { //刷新统计图数据
        // for(var i = 0; i < config.data.datasets[0].data.length; i++){
        //     config.data.datasets[0].data[i] = 0;
        // }
        config.options.title.text = $("#Table").find("option:selected").text()+"完成数统计图";
        var data = {
            labels:[],
            datasets:[],
        };
        var colorList = ["#7d5029","#146fa8","#97c21a","#f29400","#e45022"];
        var colorList2 = ["#5d2e04","#005085","#4d971c","#c97f09","#bd4722"];
        //获取所有专业,添加到datasets
        for (var i = 0; i < ProfessionList.length; i++){
            tmp = ProfessionList[i];
            data.datasets.push({
                label: tmp.Name+'总数',
                backgroundColor: colorList[i],
                stack:"1",
                data: []
            });
        }
        for (var i = 0; i < ProfessionList.length; i++){
            tmp = ProfessionList[i];
            data.datasets.push({
                label: tmp.Name+'完成数',
                backgroundColor: colorList2[i],
                stack:"2",
                data: []
            });
        }
        //填入参数
        for (var i = 0; i < datalist.length; i++) {
            var row = datalist[i];
            if(row == null || row == undefined){
                continue;
            }
            data.labels.push(mpfieldName[row.fieldName]); //添加字段(group)
            //遍历所有专业
            for (var j = 0; j < ProfessionList.length; j++){
                tmp = ProfessionList[j];
                val = tmp.Id;
                if(row[val] == undefined){
                    continue;
                }
                data.datasets[j].data.push(parseInt(row["ALL$"+val],10));
                data.datasets[j+ProfessionList.length].data.push(parseInt(row[val], 10));
            }
        }

        //console.log(data);
        config.data = data;
        //console.log(config.data.datasets[0].data)
        stackedBar.update();

    }
    o.s2tableRefresh = function (initTable) {
        $("#s2base").html(s2tablebase);
        //获取
        mAjax(
            "Part/QueryAllPartField/",
            {Token: $.cookie("Token"), Statistics:true, Table:$("#Table").val()},
            function(info) {
                if (!info.status) {
                    mAlert(info.message);
                }
                var select2_ary = [];
                var init_ary = [];
                var lasttb = undefined;
                var last2 = undefined;
                var group = undefined;
                var group2 = undefined;
                for (var i = 0; i < info.data.length; i++){
                    init_ary.push(info.data[i].Field);
                    //console.log(info.data[i].Field, info.data[i].FieldName);
                    mpfieldName[info.data[i].Field] = info.data[i].FieldName;
                    var table = info.data[i].TableName;
                    if (lasttb != table){
                        group = {
                            text:table,
                            children:[],
                        };
                        select2_ary.push(group);
                    }
                    var strs = info.data[i].FieldName.split("_");
                    if (strs.length > 1){//包含二级分类
                        if (last2 != strs[0]){
                            group2 = {
                                text:strs[0],
                                children:[],
                            };
                            group.children.push(group2);
                        }
                        group2.children.push({id:info.data[i].Field, text:strs[1]});
                        last2 = strs[0];
                    }else{
                        group.children.push({id:info.data[i].Field, text:strs[0]});
                        last2 = undefined;
                        group2 = undefined;
                    }
                    lasttb = table;
                    //$("#s2base").append("<option value="+info.data[i].Field+">"+info.data[i].TableName+"__"+info.data[i].FieldName+"</option>");
                }
                $("#s2base").select2({data: select2_ary});
                //默认全选
                $("#s2base").val(init_ary);
                $("#s2base").trigger('change');
                if (initTable != undefined){
                    o.initTable(o.Search);
                }else{
                    o.Search();
                }

            }
        );
    };

    o.formatterFieldName = function(value, row, index){
        return mpfieldName[value];
    }
    o.formatterAll = function(value, row, index){
        var sum = 0;
        for (var i = 0; i < ProfessionList.length; i++){
            tmp = ProfessionList[i];
            val = row["ALL$"+tmp.Id];
            if (val == undefined){
                continue;
            }
            sum += parseInt(val,10);
        }
        return sum;
    }
    o.formatterOk = function(value, row, index){
        var sum = 0;
        for (var i = 0; i < ProfessionList.length; i++){
            tmp = ProfessionList[i];
            val = row[tmp.Id];
            if (val == undefined){
                continue;
            }
            sum += parseInt(val,10);
        }
        return sum;
    }
    o.formatterRate = function(value, row, index){
        var all = 0;
        var sum = 0;
        for (var i = 0; i < ProfessionList.length; i++){
            tmp = ProfessionList[i];
            val = row["ALL$"+tmp.Id];
            if (val == undefined){
                continue;
            }
            all += parseInt(val,10);
            sum += parseInt(row[tmp.Id],10);
        }
        if (all == 0){
            return "-";
        }
        return (sum/all*100).toFixed(1)+'%';
    }
    //初始化表格
    o.initTable = function (initsuccessFunc) {
        //获取专业信息
        mAjax(
            "Part/QueryProfessionList/",
            {Token: $.cookie("Token"), Start:0, Length:500},
            function(info) {
                if (!info.status) {
                    mAlert(info.message);
                }
                ProfessionList = info.data.Elems;
                bop = {
                    id:$('#mytable'),
                    search: false,
                    showRefresh:false,
                    pagination:false,
                    data:{ProjectId:ProjectId},
                    loadedData:o.stackedBarRefresh, //表格获取完数据后调用
                };
                var row1 = "<tr>";
                var row2 = "<tr>";
                row1 += '<th data-field="fieldName" data-formatter="page.formatterFieldName" rowspan="2"></th>';
                row1 += '<th data-field="fieldName" data-formatter="page.formatterAll" rowspan="2">总数</th>';
                row1 += '<th data-field="fieldName" data-formatter="page.formatterOk" rowspan="2">完成</th>';
                row1 += '<th data-field="fieldName" data-formatter="page.formatterRate" rowspan="2">完成率</th>';
                for (var i = 0; i < info.data.Elems.length; i++){
                    var tmp = info.data.Elems[i];
                    row1 += '<th rowspan="1" colspan="3">'+tmp.Name+'</th>';
                    var field = "ALL$"+tmp.Id;
                    var RateField = "Rate$"+tmp.Id;
                    row2 += '<th data-field="'+field+'" rowspan="1" colspan="1" >总数</th>';
                    field = tmp.Id;
                    row2 += '<th data-field="'+field+'" rowspan="1" colspan="1" >已完成</th>';

                    row2 += '<th data-field="'+RateField+'" rowspan="1" colspan="1" >完成率</th>';
                }
                row1 += "</tr>";
                row2 += "</tr>";
                $('#mytable').append("<thead>"+row1+row2+"</thead>");
                loadBootstrapTable(bop);
                initsuccessFunc();
            }
        );

    }
    o.Reset = function () {
        $("#Table").change();
    };
    o.Search = function () {//请求查询
        var data = {ProjectId:ProjectId};
        $.each(wheresAuto.bind, function(key, value){//获取所有筛选条件
            var val = $(value).val();
            //console.log(key, val);
            if(val == null || val == "") {
                data[key] = null;
                return;
            }
            data[key] = StringToDataType($(value).val(), $(value).attr("datatype"));
        });
        //$('#mytable').bootstrapTable("load", data);
        $('#mytable').bootstrapTable("refresh", {url:syprefix+"Statistics/CompletionRate/",query:data});
    };
    return o;
};