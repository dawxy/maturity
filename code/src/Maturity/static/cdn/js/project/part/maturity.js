/**
 * Created by dawxy on 2017/5/15.
 */
var page = undefined;
var ProjectId  = undefined;
$(document).ready(function(){
    var _GET = GET();
    ProjectId = parseInt(_GET["id"],10);
    page = newpage();
    page.init();
});

var newpage = function() {
    var o = new Object();

    var wheresAuto = undefined;
    var myPieChart = undefined;//成熟度分布图
    var config = undefined;
    var ProjectName = undefined;
    o.init = function () {
        wheresAuto = NewAutoUpdate('wheres');//获取所有datakey
        loadSelect2({
            selectId:$("#ProfessionId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "Part/QueryProfessionList/",
            data: {ProjectId:ProjectId},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
        $('#ProfessionId').change(function () {
            o.Search();
        });
        $('#Table').change(function () {
            o.Search();
        });
        //初始化分布图
        config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [1, 1, 1, 1],
                    backgroundColor: ["#9C9C9C", "#f56954", "#FFE633", "#00a65a"],
                }],
                labels: [
                    '未选择',
                    '红色',
                    '黄色',
                    '绿色'
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'right',
                },
                title: {
                    display: true,
                    fontSize: 15,
                    text: '项目 阶段 成熟度状态分布图'
                },
            }
        };
        myPieChart = new Chart(document.getElementById("pieChart").getContext("2d"),config);
        //获取项目名称
        mAjax(
            "Project/QueryProjectInfo/",
            {Token: $.cookie("Token"),Id:ProjectId},
            function(info){
                if (!info.status){
                    mAlert(info.message);
                }
                ProjectName = info.data.Name;
                $('#leftprojectname').text(info.data.Name);
                o.Search();
            }

        );

    }
    o.donutRefresh = function (data) {
        if(data == undefined){
            data = {Token: $.cookie("Token"), ProjectId:ProjectId, Table:$("#Table").val()};
        }
        mAjax(
            "Statistics/MaturityDistribution/",
            data,
            function(info) {
                if (!info.status) {
                    mAlert(info.message);
                }
                for(var i = 0; i < config.data.datasets[0].data.length; i++){
                    config.data.datasets[0].data[i] = 0;
                }

                if (info.data.Elems == null){
                    return;
                }
                for (var i = 0; i < info.data.Elems.length; i++) {
                    var tmp = info.data.Elems[i];
                    if("/" == tmp.MaturityState){
                        config.data.datasets[0].data[0] = parseInt(tmp.Count, 10);
                    }else if ("红" == tmp.MaturityState){
                        config.data.datasets[0].data[1] = parseInt(tmp.Count, 10);
                    }else if ("黄" == tmp.MaturityState){
                        config.data.datasets[0].data[2] = parseInt(tmp.Count, 10);
                    }else if ("绿" == tmp.MaturityState){
                        config.data.datasets[0].data[3] = parseInt(tmp.Count, 10);
                    }
                }
                //console.log(config.data.datasets[0].data)
                myPieChart.update();
            }
        );

    }

    o.Reset = function () {
        $("#Table").val("Cam1");
        $("#Table").change();
        $("#ProfessionId").val("");
        $("#ProfessionId").change();
    };
    o.Search = function () {//请求查询
        var data = {Token: $.cookie("Token"), ProjectId:ProjectId};
        $.each(wheresAuto.bind, function(key, value){//获取所有筛选条件
            var val = $(value).val();
            //console.log(key, val);
            if(val == null || val == "") {
                data[key] = null;
                return;
            }
            data[key] = StringToDataType($(value).val(), $(value).attr("datatype"));
        });
        var pr = $("#ProfessionId").find("option:selected").text();
        if (pr == ""){
            pr = "全部";
        }
        config.options.title.text = "项目:"+ProjectName+"/"+$("#Table").find("option:selected").text()+"/专业:"+pr+"/成熟度状态分布图";
        o.donutRefresh(data); //刷新分布图
    };
    return o;
};