/**
 * Created by dawxy on 2017/5/15.
 */
var page = undefined;
var ProjectId  = undefined;
var bop = undefined;
$(document).ready(function(){
    var _GET = GET();
    ProjectId = parseInt(_GET["id"],10);
    page = newpage();
    page.init();
});

var mpfieldName = {};//字段对应的中文描述
var newpage = function() {
    var o = new Object();

    var tableBaseHtml = undefined;
    var wheresAuto = undefined;
    var init_ary = [];//所有字段
    o.init = function () {
        tableBaseHtml = $("#basetable").html();
        mAjax(
            "Part/QueryAllPartField/",
            {Token: $.cookie("Token")},
            function(info) {
                if (!info.status) {
                    mAlert(info.message);
                }
                var select2_ary = [];
                var lasttb = undefined;
                var last2 = undefined;
                var group = undefined;
                var group2 = undefined;
                for (var i = 0; i < info.data.length; i++){
                    init_ary.push(info.data[i].Field);
                    //console.log(info.data[i].Field, info.data[i].FieldName);
                    mpfieldName[info.data[i].Field] = info.data[i].FieldName;
                    var table = info.data[i].TableName;
                    if (lasttb != table){
                        group = {
                            text:table,
                            children:[],
                        };
                        select2_ary.push(group);
                    }
                    var strs = info.data[i].FieldName.split("_");
                    if (strs.length > 1){//包含二级分类
                        if (last2 != strs[0]){
                            group2 = {
                                text:strs[0],
                                children:[],
                            };
                            group.children.push(group2);
                        }
                        group2.children.push({id:info.data[i].Field, text:strs[1]});
                        last2 = strs[0];
                    }else{
                        group.children.push({id:info.data[i].Field, text:strs[0]});
                        last2 = undefined;
                        group2 = undefined;
                    }
                    lasttb = table;
                    //$("#s2base").append("<option value="+info.data[i].Field+">"+info.data[i].TableName+"__"+info.data[i].FieldName+"</option>");
                }

                $("#s2base").select2({data: select2_ary});
                page.LoadTable();
            }
        );
        $("#s2base").change(function () {
            //alert($(this).val());
            page.LoadTable($(this).val());
        });
        wheresAuto = NewAutoUpdate('wheres');//获取所有datakey
        //初始化筛选条件
        loadSelect2({
            selectId:$("#ProfessionId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "Part/QueryProfessionList/",
            data: {ProjectId:ProjectId},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
        loadSelect2({
            selectId:$("#DevelopmentTypeId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "Part/QueryDevelopmentTypeList/",
            data: {ProjectId:ProjectId},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
        loadSelect2({
            selectId:$("#DesignerEngId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "User/QueryUserList/",
            data: {RoleId:2},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#BuyerEngId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "User/QueryUserList/",
            data: {RoleId:3},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#SQEEngId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "User/QueryUserList/",
            data: {RoleId:4},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#TestEngId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "User/QueryUserList/",
            data: {RoleId:6},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        loadSelect2({
            selectId:$("#CrefftauEngId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "User/QueryUserList/",
            data: {RoleId:5},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
    }

    o.SelectAll= function () {
        //默认全选
        $("#s2base").val(init_ary);
        $("#s2base").trigger('change');
    }

    o.LoadTable = function (fields, url) { //重新渲染,加载表格
        $("#basetable").html(tableBaseHtml);
        bop = {
            id:$('#mytable'),
            search: false,
            showColumns:false,
            showRefresh:false,
            showToggle:false,
            pageList: [10,100,500,1000,2000,100000],
           // url:"Part/QueryPartDynamicList/",
            data:{
                ProjectId: ProjectId,
                SelectFields:fields,
                Wheres:[],
            },
           columns:[],
        };

        bop.url = url;
        var exis = {};
        if (fields != undefined){ //设置列(返回字段)
            for(var i = 0; i < fields.length; i++){
                var tmp = {field:fields[i], title:mpfieldName[fields[i]]};
                bop.columns.push(tmp);
                exis[fields[i]] = true;
            }
        }
        if (bop.data.SelectFields == null){
            bop.data.SelectFields = [];
        }
        $.each(wheresAuto.bind, function(key, value){//获取所有筛选条件
            var val = $(value).val();
            if(val == null || val == "") return;
            bop.data.Wheres.push({Field:key,Val:val});
            //把需要筛选的不存在的字段添加到返回字段中
            if (!exis[key]){
                exis[key] = true;
                bop.data.SelectFields.push(key);
            }
        });
        loadBootstrapTable(bop);
        $("#mytable").on('refresh-options.bs.table', function (params, status) {
            //console.log(params);
            params.url = "Part/QueryPartDynamicList/";
        });
    };

    o.Reset = function () {
        $.each(wheresAuto.bind, function(key, value) {//遍历
            $(value).val("");
            $(value).change();
        });
        $("#s2base").val("");
        $("#s2base").change();
    };
    o.Search = function () {//请求查询
        //$('#mytable').bootstrapTable("refresh",{url:syprefix+"Part/QueryPartDynamicList/"});
        o.LoadTable($("#s2base").val(), "Part/QueryPartDynamicList/");
    };
    return o;
};