/**
 * Created by dawxy on 2017/5/16.
 */
var _GET = undefined;
var PartId = undefined;
var Cam = "Part";
var LoadedFields = function () {};//left加载完回调
$(document).ready(function(){
    _GET = GET();
    Id = parseInt(_GET["ProjectId"],10);
    PartId = parseInt(_GET["partid"],10);
    $("#backlist").attr("href","/project/part?id="+Id);
    var leftJs = newleft();
    leftJs.checkLeftStatus();
    //获取项目名称
    mAjax(
        "Part/QueryPart_Base/",
        {Token: $.cookie("Token"),PartId:PartId},
        function(info){
            if (!info.status){
                mAlert(info.message);
            }
            $('#leftpartname').text(info.data.Name);
            $('#leftPartCode').text(info.data.PartCode);
            if(_GET["history"] == "true"){
                $("#backlist").attr("href","history.html?id="+Id)
                $("#backlistdesc").text("返回查询操作历史");
                // $("#leftpartname").append("</br><span style='color: red'>(历史记录)</span>")
            }
        }
    );

});

function HasAnyPower(ApiName, f, cam) { //是否有某个阶段的任意一个字段的修改权限
    mAjax(
        "Part/CheckUpFieldPower/",
        {Token: $.cookie("Token"),PartId:parseInt(_GET["partid"],10), ApiName:ApiName},
        function(info){
            //console.log()
            if (info.status && info.data.Fields != null && info.data.Fields.length > 0){
                f(true,cam);
                return;
            }
            f(false,cam);
        }
    );
}
var nowplen = 0; //当前页面p标签数
var pre = "";//p标签Id前缀
//右侧界面请求获取权限接口后回调
function LeftPowerCallBack(info) {
    //console.log(info);
    var tmpReg = {};
    $.each(info.data.Fields, function (i, value) {
        tmpReg[value] = true;
        //console.log(value);
    });
    for(var i = 1; i <= nowplen; i++){
        var bind = GetAllDateKey("#p"+i);
        var finded = false;
        $.each(bind, function(key, value) {
            //console.log(i, key);
            if(tmpReg[key] == true){
                finded = true;
            }
        });
        if(finded){
            var old = $("#"+pre+i).children().children().attr("style");
            $("#"+pre+i).children().children().attr("style", old+";color: #33A5FF");
            $("#"+pre+i).children().children().attr("class", "fa fa-circle");
            //console.log(pre+i);
        }
    }

}
var newleft = function () {
    var o = new Object();
    var cache = {};
    var inithtml = $("#leftUl").html();
    o.reset = function () {
        $("#leftUl").html(inithtml);
    }
    o.initView = function (info) {
        var url = window.document.location.href.toString();
        var u = url.split("?");
        var gets = "";
        if (typeof(u[1]) == "string"){
            gets = u[1];
        }
        nowplen = 0;
        //获取所有阶段信息
        var cams = [];
        var nowCam = undefined;//当前所选阶段
        for (var i = 0; i < info.data.length; i++){
            var c = info.data[i];
            var tmpList = [];
            for(var j = 0; j < c.Tag.length; j++){
                tmpList.push(c.Tag[j].TagName);
            }
            cams.push({
                CamId:c.CamId,
                id: "Left"+c.CamId, //大的li的ID
                url:"/project/part/cam.html",
                list:tmpList,
                pre:"lp"+c.CamId, //小的li的前缀(随便定义，只要不重复即可)
            });
            //生成html
            $('#leftUl').append(
                '<li id = "'+"Left"+c.CamId+'"> <a id="a'+c.CamId+'" href="#"> <i id= "i'+c.CamId+'" class="fa fa-info-circle"></i> <span>'+c.CamName+'</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a> </li>'
            );
            //点击阶段重新载入
            $('#a'+c.CamId).click(c, function(event){
                Cam = event.data.CamId;
                o.reset();
                o.initView(info);
            });
            if (c.CamId == Cam){
                nowCam = c;
            }
        }
        LoadedFields(nowCam);//渲染加载当前Cam所选阶段的所有字段
        //生成各个TAG
        for(var ci = 0; ci < cams.length;ci++){
            var cam = cams[ci];
            var str = "<ul class='treeview-menu'>";
            var list = cam.list;
            if (cam.CamId == Cam){
                pre = cam.pre;
                nowplen = list.length;
                nowCam = cam;
                $("#"+cam.id).attr("class","active").children().attr("href","#");
            }else{
                $("#"+cam.id).children().attr("href","#");
            }
            for (var i = 0; i < list.length; i++){
                str += "<li id='"+cam.pre+(i+1)+"' ><a href=\"#p"+(i+1)+"\"><i class=\"fa fa-circle-o\"></i> "+list[i]+"</a></li>";
            }
            str += "</ul>";
            $("#"+cam.id).append(str);
            //绑定li的点击变色事件
            for (var i = 0; i < list.length; i++){
                $("#"+cam.pre+(i+1)).click(function () {
                    for (var j = 0; j < nowplen; j++){
                        //console.log("#"+pre+(j+1))
                        $("#"+pre+(j+1)).attr("class","");
                    }
                    $(this).attr("class","active");
                });
            }
            //检查每个阶段是否有任意一个可写入的字段
            if(cache[cam.CamId] == undefined){
                HasAnyPower("UpdatePart_"+cam.CamId,function (ok,c) {
                    if (ok){
                        cache[c.CamId] = true;
                        $("#i"+c.CamId).attr("style","color: #33A5FF");
                    }else{
                        cache[c.CamId] = false;
                    }
                },cam);
            }else{
                if(cache[cam.CamId]){
                    $("#i"+cam.CamId).attr("style","color: #33A5FF");
                }
            }
        }
        //设置滚动变更TAG状态
        var s = $("#scrollContent");
        var scrollfunc = function () {
            nowTop = s.scrollTop();
            var nowp = 0;
            for(var i = 1; i <= nowplen; i++){
                tmp = $("#p"+i).offset().top-$("#BaseForm").offset().top;
                if (tmp <= nowTop){
                    nowp = i;
                }
                //console.log("#"+pre+i,i,tmp, nowp)
                $("#"+pre+i).attr("class","");
            }
            $("#"+pre+nowp).attr("class","active");
            //console.log(nowp,nowTop);
        }
        s.scroll(function () {
            scrollfunc();
        });
        scrollfunc();
    }
    //检查左部菜单
    o.checkLeftStatus = function () {
        //获取项目名称
        mAjax(
            "Part/QueryAllCams/",
            {Token: $.cookie("Token")},
            function(info){
                if (!info.status){
                    mAlert(info.message);
                }
                o.initView(info);
            }
        );

    }
    return o;
};
