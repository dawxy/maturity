/**
 * Created by dawxy on 2017/5/16.
 */
var _GET = undefined;
var HasEngs = {};//该项目下拥有的角色
var HasEngsFinish = function () {};
$(document).ready(function(){
    _GET = GET();
    var leftJs = newleft();
    leftJs.checkLeftStatus();
    Id = parseInt(_GET["id"],10);
    //获取当前用户在该项目下拥有的所有角色
    mAjax(
        "Power/CheckAllowPower/",
        {Token:$.cookie("Token"), ProjectId: Id},
        function(info){
            //console.log(info.data)
            if(info.status){
                for(var i = 0 ; i < info.data.length; i++){
                    HasEngs[info.data[i]] = true;
                }
                //根据权限显示列表
                if (HasEngs["Admin"]){
                    $("#LeftTecEng").show();
                }
                if (HasEngs["Admin"] || HasEngs["TecEng"]){
                    $("#LeftSystemEng").show();
                }
                if (HasEngs["Admin"] || HasEngs["TecEng"]){
                    $("#LeftCdtEng").show();
                }
                HasEngsFinish();
            }
        }
    );
    //获取项目名称
    mAjax(
        "Project/QueryProjectInfo/",
        {Token: $.cookie("Token"),Id:Id},
        function(info){
            if (!info.status){
                mAlert(info.message);
            }
            $('#leftprojectname').text(info.data.Name);
        }

    );
});

var newleft = function () {
    var o = new Object();
    //检查左部菜单
    o.checkLeftStatus = function () {
        if (isPathnamePre("/project/part/index.html") || window.location.pathname == "/project/part/"){
            $("#LeftPart").attr("class","active").children().attr("href","#");
        }else{
            $("#LeftPart").children().attr("href","/project/part?id="+_GET["id"]);
        }

        if (isPathnamePre("/project/part/TecEng.html")){
            $("#LeftTecEng").attr("class","active").children().attr("href","#");
        }else{
            $("#LeftTecEng").children().attr("href","/project/part/TecEng.html?id="+_GET["id"]);
        }

        if (isPathnamePre("/project/part/SystemEng.html")){
            $("#LeftSystemEng").attr("class","active").children().attr("href","#");
        }else{
            $("#LeftSystemEng").children().attr("href","/project/part/SystemEng.html?id="+_GET["id"]);
        }

        if (isPathnamePre("/project/part/CdtEng.html")){
            $("#LeftCdtEng").attr("class","active").children().attr("href","#");
        }else{
            $("#LeftCdtEng").children().attr("href","/project/part/CdtEng.html?id="+_GET["id"]);
        }

        if (isPathnamePre("/project/part/search.html")){
            $("#LeftSearch").attr("class","active").children().attr("href","#");
        }else{
            $("#LeftSearch").children().attr("href","/project/part/search.html?id="+_GET["id"]);
        }

        if (isPathnamePre("/project/part/history.html")){
            $("#LeftHistory").attr("class","active").children().attr("href","#");
        }else{
            $("#LeftHistory").children().attr("href","/project/part/history.html?id="+_GET["id"]);
        }

        if (isPathnamePre("/project/part/statistics.html") || isPathnamePre("/project/part/maturity.html")){
            $("#LeftStatistics").attr("class","active").children().attr("href","#");
            $("#LeftStatisticsTree").attr("class","treeview-menu");
            $("#LeftStatisticsTree").show();
        }else{
            $("#LeftStatistics").children().attr("href","/project/part/statistics.html?id="+_GET["id"]);
            $("#LeftStatistics").children().attr("target","_blank");
            $("#LeftStatisticsTree").attr("class","");
            $("#LeftStatisticsTree").hide();
        }

        if (isPathnamePre("/project/part/statistics.html")){
            $("#lpRate").attr("class","active").children().attr("href","#");
        }else{
            $("#lpRate").children().attr("href","/project/part/statistics.html?id="+_GET["id"]);
        }

        if (isPathnamePre("/project/part/maturity.html")){
            $("#lpMaturity").attr("class","active").children().attr("href","#");
        }else{
            $("#lpMaturity").children().attr("href","/project/part/maturity.html?id="+_GET["id"]);
        }
    }
    return o;
};
