/**
 * Created by dawxy on 2017/5/15.
 */
var page = undefined;
var ProjectId  = undefined;
var bop = undefined;
$(document).ready(function(){
    var _GET = GET();
    ProjectId = parseInt(_GET["id"],10);
    page = newpage();
    page.init();
});

var mpfieldName = {};//字段对应的中文描述
var newpage = function() {
    var o = new Object();

    var wheresAuto = undefined;
    o.init = function () {
        mAjax(
            "Part/QueryAllPartField/",
            {Token: $.cookie("Token")},
            function(info) {
                if (!info.status) {
                    mAlert(info.message);
                }
                var select2_ary = [];
                var lasttb = undefined;
                var last2 = undefined;
                var group = undefined;
                var group2 = undefined;
                for (var i = 0; i < info.data.length; i++){
                    //console.log(info.data[i].Field, info.data[i].FieldName);
                    mpfieldName[info.data[i].Field] = info.data[i].FieldName;
                    var table = info.data[i].TableName;
                    if (lasttb != table){
                        group = {
                            text:table,
                            children:[],
                        };
                        select2_ary.push(group);
                    }
                    var strs = info.data[i].FieldName.split("_");
                    if (strs.length > 1){//包含二级分类
                        if (last2 != strs[0]){
                            group2 = {
                                text:strs[0],
                                children:[],
                            };
                            group.children.push(group2);
                        }
                        group2.children.push({id:info.data[i].Field, text:strs[1]});
                        last2 = strs[0];
                    }else{
                        group.children.push({id:info.data[i].Field, text:strs[0]});
                        last2 = undefined;
                        group2 = undefined;
                    }
                    lasttb = table;
                    //$("#s2base").append("<option value="+info.data[i].Field+">"+info.data[i].TableName+"__"+info.data[i].FieldName+"</option>");
                }

                $("#s2base").select2({data: select2_ary});
                //初始化表格
                bop = {
                    id:$('#mytable'),
                    url:"Part/QueryPartUpdateHistoryList/",
                    search: false,
                    showRefresh:false,
                    data:{ProjectId:ProjectId},
                    columns: [{
                        field:"PartName",
                        title: '零件名称<i class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="零件详情(只读,包括已删除的零件)"></i>',
                        formatter:function(value, row, index){
                            var history = "";
                            history = "history=true&";
                            return "<a href='cam.html?"+history+"ProjectId="+ProjectId+"&partid="+row.PartId+"' target='_blank'>"+"("+row.PartId+")"+value+"</a>";
                        }
                    },{
                        field: 'Type',
                        title: '操作',
                        formatter:function(value, row, index){
                            if(value == "1"){
                                return "修改";
                            }else if(value == "2"){
                                return "<span style='color: red'>删除</span>";
                            }
                            return "-";
                        }
                    },{
                        field: 'TableName',
                        title: '阶段'
                    },{
                        field: 'FieldName',
                        title: '修改字段'
                    },{
                        field: 'OldValue',
                        title: '原值'
                    }, {
                        field: 'Value',
                        title: '修改后值'
                    },{
                        field: 'UpUser',
                        title: '修改人',
                        formatter:function(value, row, index){
                            return value+"("+row.UpJobNumber+")";
                        }
                    },{
                        field: 'Ip',
                        title: '操作IP'
                    },{
                        field: 'UpTime',
                        title: '修改时间'
                    },
                    ]
                }
                loadBootstrapTable(bop);
            }
        );
        wheresAuto = NewAutoUpdate('wheres');//获取所有datakey
        //初始化筛选条件
        loadSelect2({
            selectId:$("#PartId"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "Part/QueryPartList/",
            data: {ProjectId:ProjectId},
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.PartCode+")";
            }
        });
        loadSelect2({
            selectId:$("#UpUserJobNumber"),
            selectedRow:{id:"", text:"全部"},
            extrows:[{id:"", text:"全部"}],
            url: "User/QueryUserList/",
            data: {ProjectId:ProjectId},
            dataMapFunc:function (val) {
                val.id = val.JobNumber;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
        $("#Table").select2();
        $("#Type").select2();
        //初始化时间范围
        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 1, //时间间隔(秒)
            timePicker24Hour: true,
            timePickerSeconds:true,
            showDropdowns:true,
            autoApply:true,//自动应用选择值
            linkedCalendars:false,//是否始终显示连续的两个月
            locale:{
                applyLabel: '确认',
                cancelLabel: '取消',
                fromLabel : '起始时间',
                toLabel : '结束时间',
                customRangeLabel : '自定义',
                firstDay : 1,
                format: 'YYYY-MM-DD HH:mm:ss',
                separator : ' 至 '
            },
            ranges : {
                '最近1小时': [moment().subtract(1, 'hours'), moment()],
                '今日': [moment().startOf('day'), moment()],
                '昨日': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
                '最近7日': [moment().subtract(6, 'days'), moment()],
                '最近30日': [moment().subtract(29, 'days'), moment()],
                '本月': [moment().startOf("month"),moment().endOf("month")],
                '上个月': [moment().subtract(1,"month").startOf("month"),moment().subtract(1,"month").endOf("month")]
            },

            opens : 'right',	// 日期选择框的弹出位置
            showWeekNumbers : false,		// 是否显示第几周
        });
        o.reservationtimeClear();
    }
    o.reservationtimeClear = function () {
        $('#reservationtime').val("");
    }

    o.Reset = function () {
        $.each(wheresAuto.bind, function(key, value) {//遍历
            $(value).val("");
            $(value).change();
        });
    };
    o.Search = function () {//请求查询
        var data = {ProjectId:ProjectId};
        $.each(wheresAuto.bind, function(key, value){//获取所有筛选条件
            var val = $(value).val();
            if(val == null || val == "") {
                if (key == "RangeTime"){
                    data["StartTime"] = data["EndTime"] = null;
                }
                data[key] = null;
                return;
            }
            if (key == "RangeTime"){
                str = val.split(" 至 ");
                data["StartTime"] = str[0];
                data["EndTime"] = str[1];
            }else{
                data[key] = StringToDataType($(value).val(), $(value).attr("datatype"));
            }

        });
        //console.log(data);
        //$('#mytable').bootstrapTable("load", data);
        $('#mytable').bootstrapTable('refreshOptions', {pageNumber:1}).bootstrapTable("refresh", {query:data});
    };
    return o;
};