/**
 * Created by dawxy on 2017/5/16.
 */

$(document).ready(function(){
    if (CheckAllowPower(["查询系统日志"])){
        $("#LeftSyslogs").show();
    }
    var leftJs = newleft();
    leftJs.checkLeftStatus();
});

var newleft = function () {
    var o = new Object();
    //检查左部菜单
    o.checkLeftStatus = function () {
        if (isPathnamePre("/project/index.html") || window.location.pathname == "/project/"){
            $("#LeftProject").attr("class","active").children().attr("href","#");
        }
        if (isPathnamePre("/project/syslogs.html")){
            $("#LeftSyslogs").attr("class","active").children().attr("href","#");
        }
    }
    return o;
};
