/**
 * Created by dawxy on 2017/5/15.
 */
var projectindex = undefined;
$(document).ready(function(){
    if (CheckAllowPower(["添加项目"])){
        $("#addporject").show();
    }
    projectindex = newprojectindex();
    projectindex.init();
    bop = {
        id:$('#mytable'),
        url:"Project/QueryProjectList/",
        columns:[{
            field:"Code",
            title: '项目代码',
            formatter:function(value, row, index){
                return "<a href='part/?id="+row.Id+"'>"+value+"</a>";
            }
        },{
            field: 'Name',
            title: '项目名称',
            formatter:function(value, row, index){
                return "<a href='part/?id="+row.Id+"'>"+value+"</a>";
            }
        },{
            field: 'Admin',
            title: '项目管理员<i class=\"fa fa-fw fa-question-circle\" data-toggle="tooltip" title="有管理项目和项目技术集成工程师的权限"></i>'
        }, {
            field: 'RegTime',
            title: '创建时间',
        },
        ]
    };
    if (CheckAllowPower(["修改项目信息"])){
        bop.columns.push({
            field: 'Id',
            title: '修改',
            formatter:function(value, row, index){
                return "<button onclick='projectindex.Update("+value+")' class='btn btn-primary'>修改</button>";
            }
        });
    }
    if (CheckAllowPower(["删除项目"])){
        bop.columns.push({
            field: 'Id',
            title: '删除',
            formatter:function(value, row, index){
                return "<button onclick='projectindex.Del("+value+")' class='btn btn-danger'>删除</button>";
            }
        });
    }
    loadBootstrapTable(bop);
});

var newprojectindex = function() {
    var o = new Object();
    var AutoUpdate = undefined;
    var UpdateAutoUpdate = undefined;
    o.init = function () {
        AutoUpdate = NewAutoUpdate('AddForm');
        AutoUpdate.UpdateUrl = "Project/AddProject/";
        AutoUpdate.messageBox = $('#AddmessageBox');
        AutoUpdate.data = {
            Token: $.cookie("Token"),
        };
        $('#AddDialog').on('hidden.bs.modal', function (e) {//隐藏后重置
            AutoUpdate.reset();
            $('#mytable').bootstrapTable('refresh');//刷新table
        });


        UpdateAutoUpdate = NewAutoUpdate('UpdateForm');
        UpdateAutoUpdate.GetUrl = "Project/QueryProjectInfo/";
        UpdateAutoUpdate.UpdateUrl = "Project/UpdateProject/";
        UpdateAutoUpdate.messageBox = $('#UpdatemessageBox');
        UpdateAutoUpdate.data = {
            Token: $.cookie("Token"),
        };
        $('#UpdateDialog').on('hidden.bs.modal', function (e) {//隐藏后重置
            UpdateAutoUpdate.reset();
            $('#mytable').bootstrapTable('refresh');//刷新table
        });
    }

    //添加对话框
    o.Add = function (id) {
        $('#AddDialog').modal('show');
        loadSelect2({
            selectId:$("#AdminId"),
            placeholder:"请选择一个用户(可按照工号或者姓名搜索)",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
    }
    //确认添加
    o.OnAdd = function (){
        AutoUpdate.UpdateData();
    }
    var delId = undefined;
    //点击弹出删除部门对话框
    o.Del = function (id) {
        delId = id;
        $('#DelDialog').modal('show');
    }
    //删除角色
    o.OnDel= function () {
        mAjax(
            "Project/DelProject/",
            {Token: $.cookie("Token"),Id:delId},
            function(info){
                if (!info.status){
                    mAlert(info.message);
                }
                $('#mytable').bootstrapTable('refresh');//刷新table
            }

        );
    }

    //点击弹出修改对话框
    o.Update = function (id) {
        UpdateAutoUpdate.data.Id = id;
        $('#UpdateDialog').modal('show');
        UpdateAutoUpdate.GetData();
    }
    //点击确认修改
    o.OnUpdate = function () {
        UpdateAutoUpdate.UpdateData();
    }
    //AdminID字段获取数据时对应的页面展示
    o.AdminIDToView = function (bind, val, data) {
        $(bind).val(val).trigger("change");
        //设置加载选项方式
        loadSelect2({
            selectedRow:{id:val, text: data.Admin+"("+data.AdminJobNumber+")"},
            selectId:$("#UpdateAdminId"),
            placeholder: data.Admin+"("+data.AdminJobNumber+")",
            url: "User/QueryUserList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name+"("+val.JobNumber+")";
            }
        });
    }

    return o;
};