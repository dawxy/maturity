/**
 * Created by dawxy on 2017/5/9.
 */
var syprefix = "/api/"
function mAlert(msg) {
    $('#dialogBody').text(msg);
    $('#myModal').modal('show');
}

function mAjax(url, data, successfunc){
    $.ajax({
        type: "POST",
        url: syprefix+url,
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json",
        success:successfunc,
        error:function(jqXHR, textStatus,errorThrown) {
            mAlert(errorThrown);
        },
    });
}

function GET(){
    var _GET = (function(){
        var url = window.document.location.href.toString();
        var u = url.split("?");
        if(typeof(u[1]) == "string"){
            u = u[1].split("&");
            var get = {};
            for(var i in u){
                var j = u[i].split("=");
                get[j[0]] = j[1];
            }
            return get;
        } else {
            return {};
        }
    })();
    return _GET;
}

function IsUserLogined() {
    Token = $.cookie("Token");
    if (Token == null || Token == undefined || Token == ""){
        return false;
    }
    return true;
}

function LoginOut() {
    $.removeCookie("Token",  { path: '/' });

    //console.log($.cookie("Token"))
    window.location.href="/login.html?backurl="+encodeURIComponent(window.location.href);
}

function isPathnamePre(str) {
    return window.location.pathname.indexOf(str) == 0;
}
//绑定修改接口
function NewAutoUpdate(formid, FieldIndex) {
    var o = new Object();
    o.root = $('#'+formid); //根DOM
    o.basehtml = $(o.root).html();//初始html
    o.bind = {}; //需要绑定的控件
    o.loacldata =  {}; //绑定控件对应的初始数据(提交修改时对比是否改变)

    o.GetUrl = "";//获取数据请求地址
    o.GetPowerUrl = "";//获取数据请求地址
    o.UpdateUrl = "";//修改数据请求地址
    o.OnblurUpdate = false;//失去焦点就请求修改
    o.data = {};//请求参数
    o.messageBox = undefined;//消息输出控件
    o.updateSuccessBackcall = undefined;//修改成功回调
    o.resetSuccessBackcall = undefined;//重置成功回调
    o.InfoMsg = undefined; //消息提示函数
    o.PowerCallBack = function (info) {}; //查询权限列表后回调
    o.GetDataCallBack = function (info) {}; //查询列表后回调

    pp = $(o.root).parent().parent()
    pp.addClass("box")
    pp.append("<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>")
    $(".overlay").hide()
    //从服务器获取数据
    o.GetData = function () {
        if(o.GetUrl == ""){
            return;
        }
        o.reset();
        $(o.messageBox).hide();
        $(".overlay").show();//显示等待加载
        $.ajax({
            type: "POST",
            url: syprefix+o.GetUrl,
            data: JSON.stringify(o.data),
            dataType: "json",
            contentType: "application/json",
            success:function(info){
                if (info.status){
                    //给控件赋值
                    $.each(info.data, function(key, value) {
                        o.loacldata[key] = value;
                        var toview = $(o.bind[key]).attr("data-to-view-func");
                        if (toview == undefined){
                            $(o.bind[key]).val(value);//不存在自定义函数则直接设置VAL
                            $(o.bind[key]).change();
                            return;
                        }
                        eval(toview+"(o.bind[key],value, info.data)");
                        $(o.bind[key]).val(value);
                    });
                    o.GetPower();
                }else{
                    o.ErrorInfo(info.message);
                }
                o.GetDataCallBack(info);
            },
            error:function(jqXHR, textStatus,errorThrown) {
                $(o.root).html(errorThrown);
            },
            complete:function (XHR, TS) {
                $(".overlay").hide();
            },
        });

    }

    //从服务器获取可修改字段列表
    o.GetPower = function () {
        if(o.GetPowerUrl == ""){
            return;
        }
        var oldstyle = {};
        $.each(o.bind, function(key, value) {
            $(value).attr("disabled","disabled");
            oldstyle[key] = $(value).attr("style");
            $(value).attr("style","background-color: #eee");
        });
        if(o.GetPowerUrl == "#"){
            return;
        }
        $.ajax({
            type: "POST",
            url: syprefix+o.GetPowerUrl,
            data: JSON.stringify(o.data),
            dataType: "json",
            contentType: "application/json",
            success:function(info){
                if (info.status){
                    //console.log(info.data)
                    //给控件赋值
                    $.each(info.data.Fields, function(i, DataKey) {
                        ob = $(o.bind[DataKey]);
                        if(FieldIndex[DataKey] == undefined){//如果是带Id后缀的去掉id
                            DataKey = DataKey.substr(0,DataKey.length-2);
                        }
                        //console.log(DataKey,FieldIndex)
                        var f = FieldIndex[DataKey].Show.Field;
                        if(f != "" && $(o.bind[f]).val() != FieldIndex[DataKey].Show.Val){ //检查是否能被显示
                            return
                        }
                        ob.removeAttr("disabled");
                        if(ob.attr("style") == "background-color: #eee"){
                            seted = oldstyle[ob.attr("DataKey")];
                            if (seted == undefined){
                                seted = "";
                            }
                            ob.attr("style",seted);
                        }

                    });
                    //对于没有权限的字段加上所需权限提示
                    $.each(info.data.NotAllowFields, function(i, value) {
                        var ob = $(o.bind[value.Field]);
                        ob.parent().attr("data-toggle", "tooltip");
                        var EngName = value.UpEng;
                        if (value.UpEng == "DesignerEng"){
                            EngName = "设计工程师";
                        }else if (value.UpEng == "BuyerEng"){
                            EngName = "采购工程师";
                        }else if (value.UpEng == "SQEEng"){
                            EngName = "SQE工程师";
                        }else if (value.UpEng == "CrefftauEng"){
                            EngName = "工艺工程师";
                        }else if (value.UpEng == "TestEng"){
                            EngName = "试验工程师";
                        }else if (value.UpEng == "SystemEng"){
                            EngName = "本专业系统集成经理";
                        }else if (value.UpEng == "TecEng"){
                            EngName = "整车技术集成工程师";
                        }
                        ob.parent().attr("title", "您没有权限,需要["+EngName+"]才可修改此字段");
                    });
                }else{
                    console.log(info.message);
                }
                o.PowerCallBack(info);
            },
            error:function(jqXHR, textStatus,errorThrown) {
                $(o.root).html(errorThrown);
            }
        });

    }


    //请求修改
    o.UpdateData = function () {
        if(o.UpdateUrl == ""){
            return;
        }
        var updata = $.extend(true , {} , o.data);//强制按值传递
        var cnt = 0;
        //检查哪些控件的值已修改
        $.each(o.bind, function(key, value) {
            //console.log($(value).val()+"---"+o.loacldata[key]);
            //console.log(key+"--"+o.loacldata[key]);
            var todata = $(o.bind[key]).attr("view-to-data-func");
            var nowval = undefined;
            if (todata == undefined){//不存在自定义函数
                nowval = StringToDataType($(value).val(), $(value).attr("datatype"));
                //console.log(key, $(value).val(), nowval, o.loacldata[key], nowval != o.loacldata[key])
            }else{
                nowval = eval(todata+"(value)");
            }

            if (nowval != o.loacldata[key]){
                updata[key] = nowval;
                cnt++;
            }
        });

        if (cnt == 0){ //没有任何修改，不提交请求
            return;
        }
        $(o.messageBox).hide();
        $(".overlay").show();//显示等待加载

        $.ajax({
            type: "POST",
            url: syprefix+o.UpdateUrl,
            data: JSON.stringify(updata),
            dataType: "json",
            contentType: "application/json",
            success:function(info){
                // o.GetData();
                if (info.status){
                    if (o.updateSuccessBackcall != undefined){
                        o.updateSuccessBackcall(info);
                    }
                    SuccessInfo(info.message);
                }else{
                    o.ErrorInfo(info.message);
                }
            },
            error:function(jqXHR, textStatus,errorThrown) {
                $(o.root).html(errorThrown);
            },
            complete:function (XHR, TS) {
                $(".overlay").hide();
            },
        });
    }

    o.reset = function () {
        o.bind = {};
        o.loacldata = {};
        $(o.root).html(o.basehtml);
        $(o.messageBox).hide();
        //找到所有需要绑定的控件
        //console.log( ($('#'+formid).children('*')))
        $(o.root).find('*').each(function(){
            var k = $(this).attr("DataKey");
            if (k == undefined){
                return;
            }
            o.bind[k] = this;
            o.loacldata[k] = $(this).val();
            if (o.OnblurUpdate){ //修改内容就请求api
                if ($(this).attr("OnchangeUpdate") == "true"){ //是否为触发onchange去请求修改api
                    $(this).change(function () {
                        if ($(this).val() != null && $(this).val() != o.loacldata[k]){
                            o.UpdateData();
                        }
                    });
                }else{
                    $(this).blur(function () {
                        o.UpdateData();
                    });
                }
            }
        });
        if (o.resetSuccessBackcall != undefined){
            o.resetSuccessBackcall();
        }
    }

    function SuccessInfo(info) {
        if (o.InfoMsg != undefined){
            o.InfoMsg(info,true);
            return
        }
        $(o.messageBox).attr("style", "text-align:center;color:green");
        $(o.messageBox).html(info);
        $(o.messageBox).show();
    }
    o.ErrorInfo = function(info) {
        if (o.InfoMsg != undefined){
            o.InfoMsg(info,false);
            return
        }
        $(o.messageBox).attr("style", "text-align:center;color:red");
        $(o.messageBox).html(info);
        $(o.messageBox).show();
    }
    o.reset();
    return o;
}

function GetAllDateKey(id) {
    //找到所有需要绑定的控件
    var bind = {};
    //console.log( ($('#'+formid).children('*')))
    $(id).find('*').each(function(){
        var k = $(this).attr("DataKey");
        if (k == undefined){
            return;
        }
        bind[k] = this;
    });
    return bind;
}
//按照datatype转换成对应格式的数据
function StringToDataType(str, datatype) {
    if(datatype == undefined || datatype == null || datatype == ""){
        return str;
    }
    if(datatype == "bool"){
        return str == "true";
    }else if(datatype == "int"){
        return parseInt(str, 10);
    }
    o.ErrorInfo("内部错误:不支持的的数据类型:"+datatype);
}
//加载select2控件数据
function loadSelect2(op) {
    var options =  {};
    options.selectedRow = undefined;
    options.selectId = undefined;
    options.placeholder = undefined;
    options.url = undefined;
    options.data = undefined;
    options.extrows = undefined;
    options.dataMapFunc = function(val){};

    options = op;
    if (options.data == undefined){
        options.data = {};
    }
    if (options.selectedRow != undefined){
        $(options.selectId).select2().select2('val', options.selectedRow.id);
        $(options.selectId).append("<option value='"+options.selectedRow.id+"'>"+options.selectedRow.text+"</option>");
        $(options.selectId).change();
    }
    //$(".select2").append("<option >Text</option>");
    var pagesize = 10;
    $(options.selectId).select2({
        placeholder: options.placeholder,
        language: "zh-CN",
        ajax: {
            method: 'post', //请求方式（*）
            url: syprefix+options.url,
            dataType: 'json',
            //delay: 250,
            contentType: "application/json",
            data: function (params) {
                params.page = params.page || 1;
                //console.log(params.page)
                options.data.Token = $.cookie("Token");
                options.data.Secrch = params.term;
                options.data.Length = pagesize;
                options.data.Start = (params.page-1)*pagesize;
                return JSON.stringify(options.data);
            },
            processResults: function (res, params) {
                params.page = params.page || 1;
                if (res.data.Elems != null){
                    len = res.data.Elems.length;
                    for (var i = 0; i < len; i++){
                        if (options.selectedRow != undefined && options.selectedRow.id == res.data.Elems[i].Id){
                            continue;
                        }
                        options.dataMapFunc(res.data.Elems[i]);
                    }
                }
                if (res.data.Elems == null){
                    res.data.Elems = [];
                }else if (params.page == 1 && options.selectedRow != undefined){
                    res.data.Elems.unshift(options.selectedRow);
                }
                // if (params.page == 1){
                //     if (options.extrows != undefined){
                //         for (var i = 0; i < options.extrows.length; i++){
                //             res.data.Elems.unshift(options.extrows[i])
                //         }
                //     }
                // }
                return {
                    results:  res.data.Elems,
                    pagination: {
                        more: (params.page * pagesize) < res.data.RecordsTotal
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    });


}

//加载表格
function loadBootstrapTable(op) {
    var options = {
        id : op.id ?  op.id: undefined,
        url :op.url ? syprefix+ op.url: undefined,
        data: op.data ?  op.data: undefined,
        columns: op.columns ?  op.columns: undefined,
        search : (op.search!=undefined) ?  op.search :true,
        showRefresh: (op.showRefresh!=undefined)?  op.showRefresh :true,
        showColumns: (op.showColumns!=undefined) ?  op.showColumns :true,
        showToggle: (op.showToggle!=undefined) ?  op.showToggle :true,
        pagination: (op.pagination!=undefined) ?  op.pagination :true,
        loadedData: op.loadedData ?  op.loadedData: undefined,
        pageList: (op.pageList!=undefined) ?  op.pageList: [10, 25, 50, 100],
    };
    //console.log(options, op)
    //options = op;
    if (options.data == undefined){
        options.data = {};
    }

    $(options.id).bootstrapTable({
        url: options.url, //请求后台的URL（*）
        method: 'post', //请求方式（*）
        dataType: 'json',
        striped: true, //是否显示行间隔色
        cache: false, //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: options.pagination, //是否显示分页（*）
        sortable: false, //是否启用排序
        showExport: true, //是否显示导出
        exportTypes:['excel'],
        queryParams: function(params) {//传递参数（*）
            options.data.Token = $.cookie("Token");
            options.data.Secrch = params.search;
            options.data.Length = params.limit;//页面大小
            options.data.Start = params.offset;//页码
            return options.data;
        },
        sidePagination: "server", //分页方式：client客户端分页，server服务端分页（*）
        pageNumber: 1, //初始化加载第一页，默认第一页
        pageSize: 10, //每页的记录行数（*）
        pageList: options.pageList, //可供选择的每页的行数（*）
        search: options.search, //是否显示表格搜索
        showColumns: options.showColumns, //是否显示 内容列下拉框
        showRefresh: options.showRefresh, //是否显示刷新按钮
        minimumCountColumns: 1, //最少允许的列数
        clickToSelect: true, //是否启用点击选中行
        uniqueId: "Id", //每一行的唯一标识，一般为主键列
        showToggle: options.showToggle, //是否显示详细视图和列表视图的切换按钮
        cardView: false, //是否显示详细视图
        detailView: false, //是否显示父子表
        responseHandler: function(res) {
            if (!res.status){
                mAlert(res.message);
                return [];
            }
            var data = {};
            data.total = res.data.RecordsTotal;
            if (res.data.Elems == null){
                data.rows = [];
            }else{
                data.rows = res.data.Elems;
            }
            if (options.loadedData != undefined){
                options.loadedData(data.rows);
            }
            //console.log(data.rows)
            return data;
        },
        // onClickRow:function(row, $element){
        //     $element.addClass("clickbackground").siblings().removeClass("clickbackground"); //单机变色
        //     console.log(row);
        //     $.cookie("ProductID",row.Product_id);
        //     $.cookie("Name",row.Name);
        // },
        // onDblClickRow: function(row, $data){
        //     $.cookie("ProductID",row.Product_id);
        //     $.cookie("Name",row.Name);
        //     window.location.href="summary-funs.html"+"?ProductID="+$.cookie("ProductID")
        // },
        columns: options.columns,
    });
}
//检查当前用户是否拥有某些权限中的任意一个
function CheckAllowPower(Powers) {
    str = $.cookie("powers");
    if (str == null || str == undefined || str == ""){
        return false;
    }
    hasp = JSON.parse(str)
    has = {};
    for (var i = 0; i < hasp.length; i++){
        has[hasp[i]] = true;
    }
    for (var i = 0; i < Powers.length; i++){
        if (has[Powers[i]]){
            return true;
        }
    }
    return false;
}