/**
 * Created by dawxy on 2017/5/9.
 */

$(document).ready(function(){
    //检查权限
    if (CheckAllowPower(["查询权限列表"])){
        $("#TopPower").show();
    }

    var header = newHeader();
    header.getUserInfo();
    $("#loginout").click(function() {
        LoginOut();
    });
    header.checkTopStatus();
    //绑定修改用户信息按钮
    AutoUpdate = NewAutoUpdate('HeadUpinfoForm');
    AutoUpdate.GetUrl = "User/QueryUserInfo/";
    AutoUpdate.UpdateUrl = "User/UpdateInfo/";
    AutoUpdate.messageBox = $('#HeadmessageBox');
    AutoUpdate.data = {
        Token: $.cookie("Token"),
    };
    $('#HeadMyInfoDialog').on('hidden.bs.modal', function (e) {//隐藏后重置AutoUpdate
        AutoUpdate.reset();
    })
    //点击弹出弹框

    $("#HeadNameAndDepartment").click(function() {
        $('#HeadMyInfoDialog').modal('show');
        //获取信息
        AutoUpdate.GetData();
        //绑定修改信息按钮
        $("#HeadUpdateInfoBtn").click(function() {
            AutoUpdate.UpdateData();
        });

    });
    // $("#HeadUpInfo").click(function() {
    //     $('#HeadMyInfoDialog').modal('show');
    //     //获取信息
    //     AutoUpdate.GetData();
    //     //绑定修改信息按钮
    //     $("#HeadUpdateInfoBtn").click(function() {
    //         AutoUpdate.UpdateData();
    //     });
    //
    // });

    //绑定修改密码按钮
    PWAutoUpdate = NewAutoUpdate('HeadUpdatePasswordForm');
    PWAutoUpdate.UpdateUrl = "User/ChangePassword/";
    PWAutoUpdate.messageBox = $('#HeadUpdatePasswordBox');
    PWAutoUpdate.updateSuccessBackcall = function (info) {//修改成功回调
        $.cookie("Token",info.data.Token, {path:"/"})//更新token
    }
    $('#HeadUpdatePasswordDialog').on('hidden.bs.modal', function (e) {//隐藏后重置AutoUpdate
        PWAutoUpdate.reset();
    })
    //点击弹出弹框
    $("#HeadUpdatePassword").click(function() {
        PWAutoUpdate.data = {
            JobNumber: header.JobNumber,
        };
        $('#HeadUpdatePasswordDialog').modal('show');
        //绑定修改密码按钮
        $("#HeadUpdatePasswordBtn").click(function() {
            if ($(PWAutoUpdate.bind["NewPassword"]).val() != $("#NewPassword2").val()){
                PWAutoUpdate.ErrorInfo("两次输入的新密码不一致");
                return;
            }
            PWAutoUpdate.UpdateData();
        });

    });
});
var newHeader = function(){
    var o = new Object();
    o.getUserInfo =  function() {
        //检查是否已经登录
        if (!IsUserLogined()){
            LoginOut();
            return;
        }
        //获取用户信息
        Token = $.cookie("Token")
        mAjax(
            "User/QueryUserInfo/",
            {Token:Token},
            function(info){
                if (info.status){
                    $("#HeadName").text(info.data.Name);
                    $("#HeadNameAndDepartment").html(info.data.Name+"-"+info.data.Department+"<small>"+info.data.JobNumber+"</small>");

                    $("#LeftName").text(info.data.Name);
                    //alert(info.data.JobNumber)
                    o.JobNumber = info.data.JobNumber;//在修改密码的时候用
                }else{
                    LoginOut();
                }
            }
        );
    }

    //检查头部菜单
    o.checkTopStatus = function() {
        if (isPathnamePre("/project")){
            $("#TopProject").attr("class","active").children();
        }
        if (isPathnamePre("/user")){
            $("#TopUser").attr("class","active").children();
        }
        if (isPathnamePre("/power")){
            $("#TopPower").attr("class","active").children();
        }
    }

    return o;
};
