/**
 * Created by dawxy on 2017/5/10.
 */

$(document).ready(function(){
    if (CheckAllowPower(["查询部门信息"])){
        $("#Leftdepartment").show();
    }
    var leftJs = newleft();
    leftJs.checkLeftStatus();
});

var newleft = function () {
    var o = new Object();
    //检查左部菜单
    o.checkLeftStatus = function () {
        if (isPathnamePre("/user/index.html") || window.location.pathname == "/user/"){
            $("#LeftAllUser").attr("class","active").children().attr("href","#");
        }
        if (isPathnamePre("/user/department.html")){
            $("#Leftdepartment").attr("class","active").children().attr("href","#");
        }
    }
    return o;
};
