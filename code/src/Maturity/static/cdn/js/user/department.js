/**
 * Created by dawxy on 2017/5/11.
 */

var department = undefined;
$(document).ready(function(){
    department = newdepartment();
    department.init();
});

var newdepartment = function() {
    var o = new Object();
    var AutoUpdate = undefined;
    var AddAutoUpdate = undefined;
    var columns = [
        {
            title: '部门名称',
            field: 'Name'
        },
    ];
    var baseTable = "";
    o.init = function () {
        AutoUpdate = NewAutoUpdate('UpInfoForm');
        AutoUpdate.GetUrl = "Department/QueryDepartmentInfo/";
        AutoUpdate.UpdateUrl = "Department/UpdateDepartment/";
        AutoUpdate.messageBox = $('#UpInfomessageBox');
        AutoUpdate.data = {
            Token: $.cookie("Token"),
        };
        $('#UpInfoDialog').on('hidden.bs.modal', function (e) {//隐藏后重置AutoUpdate
            AutoUpdate.reset();
            o.refreshTable();//刷新table
        });

        AddAutoUpdate = NewAutoUpdate('AddForm');
        AddAutoUpdate.UpdateUrl = "Department/AddDepartment/";
        AddAutoUpdate.messageBox = $('#AddmessageBox');
        AddAutoUpdate.data = {
            Token: $.cookie("Token"),
        };
        $('#AddDialog').on('hidden.bs.modal', function (e) {//隐藏后重置AutoUpdate
            AddAutoUpdate.reset();
            o.refreshTable();//刷新table
        });

        if (CheckAllowPower(["添加部门"])){
            columns.push({
                field: 'Id',
                title: '新建',
                formatter:function(value, row, index){
                    var name = "";
                    for(var i = 0; i < row.length; i++){
                        if (value == row[i].Id){
                            name = row[i].Name;
                            break;
                        }
                    }
                    return "<button onclick='department.Add("+value+",\""+name+ "\")' class='btn btn-primary'>新建</button>";
                }
            });
        }
        if (CheckAllowPower(["修改部门列表"])){
            columns.push({
                field: 'Id',
                title: '修改',
                formatter:function(value, row, index){
                    return "<button onclick='department.onclickupdate("+value+")' class='btn btn-default'>修改</button>";
                }
            });
        }
        if (CheckAllowPower(["删除部门"])){
            columns.push({
                field: 'Id',
                title: '删除',
                formatter:function(value, row, index){
                    return "<button onclick='department.onclickdel("+value+")' class='btn btn-danger'>删除</button>";
                }
            });
        }
        baseTable = $("#mytable").html();
        o.refreshTable();
    }
    //点击弹出更新部门信息对话框
    o.onclickupdate = function (id) {
        AutoUpdate.data.DepartmentId = id;
        $('#UpInfoDialog').modal('show');
        //获取信息
        AutoUpdate.GetData();
    }
    //点击更新部门对话框中的保存按钮
    o.OnUpdateSaveClick = function (){
        AutoUpdate.UpdateData();
    }

    var delId = undefined;
    //点击弹出删除部门对话框
    o.onclickdel = function (id) {
        delId = id;
        $('#DelDialog').modal('show');
    }

    //点击确认删除
    o.OkDelClick = function () {
        mAjax(
            "Department/DelDepartment/",
            {Token: $.cookie("Token"),DepartmentId:delId},
            function(info){
                if (!info.status){
                    mAlert(info.message);
                }
                o.refreshTable();;//刷新table
            }

        );
    }
    //========分页下拉框============
    o.FatherIDToView = function (bind, val, data) {
        $(bind).val(val).trigger("change");
        //设置加载选项方式
        loadSelect2({
            selectedRow:{id:val, text: data.Father},
            selectId:$("#FatherID"),
            placeholder: data.Father,
            url: "Department/QueryDepartmentList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
    }

    o.Add = function (id,name) {
        $('#AddDialog').modal('show');
        loadSelect2({
            selectId:$("#AddFatherID"),
            selectedRow:{id:id, text: name},
            url: "Department/QueryDepartmentList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
    };
    o.OnAdd = function (){
        AddAutoUpdate.UpdateData();
    }

    o.refreshTable = function(){
        $("#mytable").html(baseTable);
        $('#mytable').treegridData({
            id: 'Id',
            parentColumn: 'ParentId',
            type: "POST", //请求数据的ajax类型
            url: syprefix+'Department/QueryDepartmentList/',   //请求数据的ajax的url
            ajaxParams: {Token: $.cookie("Token"),Start:0,Length:500}, //请求数据的ajax的data属性
            expandColumn: null,//在哪一列上面显示展开按钮
            striped: true,   //是否各行渐变色
            bordered: true,  //是否显示边框
            expandAll: true,  //是否全部展开
            columns:columns,
        });
    }
    return o;
};