/**
 * Created by dawxy on 2017/5/9.
 */

var userindex = undefined;
$(document).ready(function(){
    if (CheckAllowPower(["注册用户"])){
        $("#reguser").show();
    }
    if (CheckAllowPower(["重置用户密码"])){
        $("#reset").show();
    }
    if (CheckAllowPower(["修改所有人的用户信息"])){
        $("#upuser").show();
    }
    var ShowStop = false;
    if (CheckAllowPower(["查询用户列表"])){
        ShowStop = true;
    }
    userindex = newuserindex();
    userindex.init();
    bop = {
        id:$('#mytable'),
        url:"User/QueryUserList/",
        data:{ShowStop:ShowStop, SecrchIncludeDepartmentAndPhone:true},
        columns: [{
            field:"JobNumber",
            title: '工号',
        },{
            field: 'Name',
            title: '姓名'
        },{
            field: 'Department',
            title: '部门'
        },{
            field: 'Phone',
            title: '电话'
        },
        ]
    };
    if (ShowStop){
        bop.columns.push({
            field: 'IsStop',
            title: '是否停用',
            formatter:function(value, row, index){
                if(value==true){
                    return "<span style=\"color:red\">是</span>";
                }else{
                    return "<span style=\"color:green\">否</span>";
                }
            }
        });
    }
    if (CheckAllowPower(["查询任何人的用户信息"])){
        bop.columns.push({
            field: 'Id',
            title: '修改',
            formatter:function(value, row, index){
                return "<button onclick='userindex.onclick("+value+")' class='btn btn-default'>修改</button>";
            }
        });
    }
    loadBootstrapTable(bop);

});

var newuserindex = function() {
    //配置复制到粘帖板
    $("#copybtn").popover({
        placement:"top",
        title:"已复制"
    })
    var clipboard = new Clipboard('#copybtn');
    clipboard.on('success', function(e) {
        e.clearSelection();
    });
    clipboard.on('error', function(e) {
        $('#copybtn').popover('destroy');//失败不提示
    });

    $("#resetcopybtn").popover({
        placement:"top",
        title:"已复制"
    })
    var resetcopybtn = new Clipboard('#resetcopybtn');
    clipboard.on('success', function(e) {
        e.clearSelection();
    });
    clipboard.on('error', function(e) {
        $('#copybtn').popover('destroy');//失败不提示
    });

    var o = new Object();
    var AutoUpdate = undefined;
    var AddAutoUpdate = undefined;
    o.init = function () {
        AutoUpdate = NewAutoUpdate('UpInfoForm');
        AutoUpdate.GetUrl = "User/QueryUserInfo/";
        AutoUpdate.UpdateUrl = "User/UpdateInfo/";
        AutoUpdate.messageBox = $('#UpInfomessageBox');
        AutoUpdate.data = {
            Token: $.cookie("Token"),
        };
        $('#UpInfoDialog').on('hidden.bs.modal', function (e) {//隐藏后重置AutoUpdate
            AutoUpdate.reset();
            $('#mytable').bootstrapTable('refresh');//刷新table
        });

        AddAutoUpdate = NewAutoUpdate('AddForm');
        AddAutoUpdate.UpdateUrl = "User/Reg/";
        AddAutoUpdate.messageBox = $('#AddmessageBox');
        AddAutoUpdate.data = {
            Token: $.cookie("Token"),
        };
        AddAutoUpdate.updateSuccessBackcall = addSuccessBackcall;
        $('#AddDialog').on('hidden.bs.modal', function (e) {//隐藏后重置AutoUpdate
            AddAutoUpdate.reset();
            $('#mytable').bootstrapTable('refresh');//刷新table
        });

    }

    //打开修改对话框
    o.onclick = function (id) {
        $("#ResetSuccessForm").hide();//隐藏重置密码结果
        $("[name='stop-checkbox']").bootstrapSwitch();
        AutoUpdate.data.UserId = id;
        AutoUpdate.data.UpUserId = id;
        $('#UpInfoDialog').modal('show');

        //console.log(AutoUpdate.data)
        //获取信息
        AutoUpdate.GetData();
    }

    //绑定保存按钮
    o.OnSaveClick = function (){
        AutoUpdate.UpdateData();
    }

    o.IsStopToView = function (bind, val) {
        $(bind).bootstrapSwitch('state', val, true);
    }
    o.IsStopToData = function (bind) {
        return $(bind).is(":checked");
    }
    o.DepartmentIDToView = function (bind, val, data) {
        $(bind).val(val).trigger("change");
        //设置加载选项方式
        loadSelect2({
            selectedRow:{id:val, text: data.Department},
            selectId:$("#DepartmentID"),
            placeholder: data.Department,
            url: "Department/QueryDepartmentList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });

    }

    o.Add = function (id) {
        $("#AddSuccessForm").hide();
        $('#AddDialog').modal('show');
        loadSelect2({
            selectId:$("#AddDepartmentID"),
            placeholder: "请选择一个部门(可按名称搜索)",
            url: "Department/QueryDepartmentList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
        loadSelect2({
            selectId:$("#RoleID"),
            placeholder: "请选择一个角色(可按名称搜索)",
            url: "Role/QueryRoleList/",
            dataMapFunc:function (val) {
                val.id = val.Id;
                val.text = val.Name;
            }
        });
    };
    o.OnAdd = function (){
        AddAutoUpdate.UpdateData();
    }
    //添加后回调
    function addSuccessBackcall(info) {
        $("#AddSuccessForm").show();
        $("#AddSuccessJobNumber").text("工号:"+info.data.JobNumber);
        $("#AddSuccessPassword").text("密码:"+info.data.Password);
        $("#copybtn").attr("data-clipboard-text",  "工号:"+info.data.JobNumber+"\n"+"密码:"+info.data.Password);
    }

    //点击重置密码按钮
    o.Reset = function() {
        $("#ResetSuccessForm").hide();
        mAjax(
            "User/ResetPassword/",
            {Token:Token, UserId:AutoUpdate.data.UserId},
            function(info){
                if (info.status){
                    $("#ResetSuccessForm").show();
                    $("#ResetSuccessPassword").text("新密码:"+info.data.NewPassword);
                    $("#resetcopybtn").attr("data-clipboard-text",  info.data.NewPassword);
                }else{
                    console.log(info.message);
                }
            }
        );

    }

    return o;
};

