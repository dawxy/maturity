package StatisticsCT

import (
	"Maturity/controllers"
	"Maturity/models/StatisticsMD"
	"Maturity/models/sqlmodel"
	"fmt"
	"github.com/astaxie/beego/orm"
	"reflect"
	"strconv"
	"strings"
)

type inpuCompletionRate struct {
	Token        *string `maxlen:"22"`
	ProjectId    *uint32
	Table        *string  //查询那个阶段
	SelectFields []string //需要返回哪些字段
}

var (
	//regUpApi = make(map[string]interface{})
	needStatistics = make(map[string]string) //字段是否允许统计(以及总数依赖字段)
	regTable       = make(map[string]bool)   //那些表注册过
)

func init() {
	regPartModel(sqlmodel.Cam0_1{})
	regPartModel(sqlmodel.Cam1_2{})
	regPartModel(sqlmodel.Cam2_3{})
	regPartModel(sqlmodel.Cam3_4{})
	regPartModel(sqlmodel.Cam4_5{})
	regPartModel(sqlmodel.Cam5_8{})
}

// @Summary 统计零件每个部位的完成率
// @Description 返回每个字段的所有专业完成率
// @Param _ body controllers.StatisticsCT.inpuCompletionRate true
// @Success 200 {object} models.StatisticsMD.CompletionRate
// @router /CompletionRate/ [post]
func (this *StatisticsController) CompletionRate() {
	in := inpuCompletionRate{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !regTable[*in.Table] {
		this.BackInfo.Message = "非法阶段:" + *in.Table
		return
	}
	var fields []string
	fields = append(fields, "profession_id")
	for _, v := range in.SelectFields {
		from, ok := needStatistics[v]
		if !ok {
			this.BackInfo.Message = "不允许统计的字段:" + v
			return
		}
		f := strings.Split(v, "__")
		if f[0] != *in.Table {
			this.BackInfo.Message = "字段:" + v + ", 不属于当前阶段"
			return
		}
		field := controllers.ToLower(f[1])
		if from != "" {
			fields = append(fields, "count(CASE WHEN "+field+"=1 and "+controllers.ToLower(from)+"=1 THEN 1 else NULL END ) as "+v) //已完成
			fields = append(fields, "count(CASE WHEN "+controllers.ToLower(from)+"=1 THEN 1 else NULL END) as ALL__"+v)             // 总数,需要统计对应from字段为1
		} else {
			fields = append(fields, "count(CASE WHEN "+field+"=1 THEN 1 else NULL END ) as "+v)  //已完成
			fields = append(fields, "count(CASE WHEN "+field+"=1 THEN 1 else 1 END) as ALL__"+v) //总数
		}

	}

	sql, err := orm.NewQueryBuilder("mysql")
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	sql.Select(fields...)
	sql.From(controllers.ToLower(*in.Table))
	sql.Where("project_id = ? AND deled = false")
	group, err := orm.NewQueryBuilder("mysql")
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	group.GroupBy("profession_id")
	o := orm.NewOrm()
	dinfo := StatisticsMD.CompletionRate{}
	if _, err := o.Raw(sql.String()+" "+group.String(), *in.ProjectId).Values(&dinfo.Elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	//行和列互换
	rows := make(map[string]map[string]interface{}) //每个字段对应的行结构
	for _, mp := range dinfo.Elems {
		profession := mp["profession_id"].(string)
		for k, v := range mp {
			if k == "profession_id" {
				continue
			}
			strs := strings.Split(k, "__")
			if len(strs) > 0 && strs[0] == "ALL" {
				continue
			}
			if rows[k] == nil {
				rows[k] = make(map[string]interface{})
				rows[k]["fieldName"] = k
			}
			rows[k][profession] = v
			rows[k]["ALL$"+profession] = mp["ALL__"+k]
			var x, _ = strconv.ParseInt(v.(string), 10, 32)
			var y, _ = strconv.ParseInt(mp["ALL__"+k].(string), 10, 32)
			if y > 0 {
				rows[k]["Rate$"+profession] = fmt.Sprintf("%.1f", float32(x)/float32(y)*100) + "%"
			}
		}
	}
	dinfo.Elems = nil
	for _, v := range in.SelectFields {
		dinfo.Elems = append(dinfo.Elems, rows[v])
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}

func regPartModel(m interface{}) {
	table := reflect.TypeOf(m).Name()
	regTable[table] = true
	names := reflect.TypeOf(m)
	values := reflect.ValueOf(m)
	for i := 0; i < values.NumField(); i++ {
		k := names.Field(i)
		//v := values.Field(i)
		tag := k.Tag
		if from, ok := tag.Lookup("needStatistics"); ok {
			name := table + "__" + k.Name
			if _, ok := needStatistics[name]; ok {
				panic("存在重复的字段:" + name)
			}
			needStatistics[name] = from
		}
	}
}
