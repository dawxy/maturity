package StatisticsCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/StatisticsMD"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
	"time"
)

type inputSystemLogs struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`

	Type            *string `canempty:""`
	StartTime       *string `canempty:""`
	EndTime         *string `canempty:""`
	UpUserJobNumber *string `canempty:""`
}

// @Summary 系统日志
// @Description
// @Param _ body controllers.StatisticsCT.inputSystemLogs true
// @Success 200 {object} models.StatisticsMD.QuerySystemLogs
// @router /QuerySystemLogs/ [post]
func (this *StatisticsController) QuerySystemLogs() {
	in := inputSystemLogs{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.QuerySysLogs) {
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable(&sqlmodel.SysLogs{})
	if in.Type != nil {
		qs = qs.Filter("Type", *in.Type)
	}
	if in.UpUserJobNumber != nil {
		qs = qs.Filter("UpJobNumber", *in.UpUserJobNumber)
	}
	if in.StartTime != nil && *in.StartTime != "" {
		t, err := time.ParseInLocation("2006-01-02 15:04:05", *in.StartTime, time.Local)
		if err != nil { //v.String() != "",允许设置为空时间
			this.BackInfo.Message = *in.StartTime + "不是正确的时间格式!"
			return
		}
		qs = qs.Filter("Time__gte", t)
	}
	if in.EndTime != nil && *in.EndTime != "" {
		t, err := time.ParseInLocation("2006-01-02 15:04:05", *in.EndTime, time.Local)
		if err != nil { //v.String() != "",允许设置为空时间
			this.BackInfo.Message = *in.EndTime + "不是正确的时间格式!"
			return
		}
		qs = qs.Filter("Time__lte", t)
	}
	dinfo := StatisticsMD.QuerySystemLogs{}
	var err error
	if dinfo.RecordsTotal, err = qs.Count(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	elems := []sqlmodel.SysLogs{}
	if _, err = qs.Limit(*in.Length, *in.Start).OrderBy("-Id").All(&elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	for _, e := range elems {
		elem := StatisticsMD.QuerySystemLogsElem{}
		elem.Id = e.Id
		elem.Type = e.Type
		elem.ProjectId = e.ProjectId
		elem.ProjectName = e.ProjectName
		elem.TargetUserJobNum = e.TargetUserJobNum
		elem.TargetUserJobName = e.TargetUserJobName
		elem.Value = e.Value
		elem.UpJobNumber = e.UpJobNumber
		elem.UserName = e.UserName
		elem.Ip = e.Ip
		elem.Time = e.Time.Format("2006-01-02 15:04:05")

		dinfo.Elems = append(dinfo.Elems, elem)
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
