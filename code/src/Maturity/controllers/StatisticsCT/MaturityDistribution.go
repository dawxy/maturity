package StatisticsCT

import (
	"Maturity/controllers"
	"Maturity/controllers/partCT"
	"Maturity/models/StatisticsMD"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type inpuMaturityDistribution struct {
	Token     *string `maxlen:"22"`
	ProjectId *uint32
	Table     *string //查询那个阶段

	Profession *uint32 `canempty:""`
}

// @Summary 统计每个阶段所有零件的各个成熟度数量
// @Description 返回每个字段的所有专业完成率
// @Param _ body controllers.StatisticsCT.inpuMaturityDistribution true
// @Success 200 {object} models.StatisticsMD.CompletionRate
// @router /MaturityDistribution/ [post]
func (this *StatisticsController) MaturityDistribution() {
	in := inpuMaturityDistribution{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !regTable[*in.Table] {
		this.BackInfo.Message = "非法阶段:" + *in.Table
		return
	}
	sql, err := orm.NewQueryBuilder("mysql")
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	var extArgs []interface{}
	sql.Select("maturity_state as MaturityState,count(*) as Count")
	sql.From(controllers.ToLower(*in.Table))
	sql.Where("project_id = ? AND deled = false")
	extArgs = append(extArgs, *in.ProjectId)
	if in.Profession != nil {
		sql.And("profession_id = ?")
		extArgs = append(extArgs, *in.Profession)
	}
	sql.GroupBy("maturity_state")
	o := orm.NewOrm()
	dinfo := StatisticsMD.CompletionRate{}
	if _, err := o.Raw(sql.String(), extArgs...).Values(&dinfo.Elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	//显示对应状态的中文
	for i, row := range dinfo.Elems {
		val, err := strconv.ParseInt(row["MaturityState"].(string), 10, 8)
		if err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
		dinfo.Elems[i]["MaturityState"] = partCT.GetInt8Name(int8(val))
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
