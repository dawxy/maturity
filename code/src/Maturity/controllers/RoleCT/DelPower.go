package RoleCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputDelPower struct {
	Token   *string `maxlen:"22"`
	RoleId  *uint32
	PowerId *uint32
}

// @Summary 删除角色的某个权限
// @Description
// @Param _ body controllers.RoleCT.inputDelPower true
// @Success 200 {object} string
// @router /DelPower/ [post]
func (this *RoleController) DelPower() {
	in := inputDelPower{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.DelRoleOrPower) {
		return
	}
	if CheckPower.SystemAdmin.Id == *in.RoleId {
		this.BackInfo.Message = "不能修改" + CheckPower.SystemAdmin.Name + "的权限!"
		return
	}
	o := orm.NewOrm()
	role := sqlmodel.Role{Id: *in.RoleId}
	if !o.QueryTable(&role).Filter("Id", role.Id).Exist() {
		this.BackInfo.Message = "不存在的角色"
		return
	}
	Power := sqlmodel.Power{Id: *in.PowerId}
	rolePower := sqlmodel.RolePower{Role: &role, Power: &Power}
	if !o.QueryTable(&rolePower).Filter("Role", &role).Filter("Power", &Power).Exist() {
		this.BackInfo.Message = "此角色不存在这个权限"
		return
	}
	if _, err := o.Delete(&rolePower, "Power", "Role"); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
