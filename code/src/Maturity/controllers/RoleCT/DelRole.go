package RoleCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputDelRole struct {
	Token  *string `maxlen:"22"`
	RoleId *uint32
}

// @Summary 删除一个角色
// @Description 已经有关联用户的角色和系统管理员不能删除
// @Param _ body controllers.RoleCT.inputDelRole true
// @Success 200 {object} string
// @router /DelRole/ [post]
func (this *RoleController) DelRole() {
	in := inputDelRole{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !CheckPower.CheckPower(this, *in.Token, CheckPower.DelRoleOrPower) {
		return
	}

	if *in.RoleId <= 6 { //系统管理员以及各个工程师不能删除
		this.BackInfo.Message = "不能删除此角色!"
		return
	}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	role := sqlmodel.Role{Id: *in.RoleId}
	if !o.QueryTable(&role).Filter("Id", role.Id).Exist() {
		this.BackInfo.Message = "不存在的角色"
		return
	}
	//检查是否有用户拥有这个角色
	if o.QueryTable("UserRole").Filter("Role", &role).Exist() {
		this.BackInfo.Message = "还有用户拥有此角色,请删删除所有此角色的用户权限!"
		return
	}

	//删除
	if _, err := o.Delete(&role, "Id"); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦"
		return
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
