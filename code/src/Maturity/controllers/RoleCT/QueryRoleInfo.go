package RoleCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/RoleMD"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputQueryRoleInfo struct {
	Token  *string `maxlen:"22"`
	RoleId *uint32
}

// @Summary 查询角色信息
// @Description
// @Param _ body controllers.RoleCT.inputQueryRoleInfo true
// @Success 200 {object} models.RoleMD.PowerInfo
// @router /QueryRoleInfo/ [post]
func (this *RoleController) QueryRoleInfo() {
	in := inputQueryRoleInfo{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.RoleOrPowerList) {
		return
	}

	o := orm.NewOrm()
	m := sqlmodel.Role{Id: *in.RoleId}
	if err := o.Read(&m, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的角色!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = RoleMD.PowerInfo{Id: m.Id, RoleName: m.Name, CreatTime: m.CreatTime.Format("2006-01-02 15:04:05")}
}
