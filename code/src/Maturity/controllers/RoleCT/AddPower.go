package RoleCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputAddPower struct {
	Token   *string `maxlen:"22"`
	RoleId  *uint32
	PowerId *uint32
}

// @Summary 给某个角色添加某个权限
// @Description
// @Param _ body controllers.RoleCT.inputAddPower true
// @Success 200 {object} string
// @router /AddPower/ [post]
func (this *RoleController) AddPower() {
	in := inputAddPower{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.AddRoleOrPower) {
		return
	}
	o := orm.NewOrm()
	role := sqlmodel.Role{Id: *in.RoleId}
	if !o.QueryTable(&role).Filter("Id", role.Id).Exist() {
		this.BackInfo.Message = "不存在的角色"
		return
	}
	Power := sqlmodel.Power{Id: *in.PowerId}
	if !o.QueryTable(&Power).Filter("Id", Power.Id).Exist() {
		this.BackInfo.Message = "不存在的权限"
		return
	}
	rolePower := sqlmodel.RolePower{Role: &role, Power: &Power}
	if o.QueryTable(&rolePower).Filter("Role", &role).Filter("Power", &Power).Exist() {
		this.BackInfo.Message = "此角色已经存在这个权限"
		return
	}
	if _, err := o.Insert(&rolePower); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
