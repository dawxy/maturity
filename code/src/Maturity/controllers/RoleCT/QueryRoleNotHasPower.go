package RoleCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/RoleMD"
	"github.com/astaxie/beego/orm"
)

type inputQueryRoleNotHasPower struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`
	RoleId *uint32

	Secrch *string `canempty:""`
}

// @Summary 查询角色还未拥有的权限列表
// @Description 分页查询角色还未拥有的权限列表
// @Param _ body controllers.RoleCT.inputQueryRoleNotHasPower true
// @Success 200 {object} models.RoleMD.QueryRoleAllPower
// @router /QueryRoleNotHasPower/ [post]
func (this *RoleController) QueryRoleNotHasPower() {
	in := inputQueryRoleNotHasPower{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.RoleOrPowerList) {
		return
	}
	qb, err := orm.NewQueryBuilder("mysql")
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	qbCount, err := orm.NewQueryBuilder("mysql")
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	subqb, err := orm.NewQueryBuilder("mysql")
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	extArgs := []interface{}{*in.RoleId}
	subqb.Select("role_power.power_id").From("role_power").Where("role_power.role_id = ?")
	qb.Select("id", "name").From("power").Where("id not in(" + subqb.String() + ")")
	qbCount.Select("count(*)").From("power").Where("id not in(" + subqb.String() + ")")
	if in.Secrch != nil && *in.Secrch != "" {
		qb.And("name like ?")
		qbCount.And("name like ?")
		extArgs = append(extArgs, "%"+*in.Secrch+"%")
	}

	o := orm.NewOrm()
	dinfo := RoleMD.QueryRoleAllPower{}
	//获取总的数量
	if err = o.Raw(qbCount.String(), extArgs...).QueryRow(&dinfo.RecordsTotal); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	qb.OrderBy("power.id").Asc().Limit(int(*in.Length)).Offset(int(*in.Start))
	if _, err := o.Raw(qb.String(), extArgs...).QueryRows(&dinfo.Elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
