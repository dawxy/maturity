package RoleCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputUpdateRole struct {
	Token    *string `maxlen:"22"`
	RoleId   *uint32
	RoleName *string `maxlen:"64"`
}

// @Summary 修改角色信息
// @Description
// @Param _ body controllers.RoleCT.inputUpdateRole true
// @Success 200 {object} string
// @router /UpdateRole/ [post]
func (this *RoleController) UpdateRole() {
	in := inputUpdateRole{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.UpdateRole) {
		return
	}
	if *in.RoleId <= 6 { //系统管理员以及各个工程师不能修改
		this.BackInfo.Message = "不能修改此角色!"
		return
	}

	var ups []string //需要修改的字段
	m := sqlmodel.Role{Id: *in.RoleId}
	if in.RoleName != nil {
		ups = append(ups, "Name")
		m.Name = *in.RoleName
	}

	if len(ups) > 0 {
		o := orm.NewOrm()
		if _, err := o.Update(&m, ups...); err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦!"
			return
		}
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
