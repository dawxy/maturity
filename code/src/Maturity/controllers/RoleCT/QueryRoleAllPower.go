package RoleCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/RoleMD"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputQueryRoleAllPower struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`
	RoleId *uint32

	Secrch *string `canempty:""`
}

// @Summary 查询角色拥有的权限列表
// @Description 分页查询角色拥有的权限列表
// @Param _ body controllers.RoleCT.inputQueryRoleAllPower true
// @Success 200 {object} models.RoleMD.QueryRoleAllPower
// @router /QueryRoleAllPower/ [post]
func (this *RoleController) QueryRoleAllPower() {
	in := inputQueryRoleAllPower{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.RoleOrPowerList) {
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable("RolePower").Filter("Role", in.RoleId)
	if in.Secrch != nil && *in.Secrch != "" {
		qs = qs.Filter("Power__name__icontains", *in.Secrch)
	}

	dinfo := RoleMD.QueryRoleAllPower{}
	var err error
	if dinfo.RecordsTotal, err = qs.Count(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	elems := []sqlmodel.RolePower{}
	if _, err = qs.Limit(*in.Length, *in.Start).OrderBy("-Id").RelatedSel("Power").All(&elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	for _, e := range elems {
		elem := RoleMD.PowerElem{}
		elem.Id = e.Power.Id
		elem.Name = e.Power.Name
		dinfo.Elems = append(dinfo.Elems, elem)
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
