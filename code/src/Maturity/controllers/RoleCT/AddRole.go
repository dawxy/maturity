package RoleCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputAddRole struct {
	Token    *string `maxlen:"22"`
	RoleName *string `maxlen:"64"`
}

// @Summary 新增一个角色
// @Description 添加一个角色
// @Param _ body controllers.RoleCT.inputAddRole true
// @Success 200 {object} string
// @router /AddRole/ [post]
func (this *RoleController) AddRole() {
	in := inputAddRole{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.AddRoleOrPower) {
		return
	}
	if *in.RoleName == "" {
		this.BackInfo.Message = "角色名称不能为空"
		return
	}
	o := orm.NewOrm()
	if o.QueryTable("role").Filter("Name", *in.RoleName).Exist() {
		this.BackInfo.Message = "角色名称已经存在"
		return
	}
	if _, err := o.Insert(&sqlmodel.Role{Name: *in.RoleName}); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
