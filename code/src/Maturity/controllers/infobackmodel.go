package controllers

import (
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/logs"
	"reflect"
	"runtime/debug"
)

type JSONOutPutInterface interface {
	SetBackInfo(Status bool, Message string, Data interface{}, err ...error)
	SetUserInfo(token string, user *sqlmodel.User)
}

//JSONOutPutModel 用于JSON输出
type jsonOutPutModel struct {
	Status  bool        `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func (this *BaseController) SetBackInfo(Status bool, Message string, Data interface{}, err ...error) {
	this.BackInfo.Status = Status
	this.BackInfo.Message = Message
	this.BackInfo.Data = Data
	if len(err) > 0 {
		this.ErrLog(err[0])
	}
}

func (this *BaseController) ErrLog(err error) {
	logs.Error(err, '\n', "[Input]:", string(this.Ctx.Input.RequestBody), string(debug.Stack()))
}

//InfoBack 返回格式化信息(无XSS攻击漏洞)
func (this *BaseController) infoBack() {
	this.AllowOrigin()
	m := &jsonOutPutModel{}
	m.Status = this.BackInfo.Status
	m.Message = this.BackInfo.Message
	if nil == this.BackInfo.Data {
		m.Data = ""
	} else if reflect.TypeOf(this.BackInfo.Data).Kind() == reflect.Slice && reflect.ValueOf(this.BackInfo.Data).Len() == 0 { //一级空数组
		m.Data = make([]interface{}, 0)
	} else {
		m.Data = this.BackInfo.Data
	}
	//输出JSON带过滤
	this.Data["json"] = m
	this.ServeJSON()
}
