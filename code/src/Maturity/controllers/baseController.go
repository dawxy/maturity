package controllers

import (
	"Maturity/models/sqlmodel"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"reflect"
	"strconv"
	"strings"
)

var (
	errNotAllowParam = errors.New("NotAllowParam")
)

//基础类
type BaseController struct {
	beego.Controller
	BackInfo BackInfo
	notJson  bool
}
type BackInfo struct {
	Status  bool
	Message string
	Data    interface{}
}

func (c *BaseController) SetNotBackJson() {
	c.notJson = true
}

//渲染
func (c *BaseController) Render() error {
	//api使用json返回
	if !c.notJson && strings.HasPrefix(c.Ctx.Request.URL.String(), "/api/") {
		c.infoBack()
		return nil
	}
	//其他请求使用原来的路由
	c.Controller.Render()
	return nil
}

func (c *BaseController) GetInputStruct(in interface{}) (err error) {
	c.Ctx.Input.CopyBody(beego.BConfig.MaxMemory)
	logs.Debug(string(c.Ctx.Input.RequestBody))
	if err = json.Unmarshal(c.Ctx.Input.RequestBody, in); err != nil {
		logs.Debug(err)
		c.BackInfo.Message = "输入参数非法!"
		return errNotAllowParam
	}
	//检查所有Tag的限制
	names := reflect.TypeOf(in).Elem()
	values := reflect.ValueOf(in).Elem()
	for i := 0; i < values.NumField(); i++ {
		k := names.Field(i)
		prev := values.Field(i)
		tag := k.Tag
		if prev.Kind() == reflect.Slice {
			return
		}
		if prev.Kind() != reflect.Ptr {
			panic(names.Name() + "." + k.Name + ":必须为指针类型")
		}
		v := prev.Elem()
		//检查是否为空
		_, canEmpty := tag.Lookup("canempty")
		if !canEmpty && prev.IsNil() {
			c.BackInfo.Message = k.Name + "不能为空!"
			return errNotAllowParam
		}
		switch v.Kind() {
		case reflect.String:
			cnt := v.Len()
			maxlen := int64(255)
			if value, ok := tag.Lookup("maxlen"); ok {
				maxlen, err = strconv.ParseInt(value, 10, 32)
				if err != nil {
					panic(err)
				}
			}
			if int64(cnt) > maxlen {
				c.BackInfo.Message = "输入的" + k.Name + "长度为:" + strconv.Itoa(cnt) + ",超过最大限制:" + strconv.Itoa(int(maxlen))
				return errNotAllowParam
			}
		case reflect.Int64, reflect.Int32, reflect.Int, reflect.Int8:
			if value, ok := tag.Lookup("min"); ok {
				min, err := strconv.ParseInt(value, 10, 64)
				if err != nil {
					panic(err)
				}
				if v.Int() < min {
					c.BackInfo.Message = "输入的" + k.Name + "值为:" + fmt.Sprintf("%v", v.Int()) + ",小于最小限制:" + fmt.Sprintf("%v", min)
					return errNotAllowParam
				}
			}
			if value, ok := tag.Lookup("max"); ok {
				max, err := strconv.ParseInt(value, 10, 64)
				if err != nil {
					panic(err)
				}
				if v.Int() > max {
					c.BackInfo.Message = "输入的" + k.Name + "值为:" + fmt.Sprintf("%v", v.Int()) + ",大于最大限制:" + fmt.Sprintf("%v", max)
					return errNotAllowParam
				}
			}
		case reflect.Uint64, reflect.Uint32, reflect.Uint:
			if value, ok := tag.Lookup("min"); ok {
				min, err := strconv.ParseInt(value, 10, 64)
				if err != nil {
					panic(err)
				}
				if v.Uint() < uint64(min) {
					c.BackInfo.Message = "输入的" + k.Name + "值为:" + fmt.Sprintf("%v", v.Uint()) + ",小于最小限制:" + fmt.Sprintf("%v", min)
					return errNotAllowParam
				}
			}
			if value, ok := tag.Lookup("max"); ok {
				max, err := strconv.ParseInt(value, 10, 64)
				if err != nil {
					panic(err)
				}
				if v.Uint() > uint64(max) {
					c.BackInfo.Message = "输入的" + k.Name + "值为:" + fmt.Sprintf("%v", v.Uint()) + ",大于最大限制:" + fmt.Sprintf("%v", max)
					return errNotAllowParam
				}
			}
		}
	}
	return
}

func (c *BaseController) SetUserInfo(token string, user *sqlmodel.User) {
	c.Data[token] = user
}

func (c *BaseController) GetUserInfo(token string) *sqlmodel.User {
	if c.Data[token] == nil {
		return nil
	}
	return c.Data[token].(*sqlmodel.User)
}

func (c *BaseController) AllowOrigin() { //支持跨域
	c.Ctx.Output.Header("Access-Control-Allow-Origin", "*")
}

//ImagePreURL 获取图片URL前缀
func (c *BaseController) ImagePreURL() string {
	return c.Ctx.Request.Host + "/upload/images/"
}

func ToLower(str string) string {
	var ret []byte
	for i, c := range str {
		if i >= 0 && c >= 'A' && c <= 'Z' {
			if i == 0 {
				ret = append(ret, byte(c-'A'+'a'))
			} else {
				ret = append(ret, '_', byte(c-'A'+'a'))
			}
		} else {
			ret = append(ret, byte(c))
		}
	}
	return string(ret)
}
