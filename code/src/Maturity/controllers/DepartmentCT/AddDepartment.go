package DepartmentCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/DepartmentMD"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputAddDepartment struct {
	Token    *string `maxlen:"22"`
	Name     *string `maxlen:"64"`
	FatherId *uint32
}

// @Summary 添加部门
// @Description
// @Param _ body controllers.DepartmentCT.inputAddDepartment true
// @Success 200 {object} string
// @router /AddDepartment/ [post]
func (this *DepartmentController) AddDepartment() {
	in := inputAddDepartment{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.AddDepartment) {
		return
	}

	o := orm.NewOrm()
	d := sqlmodel.Department{}
	if o.QueryTable(&d).Filter("Name", in.Name).Exist() {
		this.BackInfo.Message = "部门名称已经存在!"
		return
	}
	d.Name = *in.Name
	//确认上级部门已经存在
	ok, err := DepartmentMD.HasDepartment(*in.FatherId)
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if !ok {
		this.BackInfo.Message = "不存在的上级部门!"
		return
	}
	d.Father = &sqlmodel.Department{Id: *in.FatherId}

	if _, err := o.Insert(&d); err != nil {
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
