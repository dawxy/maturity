package DepartmentCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/DepartmentMD"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputQueryDepartmentInfo struct {
	Token        *string `maxlen:"22"`
	DepartmentId *uint32
}

// @Summary 查询部门信息
// @Description
// @Param _ body controllers.DepartmentCT.inputQueryDepartmentInfo true
// @Success 200 {object} models.DepartmentMD.QueryDepartmentInfo
// @router /QueryDepartmentInfo/ [post]
func (this *DepartmentController) QueryDepartmentInfo() {
	in := inputQueryDepartmentInfo{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.QueryDepartmentList) {
		return
	}
	o := orm.NewOrm()
	d := sqlmodel.Department{Id: *in.DepartmentId}
	if err := o.Read(&d, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的部门"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	dinfo := DepartmentMD.QueryDepartmentInfo{
		Id:        d.Id,
		Name:      d.Name,
		CreatTime: d.CreatTime.Format("2006-01-02 15:04:05"),
	}
	if d.Father != nil {
		o.LoadRelated(&d, "Father")
		dinfo.Father = d.Father.Name
		dinfo.FatherID = d.Father.Id
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
