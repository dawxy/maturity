package DepartmentCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/DepartmentMD"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type inputUpdateDepartment struct {
	Token        *string `maxlen:"22"`
	DepartmentId *uint32
	Name         *string `canempty:"" maxlen:"64"`
	FatherId     *uint32 `canempty:""`
}

// @Summary 修改部门信息
// @Description 除了Token和DepartmentId以外,不填不修改,根部门不能修改
// @Param _ body controllers.DepartmentCT.inputUpdateDepartment true
// @Success 200 {object} string
// @router /UpdateDepartment/ [post]
func (this *DepartmentController) UpdateDepartment() {
	in := inputUpdateDepartment{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.UpdateDepartment) {
		return
	}
	var ups []string //需要修改的字段
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	m := sqlmodel.Department{Id: *in.DepartmentId}
	//检查部门是否存在,切判断是否为根部门
	if err := o.Read(&m, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的部门!!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if m.Name == CheckPower.RootDepartmentName {
		this.BackInfo.Message = "根部门不能修改!!"
		return
	}

	if in.Name != nil {
		m.Name = *in.Name
		ups = append(ups, "Name")
	}
	if in.FatherId != nil {
		ok, errinfo := checkLoop(this, o, &sqlmodel.Department{}, 0, m.Id, *in.FatherId)
		if errinfo != "" {
			this.BackInfo.Message = errinfo
		}
		if !ok {
			this.BackInfo.Message = "修改失败,修改后的上级部门为当前目录下的某个子目录!"
			return
		}
		if *in.DepartmentId == *in.FatherId {
			this.BackInfo.Message = "上级部门不能为当前部门!"
			return
		}
		//确认上级部门已经存在
		ok, err := DepartmentMD.HasDepartment(*in.FatherId)
		if err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦!"
			return
		}
		if !ok {
			this.BackInfo.Message = "不存在的上级部门!"
			return
		}
		m.Father = &sqlmodel.Department{Id: *in.FatherId}
		ups = append(ups, "Father")
	}

	if len(ups) > 0 {
		if _, err := o.Update(&m, ups...); err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦!"
			return
		}
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}

func checkLoop(this *DepartmentController, o orm.Ormer, m *sqlmodel.Department, deep, upId, toId uint32) (ok bool, errinfo string) { //检测是否存在环(寻找要修改后的部门的所有父亲结点)
	if deep > 100 { //递归保护
		errinfo = "服务器出错啊:递归深度过大:" + strconv.FormatInt(int64(upId), 10) + "," + strconv.FormatInt(int64(toId), 10)
		return
	}
	if toId == upId { //有环
		return
	}
	m.Id = toId
	if err := o.Read(m, "Id"); err != nil {
		if err == orm.ErrNoRows {
			errinfo = "不存在的部门!!"
			return
		}
		this.ErrLog(err)
		errinfo = "服务器出错啦!"
		return
	}
	if m.Father == nil { //到根了
		ok = true
		return
	}
	return checkLoop(this, o, m, deep+1, upId, m.Father.Id)
}
