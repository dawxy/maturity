package DepartmentCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/DepartmentMD"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputQueryDepartmentList struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`

	FatherId *uint32 `canempty:""` //按照所属部门筛选
	Secrch   *string `canempty:""`
}

// @Summary 部门列表
// @Description
// @Param _ body controllers.DepartmentCT.inputQueryDepartmentList true
// @Success 200 {object} models.DepartmentMD.QueryDepartmentList
// @router /QueryDepartmentList/ [post]
func (this *DepartmentController) QueryDepartmentList() {
	in := inputQueryDepartmentList{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.QueryDepartmentList) {
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable("Department")
	if in.FatherId != nil {
		qs = qs.Filter("Father", &sqlmodel.Department{Id: *in.FatherId})
	}
	if in.Secrch != nil && *in.Secrch != "" {
		qs = qs.Filter("name__icontains", *in.Secrch)
	}

	dinfo := DepartmentMD.QueryDepartmentList{}
	var err error
	if dinfo.RecordsTotal, err = qs.Count(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	elems := []sqlmodel.Department{}
	if _, err = qs.Limit(*in.Length, *in.Start).OrderBy("-Id").RelatedSel("Father").All(&elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	for _, e := range elems {
		elem := DepartmentMD.DepartmentElem{}
		elem.Id = e.Id
		elem.Name = e.Name
		if e.Father != nil {
			elem.Father = e.Father.Name
			elem.ParentId = e.Father.Id
		}
		elem.CreatTime = e.CreatTime.Format("2006-01-02 15:04:05")
		dinfo.Elems = append(dinfo.Elems, elem)
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
