package DepartmentCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputDelDepartment struct {
	Token        *string `maxlen:"22"`
	DepartmentId *uint32
}

// @Summary 删除部门信息
// @Description 根部门不能删除
// @Param _ body controllers.DepartmentCT.inputDelDepartment true
// @Success 200 {object} string
// @router /DelDepartment/ [post]
func (this *DepartmentController) DelDepartment() {
	in := inputUpdateDepartment{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.DelDepartment) {
		return
	}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	m := sqlmodel.Department{Id: *in.DepartmentId}
	//检查部门是否存在,切判断是否为根部门
	if err := o.Read(&m, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的部门!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if m.Name == CheckPower.RootDepartmentName {
		this.BackInfo.Message = "根部门不能删除!"
		return
	}
	//检查该部门是否有员工
	if o.QueryTable("user").Filter("Department", *in.DepartmentId).Exist() {
		this.BackInfo.Message = "该部门下还有员工,请先移动员工后再删除!"
		return
	}
	//检查该部门是否有下级部门
	if o.QueryTable("Department").Filter("Father", *in.DepartmentId).Exist() {
		this.BackInfo.Message = "该部门下还有下级部门,请先删除下级部门后再删除!"
		return
	}
	//删除
	if _, err := o.Delete(&m, "Id"); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}

	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
