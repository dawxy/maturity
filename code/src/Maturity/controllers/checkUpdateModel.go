package controllers

import (
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
	"reflect"
	"strconv"
	"strings"
	"time"
)

//对接口输入in做数据库表更新,其中TAG未标记ignoreUpCheck的字段需要检查对应字段去掉最后两个"Id"字符的表做其id是否存在的检查
//其中对应的model是外键会检查对应表是否存在这个id
func (this *BaseController) UpdateModel(o orm.Ormer, ProjectId, PartId uint32, m interface{}, in interface{}, token string) (status bool) {
	var ups []string       //需要修改的字段
	var upvalues []string  //修改后的字段
	var oldvalues []string //修改前的字段值
	//检查所有Tag的限制
	names := reflect.TypeOf(in).Elem()
	values := reflect.ValueOf(in).Elem()
	mnames := reflect.TypeOf(m).Elem()
	mvals := reflect.ValueOf(m).Elem()
	for i := 0; i < values.NumField(); i++ {
		k := names.Field(i)
		v := values.Field(i)
		tag := k.Tag
		//是否需要验证存在
		_, ignoreUpCheck := tag.Lookup("ignoreUpCheck")
		mFieldName := k.Name
		if ignoreUpCheck {
			continue
		}
		if v.Kind() != reflect.Ptr {
			panic(names.Name() + "." + k.Name + ":必须为指针类型")
		}
		if v.IsNil() { //不输入则不会修改
			continue
		}
		isPtr := false //model值是否为指针
		mn, ok := mnames.FieldByName(mFieldName)
		if !ok { //model中不存在这个字段，则尝试查找后缀id(外键)
			//尝试检查是否有后缀Id
			if !strings.HasSuffix(mFieldName, "Id") {
				panic("对于没找找到同名字段请在in以Id为后缀!-" + k.Name)
			}
			//去掉in后的Id后则为m的对应字段
			mFieldName = mFieldName[:len(mFieldName)-2]
			mn, ok = mnames.FieldByName(mFieldName)
			if !ok {
				panic("model中不存在的字段!-" + k.Name)
			}
			tagOrm := mn.Tag.Get("orm")
			//验证对应的sqlmodel是否有外键rel(fk)
			if strings.Index(tagOrm, "rel(fk)") >= 0 { //有外键
				//查找对应表的Id是否存在
				stru := reflect.New(mn.Type.Elem()) //新建表对应的结构体
				if err := o.QueryTable(mn.Type.Elem().Name()).Filter("Id", v.Elem().Interface()).One(stru.Interface()); err != nil {
					if err == orm.ErrNoRows {
						this.BackInfo.Message = "\"" + mn.Tag.Get("desc") + "\"不存在!"
						return
					}
					this.ErrLog(err)
					this.BackInfo.Message = "服务器出错啦!"
					return
				}
				//log.Println(mvals.FieldByName(mFieldName).Elem().FieldByName("Id").Interface(), "--",v.Elem().Interface())
				mv := mvals.FieldByName(mFieldName)
				if !mv.IsNil() && reflect.DeepEqual(mv.Elem().FieldByName("Id").Interface(), v.Elem().Interface()) { //相同的不需要修改
					continue
				}
				v = stru
				isPtr = true
			} else { //不存在外键
				panic(reflect.TypeOf(m).Name() + "-找不到model对应字段且没有外键:" + mFieldName)
			}
		}

		mval := mvals.FieldByName(mFieldName)
		if !mval.IsValid() {
			panic(reflect.TypeOf(m).Name() + "-不存在字段:" + mFieldName)
		}
		if !isPtr {
			v = v.Elem()
		}
		var str string
		var oldv string
		if _, istime := mval.Interface().(time.Time); istime { //model里有time类型的字段时,in为了兼容json使用了string类型，则需要转换
			t, err := time.ParseInLocation("2006-01-02", v.String(), time.Local)
			if v.String() != "" && err != nil { //v.String() != "",允许设置为空时间
				this.BackInfo.Message = k.Name + "不是正确的时间格式!" + err.Error()
				return
			}
			oldv = mval.Interface().(time.Time).Format("2006-01-02")
			str = v.String()
			v = reflect.ValueOf(t)
		} else {
			switch v.Kind() {
			case reflect.String:
				oldv = mval.String()
				str = v.String()
			case reflect.Int32, reflect.Int8:
				oldv = strconv.FormatInt(mval.Int(), 10)
				str = strconv.FormatInt(v.Int(), 10)
			default:
				//查询原id对应的结构
				stru := mval
				if !mval.IsNil() {
					if err := o.QueryTable(mn.Type.Elem().Name()).Filter("Id", mval.Elem().FieldByName("Id").Interface()).One(stru.Interface()); err != nil {
						if err != orm.ErrNoRows {
							this.ErrLog(err)
							this.BackInfo.Message = "服务器出错啦!"
							return
						}
					}
					oldv = stru.Elem().FieldByName("Name").String()
				}
				str = v.Elem().FieldByName("Name").String()
			}
		}
		//log.Println(mval.Interface(), "--", v.Interface())
		if reflect.DeepEqual(mval.Interface(), v.Interface()) { //相同的不需要修改
			continue
		}
		oldvalues = append(oldvalues, oldv)
		mval.Set(v)
		upvalues = append(upvalues, str)
		ups = append(ups, mFieldName)
	}
	if len(ups) > 0 {
		//记录修改历史
		part := sqlmodel.Part{Id: PartId}
		if err := o.Read(&part, "Id"); err != nil {
			if err == orm.ErrNoRows {
				this.BackInfo.Message = "不存在的零件!"
				return
			}
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦!"
			return
		}
		project := sqlmodel.Project{Id: ProjectId}
		if err := o.Read(&project, "Id"); err != nil {
			if err == orm.ErrNoRows {
				this.BackInfo.Message = "不存在的项目!"
				return
			}
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦!"
			return
		}
		upuser := this.GetUserInfo(token)
		uph := sqlmodel.UpdateHistory{Type: 1, ProjectId: ProjectId, ProjectName: project.Name, PartId: PartId, PartName: part.Name, Table: mnames.Name(), UpUser: upuser.Name, UpJobNumber: upuser.JobNumber, Ip: this.Ctx.Request.RemoteAddr, UpTime: time.Now()}
		for i, v := range upvalues {
			uph.OldValue = oldvalues[i]
			uph.Value = v
			uph.Field = ups[i]
			//println(uph.Type, uph.Field, uph.Value)
			if _, err := o.Insert(&uph); err != nil {
				this.ErrLog(err)
				this.BackInfo.Message = "服务器出错啦!"
				return
			}
			uph.Id = 0 //重置为0,否则下一个有id的话会出错
		}
		if _, err := o.Update(m, ups...); err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦!"
			return
		}
	}
	status = true
	return
}
