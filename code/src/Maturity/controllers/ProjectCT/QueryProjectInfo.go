package ProjectCT

import (
	"Maturity/models/projectMD"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
)

type inputQueryProjectInfo struct {
	Token *string `maxlen:"22"`
	Id    *uint32
}

// @Summary 项目信息
// @Description
// @Param _ body controllers.ProjectCT.inputQueryProjectInfo true
// @Success 200 {object} models.projectMD.ProjectElem
// @router /QueryProjectInfo/ [post]
func (this *ProjectController) QueryProjectInfo() {
	in := inputQueryProjectInfo{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !userMD.CheckUserToken(*in.Token) {
		this.BackInfo.Message = "令牌错误"
		return
	}

	o := orm.NewOrm()
	m := sqlmodel.Project{Id: *in.Id}
	if err := o.Read(&m); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的项目"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	o.LoadRelated(&m, "Admin")
	dinfo := projectMD.ProjectInfo{
		Id:             m.Id,
		Name:           m.Name,
		Code:           m.Code,
		AdminId:        m.Admin.Id,
		Admin:          m.Admin.Name,
		AdminJobNumber: m.Admin.JobNumber,
		RegTime:        m.RegTime.Format("2006-01-02 15:04:05"),
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
