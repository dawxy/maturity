package ProjectCT

import (
	"Maturity/models/projectMD"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
)

type inputQueryProjectList struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`

	Secrch *string `canempty:""`
}

// @Summary 项目列表
// @Description Start`min:"0"` <br>Length`min:"1" max:"500"`
// @Param _ body controllers.ProjectCT.inputQueryProjectList true
// @Success 200 {object} models.projectMD.ProjectList
// @router /QueryProjectList/ [post]
func (this *ProjectController) QueryProjectList() {
	in := inputQueryProjectList{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !userMD.CheckUserToken(*in.Token) {
		this.BackInfo.Message = "令牌错误"
		return
	}

	o := orm.NewOrm()
	qs := o.QueryTable("project")
	if in.Secrch != nil && *in.Secrch != "" {
		cond := orm.NewCondition()
		cond = cond.Or("name__icontains", *in.Secrch).Or("Code__icontains", *in.Secrch)
		qs = qs.SetCond(orm.NewCondition().AndCond(cond))
	}
	dinfo := projectMD.ProjectList{}
	var err error
	if dinfo.RecordsTotal, err = qs.Count(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	elems := []sqlmodel.Project{}
	if _, err = qs.Limit(*in.Length, *in.Start).OrderBy("-reg_time").RelatedSel("Admin").All(&elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	for _, e := range elems {
		elem := projectMD.ProjectElem{}
		elem.Name = e.Name
		elem.Code = e.Code
		elem.Id = e.Id
		elem.Admin = e.Admin.Name
		elem.RegTime = e.RegTime.Local().Format("2006-01-02 15:04:05")
		dinfo.Elems = append(dinfo.Elems, elem)
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
