package ProjectCT

import (
	"Maturity/controllers"
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
)

//项目相关
type ProjectController struct {
	controllers.BaseController
}

func (this *ProjectController) IsProjectAdmin(Token string, ProjectId uint32) (ok bool) {
	u := this.GetUserInfo(Token)
	if u == nil {
		var err error
		u, err = userMD.GetUser(Token)
		if err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
		if u == nil {
			this.BackInfo.Message = "令牌错误"
			return
		}
		this.SetUserInfo(Token, u)
	}
	o := orm.NewOrm()
	//系统管理员有权限
	if o.QueryTable(&sqlmodel.UserRole{}).Filter("User", u).Filter("Role", CheckPower.SystemAdmin).Exist() {
		return true
	}
	p := sqlmodel.Project{Id: ProjectId}
	if err := o.Read(&p, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的项目!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	if p.Admin.Id != u.Id {
		this.BackInfo.Message = "您没有权限"
		return
	}
	ok = true
	return
}

func (this *ProjectController) IsProjectTecEng(Token string, ProjectId uint32) (ok bool) {
	u := this.GetUserInfo(Token)
	if u == nil {
		var err error
		u, err = userMD.GetUser(Token)
		if err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
		if u == nil {
			this.BackInfo.Message = "令牌错误"
			return
		}
	}
	//系统管理员有权限
	o := orm.NewOrm()
	if o.QueryTable(&sqlmodel.UserRole{}).Filter("User", u).Filter("Role", CheckPower.SystemAdmin).Exist() {
		return true
	}
	return o.QueryTable(&sqlmodel.TecEng{}).Filter("Project", ProjectId).Filter("User", u).Exist()
}
