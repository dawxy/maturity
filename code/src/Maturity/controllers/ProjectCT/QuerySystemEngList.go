package ProjectCT

import (
	"Maturity/models/projectMD"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputQuerySystemEngList struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`

	Secrch       *string `canempty:""`
	ProjectId    *uint32
	ProfessionId *uint32 `canempty:""`
}

// @Summary 查询系统工程师列表
// @Description Start`min:"0"` <br>Length`min:"1" max:"500"`
// @Param _ body controllers.ProjectCT.inputQuerySystemEngList true
// @Success 200 {object} models.projectMD.QuerySystemEngList
// @router /QuerySystemEngList/ [post]
func (this *ProjectController) QuerySystemEngList() {
	in := inputQuerySystemEngList{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !this.IsProjectAdmin(*in.Token, *in.ProjectId) && !this.IsProjectTecEng(*in.Token, *in.ProjectId) {
		return
	}

	o := orm.NewOrm()
	qs := o.QueryTable(&sqlmodel.SystemEng{})
	if in.Secrch != nil && *in.Secrch != "" {
		cond := orm.NewCondition()
		cond = cond.Or("User__name__icontains", *in.Secrch).Or("User__JobNumber__icontains", *in.Secrch)
		qs = qs.SetCond(orm.NewCondition().AndCond(cond))
	}
	qs = qs.Filter("Project", *in.ProjectId)
	if in.ProfessionId != nil {
		qs = qs.Filter("Profession", *in.ProfessionId)
	}
	dinfo := projectMD.QuerySystemEngList{}
	var err error
	if dinfo.RecordsTotal, err = qs.Count(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	elems := []sqlmodel.SystemEng{}
	if _, err = qs.Limit(*in.Length, *in.Start).OrderBy("Id").RelatedSel().All(&elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	for _, e := range elems {
		elem := projectMD.QuerySystemEngListElem{}
		elem.Id = e.Id
		elem.Name = e.User.Name
		elem.JobNumber = e.User.JobNumber
		elem.Profession = e.Profession.Name
		dinfo.Elems = append(dinfo.Elems, elem)
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
