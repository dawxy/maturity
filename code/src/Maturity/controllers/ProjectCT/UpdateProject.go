package ProjectCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputUpdateProject struct {
	Token   *string `maxlen:"22"`
	Id      *uint32
	Code    *string `canempty:"" maxlen:"128"`
	Name    *string `canempty:"" maxlen:"64"`
	AdminId *uint32 `canempty:""`
}

// @Summary 修改项目信息
// @Description
// @Param _ body controllers.ProjectCT.inputUpdateProject true
// @Success 200 {object} string
// @router /UpdateProject/ [post]
func (this *ProjectController) UpdateProject() {
	in := inputUpdateProject{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !CheckPower.CheckPower(this, *in.Token, CheckPower.UpdateProject) {
		return
	}

	o := orm.NewOrm()
	//检查是否存在这个项目
	m := sqlmodel.Project{Id: *in.Id}
	if err := o.Read(&m, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的项目!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	var ups []string //需要修改的字段

	if in.Code != nil {
		m.Code = *in.Code
		ups = append(ups, "Code")
	}
	if in.Name != nil {
		m.Name = *in.Name
		ups = append(ups, "Name")
	}
	if in.AdminId != nil {
		if !o.QueryTable("user").Filter("Id", *in.AdminId).Exist() {
			this.BackInfo.Message = "不存在的用户!"
			return
		}
		m.Admin = &sqlmodel.User{Id: *in.AdminId}
		ups = append(ups, "Admin")
	}

	if len(ups) > 0 {
		if _, err := o.Update(&m, ups...); err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦!"
			return
		}
	}

	this.BackInfo.Status = true
	this.BackInfo.Message = "成功!"
}
