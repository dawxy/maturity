package ProjectCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputDelProject struct {
	Token *string `maxlen:"22"`
	Id    *uint32
}

// @Summary 删除项目
// @Description 删除项目
// @Param _ body controllers.ProjectCT.inputDelProject true
// @Success 200 {object} string
// @router /DelProject/ [post]
func (this *ProjectController) DelProject() {
	in := inputDelProject{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !CheckPower.CheckPower(this, *in.Token, CheckPower.DelProject) {
		return
	}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}
	defer o.Rollback()
	//检查该项目下是否还有零件
	if o.QueryTable("Part").Filter("Project", *in.Id).Exist() {
		this.BackInfo.Message = "该项目下还有未删除的零件!"
		return
	}
	//删除
	if _, err := o.Delete(&sqlmodel.Project{Id: *in.Id}, "Id"); err != nil {
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}
	if err := o.Commit(); err != nil {
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功!"
}
