package ProjectCT

import (
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
	"time"
)

type inputAddTecEng struct {
	Token     *string `maxlen:"22"`
	UserId    *uint32
	ProjectId *uint32
}

// @Summary 添加技术集成工程师
// @Description
// @Param _ body controllers.ProjectCT.inputAddTecEng true
// @Success 200 {object} string
// @router /AddTecEng/ [post]
func (this *ProjectController) AddTecEng() {
	in := inputAddTecEng{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !this.IsProjectAdmin(*in.Token, *in.ProjectId) {
		return
	}

	p := sqlmodel.TecEng{}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	//检查是否存在这个用户
	u := &sqlmodel.User{Id: *in.UserId}
	if err := o.Read(u, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的用户!"
		}
		return
	}
	p.User = u
	project := &sqlmodel.Project{Id: *in.ProjectId}
	if err := o.Read(project, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的项目!"
		}
		return
	}
	p.Project = project
	if o.QueryTable(&p).Filter("User", *in.UserId).Filter("Project", *in.ProjectId).Exist() {
		this.BackInfo.Message = "该项目下已经存在该用户为技术集成工程师!"
		return
	}

	if _, err := o.Insert(&p); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	//记录日志
	upu := this.GetUserInfo(*in.Token)
	sysl := sqlmodel.SysLogs{Type: "用户添加权限", ProjectName: project.Name, ProjectId: project.Id, Value: "整车技术集成工程师", TargetUserJobNum: u.JobNumber, TargetUserJobName: u.Name, UserName: upu.Name, UpJobNumber: upu.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
	if _, err := o.Insert(&sysl); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
