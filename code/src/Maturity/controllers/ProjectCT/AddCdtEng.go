package ProjectCT

import (
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
	"time"
)

type inputAddCdtEng struct {
	Token     *string `maxlen:"22"`
	UserId    *uint32
	ProjectId *uint32
	EngName   *string
}

var (
	engsName = make(map[string]string)
)

type tmpEng struct {
	Id   string
	Name string
}

func init() {
	engs := []tmpEng{{Id: "DesignerEng", Name: "设计工程师"}, {Id: "BuyerEng", Name: "采购工程师"}, {Id: "SQEEng", Name: "SQE工程师"}, {Id: "CrefftauEng", Name: "工艺工程师"}, {Id: "TestEng", Name: "试验工程师"}, {Id: "SizeEng", Name: "尺寸工程师"}, {Id: "NewqualityEng", Name: "新品质量工程师"}}
	for _, v := range engs {
		engsName[v.Id] = v.Name
	}
}

// @Summary 添加协同开发小组管理员
// @Description
// @Param _ body controllers.ProjectCT.inputAddCdtEng true
// @Success 200 {object} string
// @router /AddCdtEng/ [post]
func (this *ProjectController) AddCdtEng() {
	in := inputAddCdtEng{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !this.IsProjectAdmin(*in.Token, *in.ProjectId) && !this.IsProjectTecEng(*in.Token, *in.ProjectId) {
		return
	}
	//检查是否存在这个工程师
	ename, ok := engsName[*in.EngName]
	if !ok {
		this.BackInfo.Message = "工程师种类非法!"
		return
	}

	p := sqlmodel.CdtEng{}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	//检查项目
	project := &sqlmodel.Project{Id: *in.ProjectId}
	if err := o.Read(project, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的项目!"
		}
		return
	}
	p.Project = project
	//检查是否存在这个用户
	u := &sqlmodel.User{Id: *in.UserId}
	if err := o.Read(u, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的用户!"
		}
		return
	}
	p.User = u

	p.EngName = *in.EngName
	if o.QueryTable(&p).Filter("User", *in.UserId).Filter("EngName", *in.EngName).Filter("Project", *in.ProjectId).Exist() {
		this.BackInfo.Message = "该种类工程师已经存在该管理员!"
		return
	}

	if _, err := o.Insert(&p); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	//记录日志
	upu := this.GetUserInfo(*in.Token)
	sysl := sqlmodel.SysLogs{Type: "用户添加权限", ProjectName: project.Name, ProjectId: project.Id, Value: ename + ",协同开发小组管理员", TargetUserJobNum: u.JobNumber, TargetUserJobName: u.Name, UserName: upu.Name, UpJobNumber: upu.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
	if _, err := o.Insert(&sysl); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
