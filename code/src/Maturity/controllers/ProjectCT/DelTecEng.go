package ProjectCT

import (
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
	"time"
)

type inputDelTecEng struct {
	Token *string `maxlen:"22"`
	Id    *uint32
}

// @Summary 删除技术集成工程师
// @Description
// @Param _ body controllers.ProjectCT.inputDelTecEng true
// @Success 200 {object} string
// @router /DelTecEng/ [post]
func (this *ProjectController) DelTecEng() {
	in := inputDelTecEng{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	tecEng := sqlmodel.TecEng{Id: *in.Id}
	if err := o.Read(&tecEng, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的技术集成工程师!"
		}
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}
	if _, err := o.LoadRelated(&tecEng, "User"); err != nil {
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}
	if _, err := o.LoadRelated(&tecEng, "Project"); err != nil {
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}
	if !this.IsProjectAdmin(*in.Token, tecEng.Project.Id) {
		return
	}
	//删除
	if _, err := o.Delete(&sqlmodel.TecEng{Id: *in.Id}, "Id"); err != nil {
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}

	//记录日志
	upu := this.GetUserInfo(*in.Token)
	sysl := sqlmodel.SysLogs{Type: "用户删除权限", ProjectName: tecEng.Project.Name, ProjectId: tecEng.Project.Id, Value: "整车技术集成工程师", TargetUserJobNum: tecEng.User.JobNumber, TargetUserJobName: tecEng.User.Name, UserName: upu.Name, UpJobNumber: upu.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
	if _, err := o.Insert(&sysl); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功!"
}
