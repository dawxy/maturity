package ProjectCT

import (
	"Maturity/models/projectMD"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputQueryCdtEngList struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`

	Secrch    *string `canempty:""`
	ProjectId *uint32
	EngName   *string `canempty:""`
}

// @Summary 查询协同开发小组管理员列表
// @Description Start`min:"0"` <br>Length`min:"1" max:"500"`
// @Param _ body controllers.ProjectCT.inputQueryCdtEngList true
// @Success 200 {object} models.projectMD.QueryCdtEngList
// @router /QueryCdtEngList/ [post]
func (this *ProjectController) QueryCdtEngList() {
	in := inputQueryCdtEngList{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !this.IsProjectAdmin(*in.Token, *in.ProjectId) && !this.IsProjectTecEng(*in.Token, *in.ProjectId) {
		return
	}

	o := orm.NewOrm()
	qs := o.QueryTable(&sqlmodel.CdtEng{})
	if in.Secrch != nil && *in.Secrch != "" {
		cond := orm.NewCondition()
		cond = cond.Or("User__name__icontains", *in.Secrch).Or("User__JobNumber__icontains", *in.Secrch)
		qs = qs.SetCond(orm.NewCondition().AndCond(cond))
	}
	qs = qs.Filter("Project", *in.ProjectId)
	if in.EngName != nil {
		qs = qs.Filter("EngName", *in.EngName)
	}
	dinfo := projectMD.QueryCdtEngList{}
	var err error
	if dinfo.RecordsTotal, err = qs.Count(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	elems := []sqlmodel.CdtEng{}
	if _, err = qs.Limit(*in.Length, *in.Start).OrderBy("Id").RelatedSel().All(&elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	for _, e := range elems {
		elem := projectMD.QueryCdtEngListElem{}
		elem.Id = e.Id
		elem.Name = e.User.Name
		elem.JobNumber = e.User.JobNumber
		elem.EngName = engsName[e.EngName]
		dinfo.Elems = append(dinfo.Elems, elem)
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
