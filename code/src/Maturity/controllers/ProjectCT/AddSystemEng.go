package ProjectCT

import (
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
	"time"
)

type inputAddSystemEng struct {
	Token        *string `maxlen:"22"`
	UserId       *uint32
	ProjectId    *uint32
	ProfessionId *uint32
}

// @Summary 添加系统集成经理
// @Description
// @Param _ body controllers.ProjectCT.inputAddSystemEng true
// @Success 200 {object} string
// @router /AddSystemEng/ [post]
func (this *ProjectController) AddSystemEng() {
	in := inputAddSystemEng{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !this.IsProjectAdmin(*in.Token, *in.ProjectId) && !this.IsProjectTecEng(*in.Token, *in.ProjectId) {
		return
	}

	p := sqlmodel.SystemEng{}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	//检查项目
	project := &sqlmodel.Project{Id: *in.ProjectId}
	if err := o.Read(project, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的项目!"
		}
		return
	}
	p.Project = project
	//检查是否存在这个用户
	u := &sqlmodel.User{Id: *in.UserId}
	if err := o.Read(u, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的用户!"
		}
		return
	}
	p.User = u
	profession := &sqlmodel.Profession{Id: *in.ProfessionId}
	if err := o.Read(profession); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的专业!"
		}
		return
	}
	p.Profession = profession
	if o.QueryTable(&p).Filter("User", *in.UserId).Filter("Profession", *in.ProfessionId).Filter("Project", *in.ProjectId).Exist() {
		this.BackInfo.Message = "该专业下已经存在该用户!"
		return
	}

	if _, err := o.Insert(&p); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	//记录日志
	upu := this.GetUserInfo(*in.Token)
	sysl := sqlmodel.SysLogs{Type: "用户添加权限", ProjectName: project.Name, ProjectId: project.Id, Value: profession.Name + ",系统集成经理", TargetUserJobNum: u.JobNumber, TargetUserJobName: u.Name, UserName: upu.Name, UpJobNumber: upu.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
	if _, err := o.Insert(&sysl); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
