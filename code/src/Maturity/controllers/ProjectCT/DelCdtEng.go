package ProjectCT

import (
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
	"time"
)

type inputDelCdtEng struct {
	Token *string `maxlen:"22"`
	Id    *uint32
}

// @Summary 删除系统集成工程师
// @Description
// @Param _ body controllers.ProjectCT.inputDelCdtEng true
// @Success 200 {object} string
// @router /DelCdtEng/ [post]
func (this *ProjectController) DelCdtEng() {
	in := inputDelCdtEng{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	m := sqlmodel.CdtEng{Id: *in.Id}
	if err := o.Read(&m); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的系统集成工程师!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if _, err := o.LoadRelated(&m, "User"); err != nil {
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}
	if _, err := o.LoadRelated(&m, "Project"); err != nil {
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}
	if !this.IsProjectAdmin(*in.Token, m.Project.Id) && !this.IsProjectTecEng(*in.Token, m.Project.Id) {
		return
	}
	//删除
	if _, err := o.Delete(&m, "Id"); err != nil {
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}
	//记录日志
	upu := this.GetUserInfo(*in.Token)
	sysl := sqlmodel.SysLogs{Type: "用户删除权限", ProjectName: m.Project.Name, ProjectId: m.Project.Id, Value: engsName[m.EngName] + ",系统集成经理", TargetUserJobNum: m.User.JobNumber, TargetUserJobName: m.User.Name, UserName: upu.Name, UpJobNumber: upu.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
	if _, err := o.Insert(&sysl); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功!"
}
