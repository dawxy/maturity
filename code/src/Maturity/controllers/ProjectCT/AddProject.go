package ProjectCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputAddProject struct {
	Token   *string `maxlen:"22"`
	Name    *string `maxlen:"64"`
	AdminId *uint32
	Code    *string `maxlen:"128"`
}

// @Summary 添加项目
// @Description 添加项目
// @Param _ body controllers.ProjectCT.inputAddProject true
// @Success 200 {object} string
// @router /AddProject/ [post]
func (this *ProjectController) AddProject() {
	in := inputAddProject{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !CheckPower.CheckPower(this, *in.Token, CheckPower.AddProject) {
		return
	}

	u := this.GetUserInfo(*in.Token)
	p := sqlmodel.Project{Code: *in.Code, Name: *in.Name, Creator: u}
	o := orm.NewOrm()
	//检查是否存在这个用户
	if !o.QueryTable("user").Filter("Id", *in.AdminId).Exist() {
		this.BackInfo.Message = "不存在的用户!"
		return
	}
	p.Admin = &sqlmodel.User{Id: *in.AdminId}
	p.Creator = this.GetUserInfo(*in.Token)
	if _, err := o.Insert(&p); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
