package controllers

import (
	"math/rand"
	"strings"
	"time"

	"github.com/pborman/uuid"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}
func get16int(c byte) uint64 {
	if c >= 'a' && c <= 'z' {
		return uint64(c - 'a' + 10)
	}
	return uint64(c - '0')
}

var mp = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" //进制字典
func to62(str string) string {
	//转10进制
	var num uint64
	for i := 0; i < len(str); i++ {
		num = num*16 + get16int(str[i])
	}
	//转62进制
	ans := make([]byte, 11, 11)
	cnt := 0
	for num != 0 {
		ans[cnt] = mp[num%62]
		num /= 62
		cnt++
	}
	//不足11位则补上随机数
	for ; cnt < 11; cnt++ {
		ans[cnt] = mp[rand.Intn(62)]
	}
	return string(ans)
}

//UUID22 生成22位UUID
func UUID22() string {
	str := strings.Replace(uuid.New(), "-", "", -1)
	return to62(str[:16]) + to62(str[16:])
}
