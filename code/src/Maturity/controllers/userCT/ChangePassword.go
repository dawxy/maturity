package userCT

import (
	"Maturity/controllers"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
	"time"
)

type inputChangePassword struct {
	JobNumber   *string `maxlen:"8"`
	OldPassword *string `maxlen:"32"`
	NewPassword *string `maxlen:"32"`
}

// @Summary 用户自己修改密码
// @Description 通过旧密码修改自己的密码
// @Param _ body controllers.userCT.inputChangePassword true
// @Success 200 {object} models.userMD.LoginModel
// @router /ChangePassword/ [post]
func (this *UserController) ChangePassword() {
	in := inputChangePassword{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if err := userMD.CheckPassword(*in.NewPassword); err != "" {
		this.BackInfo.Message = err
		return
	}
	if *in.NewPassword == *in.OldPassword {
		this.BackInfo.Message = "新密码不能和旧密码相同!"
		return
	}

	u := sqlmodel.User{JobNumber: *in.JobNumber}
	o := orm.NewOrm()
	if err := o.Read(&u, "JobNumber"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的用户!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if u.Password != userMD.GetUserPassword(u.Token, *in.OldPassword) {
		this.BackInfo.Message = "旧密码错误!"
		return
	}
	u.Password = *in.NewPassword
	u.Token = controllers.UUID22()
	u.Password = userMD.GetUserPassword(u.Token, u.Password)
	u.IsInitPassword = false
	if _, err := o.Update(&u, "Password", "Token", "IsInitPassword"); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	//记录日志
	sysl := sqlmodel.SysLogs{Type: "密码修改", UserName: u.Name, TargetUserJobNum: u.JobNumber, TargetUserJobName: u.Name, UpJobNumber: u.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
	if _, err := o.Insert(&sysl); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
	this.BackInfo.Data = userMD.LoginModel{Token: u.Token}
}
