package userCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/RoleMD"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inpuQueryUserRoles struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`
	UserId *uint32

	Secrch *string `canempty:""`
}

// @Summary 用户拥有的角色列表
// @Description 分页查询
// @Param _ body controllers.userCT.inpuQueryUserRoles true
// @Success 200 {object} models.RoleMD.QueryRoleList
// @router /QueryUserRoles/ [post]
func (this *UserController) QueryUserRoles() {
	in := inpuQueryUserRoles{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.RoleOrPowerList) {
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable("user_role").Filter("User", in.UserId)
	if in.Secrch != nil && *in.Secrch != "" {
		qs = qs.Filter("Power__name__icontains", *in.Secrch)
	}

	dinfo := RoleMD.QueryRoleList{}
	var err error
	if dinfo.RecordsTotal, err = qs.Count(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	elems := []sqlmodel.UserRole{}
	if _, err = qs.Limit(*in.Length, *in.Start).RelatedSel("Role").All(&elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	for _, e := range elems {
		elem := RoleMD.RoleElem{}
		elem.Id = e.Role.Id
		elem.Name = e.Role.Name
		dinfo.Elems = append(dinfo.Elems, elem)
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
