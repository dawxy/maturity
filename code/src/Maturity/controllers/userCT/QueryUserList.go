package userCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
)

type inputQueryUserList struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`

	ShowStop                        *bool   `canempty:""` //是否显示停用账号(不填不显示)
	DepartmentId                    *uint32 `canempty:""` //按照部门筛选
	RoleId                          *uint32 `canempty:""` //按照角色筛选
	Secrch                          *string `canempty:""`
	SecrchIncludeDepartmentAndPhone *bool   `canempty:""` //搜索条件是否包含部门和手机号
}

// @Summary 用户列表
// @Description
// @Param _ body controllers.userCT.inputQueryUserList true
// @Success 200 {object} models.userMD.UserList
// @router /QueryUserList/ [post]
func (this *UserController) QueryUserList() {
	in := inputQueryUserList{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	SecrchIncludeDepartmentAndPhone := false
	if in.SecrchIncludeDepartmentAndPhone != nil && *in.SecrchIncludeDepartmentAndPhone {
		SecrchIncludeDepartmentAndPhone = true
	}
	if in.ShowStop != nil && *in.ShowStop == true {
		if !CheckPower.CheckPower(this, *in.Token, CheckPower.QueryUserList) {
			return
		}
	} else {
		if !userMD.CheckUserToken(*in.Token) {
			this.BackInfo.Message = "令牌错误~"
			return
		}
	}

	o := orm.NewOrm()
	qs := o.QueryTable(&sqlmodel.User{})
	if in.Secrch != nil && *in.Secrch != "" {
		cond := orm.NewCondition()
		cond = cond.Or("name__icontains", *in.Secrch).Or("JobNumber__icontains", *in.Secrch)
		if SecrchIncludeDepartmentAndPhone {
			cond = cond.Or("Department__Name__icontains", *in.Secrch).Or("Phone__icontains", *in.Secrch)
		}
		qs = qs.SetCond(orm.NewCondition().AndCond(cond))
		//qs = qs.Filter("name__icontains", *in.Secrch)
	}
	if in.DepartmentId != nil {
		qs = qs.Filter("Department", &sqlmodel.Department{Id: *in.DepartmentId})
	}
	if in.RoleId != nil {
		qs = qs.Filter("UserRoles__Role__id", *in.RoleId)
	}
	if in.ShowStop == nil || !*in.ShowStop {
		qs = qs.Filter("IsStop", false)
	}

	dinfo := userMD.UserList{}
	var err error
	if dinfo.RecordsTotal, err = qs.Count(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	elems := []sqlmodel.User{}
	if _, err = qs.Limit(*in.Length, *in.Start).OrderBy("-Id").RelatedSel("Department").All(&elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	for _, e := range elems {
		elem := userMD.UserElem{}
		elem.Name = e.Name
		elem.JobNumber = e.JobNumber
		elem.Id = e.Id
		elem.Department = e.Department.Name
		elem.Phone = e.Phone
		elem.IsStop = e.IsStop
		dinfo.Elems = append(dinfo.Elems, elem)
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
