package userCT

import (
	"Maturity/controllers"
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
	"math/rand"
	"strconv"
	"time"
)

type inputResetPassword struct {
	Token  *string `maxlen:"22"`
	UserId *uint32
}

// @Summary 重置用户密码
// @Description 会随机生成6位新密码
// @Param _ body controllers.userCT.inputResetPassword true
// @Success 200 {object} models.userMD.ResetPassword
// @router /ResetPassword/ [post]
func (this *UserController) ResetPassword() {
	in := inputQueryUserInfo{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !CheckPower.CheckPower(this, *in.Token, CheckPower.ResetPassword) {
		return
	}

	u := sqlmodel.User{Id: *in.UserId}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	if err := o.Read(&u, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的用户"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}

	u.Password = ""
	for i := 0; i < 6; i++ {
		u.Password += strconv.FormatInt(rand.Int63n(10), 10)
	}
	newPassword := u.Password
	u.Token = controllers.UUID22()
	u.Password = userMD.GetUserPassword(u.Token, u.Password)
	u.IsInitPassword = true
	if _, err := o.Update(&u, "Password", "Token", "IsInitPassword"); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	//记录日志
	upu := this.GetUserInfo(*in.Token)
	sysl := sqlmodel.SysLogs{Type: "重置密码", Value: newPassword, TargetUserJobNum: u.JobNumber, TargetUserJobName: u.Name, UserName: upu.Name, UpJobNumber: upu.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
	if _, err := o.Insert(&sysl); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = userMD.ResetPassword{NewPassword: newPassword}
}
