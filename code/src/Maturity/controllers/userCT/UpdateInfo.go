package userCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
	"strings"
	"time"
)

type inputUpdateInfo struct {
	Token        *string `maxlen:"22"`
	UpUserId     *uint32 `canempty:""`
	JobNumber    *string `canempty:"" maxlen:"8"`
	Email        *string `canempty:"" maxlen:"128"`
	Name         *string `canempty:"" maxlen:"64"`
	Phone        *string `canempty:"" maxlen:"32"`
	DepartmentID *uint32 `canempty:""`
	IsStop       *bool   `canempty:""`
}

// @Summary 修改任何人的用户信息
// @Description 除了Token外其他的填写就会修改,不填不修改;admin账号不能修改工号和停用;UpUserId不填则修改自己的信息
// @Param _ body controllers.userCT.inputUpdateInfo true
// @Success 200 {object} string
// @router /UpdateInfo/ [post]
func (this *UserController) UpdateInfo() {
	in := inputUpdateInfo{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if in.JobNumber != nil {
		*in.JobNumber = strings.ToLower(*in.JobNumber)
	}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	var ups []string //需要修改的字段
	//修改别人,需要权限,且可修改信息较多
	u := &sqlmodel.User{}
	if in.UpUserId != nil {
		if !CheckPower.CheckPower(this, *in.Token, CheckPower.UpdateInfo) {
			return
		}
		u.Id = *in.UpUserId
		if err := o.Read(u, "Id"); err != nil {
			if err == orm.ErrNoRows {
				this.BackInfo.Message = "不存在的用户"
				return
			}
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦!"
			return
		}
		//只有有修改权限才可修改
		if in.JobNumber != nil && u.JobNumber != *in.JobNumber {
			if *in.JobNumber == "" {
				this.BackInfo.Message = "工号不能修改为空"
				return
			}
			if u.JobNumber == CheckPower.AdminUser.JobNumber {
				this.BackInfo.Message = "不能修改" + CheckPower.AdminUser.JobNumber + "的工号!"
				return
			}
			if o.QueryTable(u).Filter("JobNumber", *in.JobNumber).Exist() {
				this.BackInfo.Message = "该工号已经存在!"
				return
			}
			//记录日志
			upu := this.GetUserInfo(*in.Token)
			sysl := sqlmodel.SysLogs{Type: "修改工号", Value: *in.JobNumber, TargetUserJobNum: u.JobNumber, TargetUserJobName: u.Name, UserName: upu.Name, UpJobNumber: upu.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
			if _, err := o.Insert(&sysl); err != nil {
				this.ErrLog(err)
				this.BackInfo.Message = "服务器出错啦!"
				return
			}

			u.JobNumber = *in.JobNumber
			ups = append(ups, "JobNumber")
		}
		if in.Email != nil {
			if *in.Email == "" {
				this.BackInfo.Message = "邮箱不能修改为空"
			}
			if o.QueryTable(u).Filter("Email", *in.Email).Exist() {
				this.BackInfo.Message = "该邮箱已经绑定其他用户,请先取消绑定!"
				return
			}
			u.Email = *in.Email
			ups = append(ups, "Email")
		}
		if in.Name != nil && u.Name != *in.Name {
			if *in.Name == "" {
				this.BackInfo.Message = "名字不能修改为空"
				return
			}
			u.Name = *in.Name
			ups = append(ups, "Name")
		}
		if in.DepartmentID != nil && u.Department.Id != *in.DepartmentID {
			if !o.QueryTable("Department").Filter("Id", *in.DepartmentID).Exist() {
				this.BackInfo.Message = "不存在的部门!"
				return
			}
			u.Department.Id = *in.DepartmentID
			ups = append(ups, "Department")
		}
		if in.IsStop != nil && u.IsStop != *in.IsStop {
			if u.JobNumber == CheckPower.AdminUser.JobNumber {
				this.BackInfo.Message = "不能停用" + CheckPower.AdminUser.JobNumber
				return
			}
			//记录日志
			upu := this.GetUserInfo(*in.Token)
			var newv string
			if *in.IsStop {
				newv = "停用"
			} else {
				newv = "启用"
			}
			sysl := sqlmodel.SysLogs{Type: "账号启停", Value: newv, TargetUserJobNum: u.JobNumber, TargetUserJobName: u.Name, UserName: upu.Name, UpJobNumber: upu.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
			if _, err := o.Insert(&sysl); err != nil {
				this.ErrLog(err)
				this.BackInfo.Message = "服务器出错啦!"
				return
			}
			u.IsStop = *in.IsStop
			ups = append(ups, "IsStop")
		}
	} else {
		var err error
		u, err = userMD.GetUser(*in.Token)
		if u == nil {
			this.BackInfo.Message = "令牌错误!"
			return
		}
		if err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦!"
			return
		}
	}

	if in.Phone != nil && u.Phone != *in.Phone {
		if err := userMD.CheckPhoneNum(*in.Phone); err != "" {
			this.BackInfo.Message = err
			return
		}
		u.Phone = *in.Phone
		ups = append(ups, "Phone")
	}

	if len(ups) > 0 {
		if _, err := o.Update(u, ups...); err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦!"
			return
		}
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
