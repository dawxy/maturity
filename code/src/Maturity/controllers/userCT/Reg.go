package userCT

import (
	"Maturity/controllers"
	"Maturity/models/CheckPower"
	"Maturity/models/DepartmentMD"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"context"
	"crypto/tls"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	"math/rand"
	"net"
	"net/smtp"
	"strconv"
	"strings"
	"time"
)

type inputReg struct {
	Token        *string `maxlen:"22"`
	JobNumber    *string `maxlen:"8"`
	Email        *string `canempty:"" maxlen:"128"`
	Name         *string `maxlen:"64"`
	Phone        *string `canempty:"" maxlen:"32"`
	RoleId       *uint32
	DepartmentID *uint32
}

var (
	sendEmailChanel = make(chan struct{}, 50) //限制邮件并发数
)

// @Summary 用户注册
// @Description Name和Phone可以为空
// @Param _ body controllers.userCT.inputReg true
// @Success 200 {object} models.userMD.LoginModel
// @router /Reg/ [post]
func (this *UserController) Reg() {
	in := inputReg{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !CheckPower.CheckPower(this, *in.Token, CheckPower.RegUser) {
		return
	}

	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	*in.JobNumber = strings.ToLower(*in.JobNumber)
	if o.QueryTable(&sqlmodel.User{}).Filter("JobNumber", in.JobNumber).Exist() {
		this.BackInfo.Message = "该工号已经注册!"
		return
	}

	m := sqlmodel.User{}
	token := controllers.UUID22()
	m.Token = token
	m.JobNumber = *in.JobNumber
	m.RegIP = this.Ctx.Request.RemoteAddr
	Password := ""
	for i := 0; i < 6; i++ {
		Password += strconv.FormatInt(rand.Int63n(10), 10)
	}
	m.Password = userMD.GetUserPassword(m.Token, Password)

	ok, err := DepartmentMD.HasDepartment(*in.DepartmentID)
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if !ok {
		this.BackInfo.Message = "不存在的部门!"
		return
	}
	m.Department = &sqlmodel.Department{Id: *in.DepartmentID}

	if in.Name != nil {
		m.Name = *in.Name
	}
	if in.Email != nil && *in.Email != "@hozonauto.com" {
		if o.QueryTable(&sqlmodel.User{}).Filter("Email", *in.Email).Exist() {
			this.BackInfo.Message = "该邮箱已经注册!"
			return
		}
		m.Email = *in.Email
	}
	if in.Phone != nil {
		if err := userMD.CheckPhoneNum(*in.Phone); err != "" {
			this.BackInfo.Message = err
			return
		}
		m.Phone = *in.Phone
	}
	m.IsInitPassword = true
	if _, err := o.Insert(&m); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	//插入角色
	role := sqlmodel.Role{Id: *in.RoleId}
	if err = o.Read(&role, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的角色!"
		}
		return
	}
	if _, err := o.Insert(&sqlmodel.UserRole{Role: &role, User: &m}); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	//记录日志
	upu := this.GetUserInfo(*in.Token)
	//记录日志
	sysl := sqlmodel.SysLogs{Type: "新建用户", TargetUserJobNum: m.JobNumber, TargetUserJobName: m.Name, UserName: upu.Name, UpJobNumber: upu.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
	if _, err := o.Insert(&sysl); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	sysl1 := sqlmodel.SysLogs{Type: "用户添加权限", Value: role.Name, TargetUserJobNum: m.JobNumber, TargetUserJobName: m.Name, UserName: upu.Name, UpJobNumber: upu.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
	if _, err := o.Insert(&sysl1); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	//发送邮件
	if in.Email != nil && *in.Email != "@hozonauto.com" {
		user := "hozonauto@hozonauto.com"
		password := "1"
		host := "smtp.icoremail.net:465"

		subject := *in.Name + ",已为您开通《零部件管理系统》账号"
		msg := *in.Name + ",</br>您的账号为:" + *in.JobNumber + "</br>初始密码为:" + Password + "</br>登录地址:<a href='http://" + this.Ctx.Request.Host + "'>http://" + this.Ctx.Request.Host + "</a>,登录后请修改密码!"
		body := `
		<html>
		<body>
		<h3>
		` + msg +
			`</h3>
			</body>
			</html>
			`
		go func() { //后台发送邮件
			ctx, _ := context.WithTimeout(context.Background(), time.Second*30)
			select {
			case sendEmailChanel <- struct{}{}:
				defer func() {
					<-sendEmailChanel
				}()
			case <-ctx.Done():
				logs.Warning("To", *in.Email, "邮件队列等待超时!\n", err)
				return
			}

			if err := SendToMail(user, password, host, *in.Email, subject, body, "html"); err != nil {
				logs.Warning("To", *in.Email, "邮件发送失败!\n", err)
				return
			}
		}()
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
	this.BackInfo.Data = userMD.RegModel{JobNumber: *in.JobNumber, Password: Password}
}
func SendToMail(user, password, servername, to, subject, body, mailtype string) error {
	host, _, _ := net.SplitHostPort(servername)
	auth := smtp.PlainAuth("", user, password, host)
	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}
	var content_type string
	if mailtype == "html" {
		content_type = "Content-Type: text/" + mailtype + "; charset=UTF-8"
	} else {
		content_type = "Content-Type: text/plain; charset=UTF-8"
	}

	msg := []byte("To: " + to + "\r\nFrom: " + user + ">\r\nSubject: " + subject + "\r\n" + content_type + "\r\n\r\n" + body)
	conn, err := tls.Dial("tcp", servername, tlsconfig)
	if err != nil {
		return err
	}
	c, err := smtp.NewClient(conn, host)
	if err != nil {
		return err
	}
	// Auth
	if err = c.Auth(auth); err != nil {
		return err
	}
	// To && From
	if err = c.Mail(user); err != nil {
		return err
	}
	if err = c.Rcpt(to); err != nil {
		return err
	}
	// Data
	w, err := c.Data()
	if err != nil {
		return err
	}
	_, err = w.Write([]byte(msg))
	if err != nil {
		return err
	}
	err = w.Close()
	if err != nil {
		return err
	}
	c.Quit()
	return err
}
