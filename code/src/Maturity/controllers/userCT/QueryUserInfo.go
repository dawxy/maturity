package userCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
)

type inputQueryUserInfo struct {
	Token  *string `maxlen:"22"`
	UserId *uint32 `canempty:""`
}

// @Summary 查询用户信息
// @Description UserId查询的用户ID，查询自己不需要权限,否则需要查询权限
// @Param _ body controllers.userCT.inputQueryUserInfo true
// @Success 200 {object} models.userMD.QueryUserInfo
// @router /QueryUserInfo/ [post]
func (this *UserController) QueryUserInfo() {
	in := inputQueryUserInfo{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	u, err := userMD.GetUser(*in.Token)
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦"
		return
	}
	if u == nil {
		this.BackInfo.Message = "令牌错误"
		return
	}
	if in.UserId != nil { //查询别人需要验证权限
		if !CheckPower.CheckPower(this, *in.Token, CheckPower.QueryUserInfo) {
			return
		}
		u.Id = *in.UserId
		if err = orm.NewOrm().Read(u, "Id"); err != nil {
			if err == orm.ErrNoRows {
				this.BackInfo.Message = "不存在的用户"
				return
			}
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦"
			return
		}
	}

	o := orm.NewOrm()
	o.LoadRelated(u, "Department")
	dinfo := userMD.QueryUserInfo{
		Id:           u.Id,
		Token:        u.Token,
		JobNumber:    u.JobNumber,
		Email:        u.Email,
		Name:         u.Name,
		Phone:        u.Phone,
		Department:   u.Department.Name,
		DepartmentID: u.Department.Id,
		RegTime:      u.RegTime.Format("2006-01-02 15:04:05"),
		RegIP:        u.RegIP,
		IsStop:       u.IsStop,
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
