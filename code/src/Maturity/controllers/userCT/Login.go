package userCT

import (
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
	"strings"
	"time"
)

type inputLogin struct {
	JobNumber *string `maxlen:"8"`
	Password  *string `maxlen:"32"`
}

// @Summary 用户登录
// @Description 登录
// @Param _ body controllers.userCT.inputLogin true
// @Success 200 {object} models.userMD.LoginModel
// @router /Login/ [post]
func (this *UserController) Login() {
	in := inputLogin{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	*in.JobNumber = strings.ToLower(*in.JobNumber)
	u := sqlmodel.User{JobNumber: *in.JobNumber, IsStop: false}
	o := orm.NewOrm()
	if err := o.Read(&u, "JobNumber", "IsStop"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的用户或者账号被停用!"
			return
		}
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}
	if u.Password != userMD.GetUserPassword(u.Token, *in.Password) {
		this.BackInfo.Message = "用户名或者密码错误!"
		return
	}
	ret := userMD.LoginModel{}
	if u.IsInitPassword { //密码为初始密码,需要修改后才可正常登录
		u.IsInitPassword = true
	} else {
		ret.Token = u.Token
	}
	//记录登录日志
	sysl := sqlmodel.SysLogs{Type: "用户登录", UserName: u.Name, UpJobNumber: u.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
	if _, err := o.Insert(&sysl); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = ret
}
