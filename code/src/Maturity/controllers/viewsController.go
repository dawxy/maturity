package controllers

import (
	"path"
	"strings"
)

//ViewsController 需要渲染的页面显示
type ViewsController struct {
	BaseController
}

func (this *ViewsController) Get() {
	//清理URL(比如包含多个连续的'/'则清理掉)
	this.Ctx.Request.URL.Path = path.Clean(this.Ctx.Request.URL.Path)
	this.Ctx.Request.RequestURI = path.Clean(this.Ctx.Request.RequestURI)
	this.TplName = strings.TrimPrefix(this.Ctx.Input.URL(), "/views")
	if this.TplName[0] == '/' {
		this.TplName = this.TplName[1:]
	}
}
