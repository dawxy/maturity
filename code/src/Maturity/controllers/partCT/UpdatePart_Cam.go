package partCT

import (
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
	"reflect"
)

type inputUpdatePart_CamHeader struct {
	Token  *string `maxlen:"22" ignoreUpCheck:""`
	PartId *uint32 `ignoreUpCheck:""`
	Cam    *string
}
type inputUpdatePart_Cam0_1 struct {
	Token  *string `maxlen:"22" ignoreUpCheck:""`
	PartId *uint32 `ignoreUpCheck:""`

	NeedSOR                             *int8   `canempty:"" UpEng:"DesignerEng"`
	LongCycleType                       *string `canempty:"" UpEng:"DesignerEng"`
	SORPlanCompletion                   *string `canempty:"" UpEng:"DesignerEng"`
	SORCode                             *string `canempty:"" UpEng:"DesignerEng"`
	SORCompleted                        *int8   `canempty:"" UpEng:"SystemEng"`
	SORCompletionTime                   *string `canempty:"" UpEng:"DesignerEng"`
	NeedTargetCost                      *int8   `canempty:"" UpEng:"BuyerEng"`
	TargetCostPlanCompletion            *string `canempty:"" UpEng:"BuyerEng"`
	CostAnalysisReportNumber            *string `canempty:"" UpEng:"BuyerEng"`
	TargetCostCompleted                 *int8   `canempty:"" UpEng:"BuyerEng"`
	TargetCostCompletionTime            *string `canempty:"" UpEng:"BuyerEng"`
	NeedFeasibilityAnalysis             *int8   `canempty:"" UpEng:"DesignerEng"`
	FeasibilityAnalysisPlanCompletion   *string `canempty:"" UpEng:"DesignerEng"`
	FeasibilityAnalysisReportNumber     *string `canempty:"" UpEng:"DesignerEng"`
	FeasibilityAnalysisCompleted        *int8   `canempty:"" UpEng:"SystemEng"`
	FeasibilityAnalysisCompletionTime   *string `canempty:"" UpEng:"DesignerEng"`
	NeedComplianceAnalysis              *int8   `canempty:"" UpEng:"DesignerEng"`
	ComplianceAnalysisPlanCompletion    *string `canempty:"" UpEng:"DesignerEng"`
	ComplianceAnalysisReportNumber      *string `canempty:"" UpEng:"DesignerEng"`
	ComplianceAnalysisCompleted         *int8   `canempty:"" UpEng:"SystemEng"`
	ComplianceAnalysisCompletionTime    *string `canempty:"" UpEng:"DesignerEng"`
	DesignConceptBookPlanCompletionTime *string `canempty:"" UpEng:"DesignerEng"`
	DesignConceptBookNumber             *string `canempty:"" UpEng:"DesignerEng"`
	DesignConceptBookCompleted          *int8   `canempty:"" UpEng:"SystemEng"`
	DesignConceptBookCompletionTime     *string `canempty:"" UpEng:"DesignerEng"`
	NeedArrangeFeasibilityReport        *int8   `canempty:"" UpEng:"DesignerEng"`
	ClothOrderPlanCompletion            *string `canempty:"" UpEng:"DesignerEng"`
	ClothOrderPlanCompleted             *int8   `canempty:"" UpEng:"DesignerEng"`
	M0ReleasePlanCompletion             *string `canempty:"" UpEng:"DesignerEng"`
	M0Version                           *string `canempty:"" UpEng:"DesignerEng"`
	M0Release                           *int8   `canempty:"" UpEng:"SystemEng"`
	M0CompletionTime                    *string `canempty:"" UpEng:"DesignerEng"`
}
type inputUpdatePart_Cam1_2 struct {
	Token  *string `maxlen:"22" ignoreUpCheck:""`
	PartId *uint32 `ignoreUpCheck:""`

	ArrangementFrozen                      *int8   `canempty:"" UpEng:"DesignerEng"`
	M1ReleasePlanCompletion                *string `canempty:"" UpEng:"DesignerEng"`
	M1Version                              *string `canempty:"" UpEng:"DesignerEng"`
	M1Release                              *int8   `canempty:"" UpEng:"SystemEng"`
	M1CompletionTime                       *string `canempty:"" UpEng:"DesignerEng"`
	CTSElectronicVersionPlanCompletionTime *string `canempty:"" UpEng:"DesignerEng"`
	CTSNumber                              *string `canempty:"" UpEng:"DesignerEng"`
	CTSElectronicVersionCompleted          *int8   `canempty:"" UpEng:"SystemEng"`
	CTSElectronicVersionCompletionTime     *string `canempty:"" UpEng:"DesignerEng"`
	CTSSignaturePlanCompletionTime         *string `canempty:"" UpEng:"DesignerEng"`
	CTSSignatureCompleted                  *int8   `canempty:"" UpEng:"SystemEng"`
	CTSSignatureCompletionTime             *string `canempty:"" UpEng:"DesignerEng"`
	DecisionPlanCompletionTime             *string `canempty:"" UpEng:"BuyerEng"`
	DecisionBookNumber                     *string `canempty:"" UpEng:"BuyerEng"`
	DecisionIsCompleted                    *int8   `canempty:"" UpEng:"BuyerEng"`
	DecisionCompletionTime                 *string `canempty:"" UpEng:"BuyerEng"`
	BusinessAgreementCompleted             *int8   `canempty:"" UpEng:"BuyerEng"`
	BusinessAgreementNumber                *string `canempty:"" UpEng:"BuyerEng"`
	BusinessAgreementFilingCompletionTime  *string `canempty:"" UpEng:"BuyerEng"`
}
type inputUpdatePart_Cam2_3 struct {
	Token  *string `maxlen:"22" ignoreUpCheck:""`
	PartId *uint32 `ignoreUpCheck:""`

	TechnicalAgreementPlanCompletionTime             *string `canempty:"" UpEng:"DesignerEng"`
	TechnicalAgreementNumber                         *string `canempty:"" UpEng:"DesignerEng"`
	TechnicalAgreementCompleted                      *int8   `canempty:"" UpEng:"SystemEng"`
	TechnicalAgreementFilingCompletionTime           *string `canempty:"" UpEng:"DesignerEng"`
	DesignLift                                       *string `canempty:"" UpEng:"DesignerEng"`
	QualityAgreementPlanCompletionTime               *string `canempty:"" UpEng:"SQEEng"`
	QualityNumber                                    *string `canempty:"" UpEng:"SQEEng"`
	QualityAgreementCompleted                        *int8   `canempty:"" UpEng:"SQEEng"`
	QualityAgreementFilingCompletionTime             *string `canempty:"" UpEng:"SQEEng"`
	ThreePacksOfTime                                 *string `canempty:"" UpEng:"SQEEng"`
	IPTV                                             *string `canempty:"" UpEng:"SQEEng"`
	PPM                                              *string `canempty:"" UpEng:"SQEEng"`
	DFMEAElectronicVersionPlanCompletionTime         *string `canempty:"" UpEng:"DesignerEng"`
	DFMEANumber                                      *string `canempty:"" UpEng:"DesignerEng"`
	DFMEAlectronicVersionCompleted                   *int8   `canempty:"" UpEng:"SystemEng"`
	DFMEAElectronicVersionCompletionTime             *string `canempty:"" UpEng:"DesignerEng"`
	DFMEASignaturePlanCompletionTime                 *string `canempty:"" UpEng:"DesignerEng"`
	DFMEASignatureCompleted                          *int8   `canempty:"" UpEng:"SystemEng"`
	DFMEASignatureCompletionTime                     *string `canempty:"" UpEng:"DesignerEng"`
	DVPElectronicVersionPlanCompletionTime           *string `canempty:"" UpEng:"DesignerEng"`
	DVPNumber                                        *string `canempty:"" UpEng:"DesignerEng"`
	DVPlectronicVersionCompleted                     *int8   `canempty:"" UpEng:"SystemEng"`
	DVPElectronicVersionCompletionTime               *string `canempty:"" UpEng:"DesignerEng"`
	DVPSignaturePlanCompletionTime                   *string `canempty:"" UpEng:"DesignerEng"`
	DVPSignatureCompleted                            *int8   `canempty:"" UpEng:"SystemEng"`
	DVPSignatureCompletionTime                       *string `canempty:"" UpEng:"DesignerEng"`
	NeedAssemblyAdjustmentInstructions               *int8   `canempty:"" UpEng:"DesignerEng"`
	AssemblyAdjustmentInstructionsPlanCompletionTime *string `canempty:"" UpEng:"DesignerEng"`
	AssemblyAdjustmentInstructionsNumber             *string `canempty:"" UpEng:"DesignerEng"`
	AssemblyAdjustmentInstructionsCompleted          *int8   `canempty:"" UpEng:"SystemEng"`
	AssemblyAdjustmentInstructionsCompletionTime     *string `canempty:"" UpEng:"DesignerEng"`
	DigitalMoldFreezingPlanCompletionTime            *string `canempty:"" UpEng:"DesignerEng"`
	ChecklistPlanCompletionTime                      *string `canempty:"" UpEng:"DesignerEng"`
	ChecklistCompleted                               *int8   `canempty:"" UpEng:"DesignerEng"`
	ChecklistCompletionTime                          *string `canempty:"" UpEng:"DesignerEng"`
	DigitalMoldFreezing                              *int8   `canempty:"" UpEng:"SystemEng"`
	M2DataVersion                                    *string `canempty:"" UpEng:"DesignerEng"`
	DigitalMoldFreezingCompletionTime                *string `canempty:"" UpEng:"DesignerEng"`
	TwoDPlanCompletionTime                           *string `canempty:"" UpEng:"DesignerEng"`
	TwoDNumber                                       *string `canempty:"" UpEng:"DesignerEng"`
	TwoDCompleted                                    *int8   `canempty:"" UpEng:"SystemEng"`
	TwoDCompletionTime                               *string `canempty:"" UpEng:"DesignerEng"`
	NeedGDT                                          *int8   `canempty:"" UpEng:"SizeEng"`
	GDTPlanCompletionTime                            *string `canempty:"" UpEng:"SizeEng"`
	GDTNumber                                        *string `canempty:"" UpEng:"SizeEng"`
	GDTCompleted                                     *int8   `canempty:"" UpEng:"SizeEng"`
	GDTCompletionTime                                *string `canempty:"" UpEng:"SizeEng"`
	SupplyStatusId                                   *uint32 `canempty:"" UpEng:"DesignerEng"`
	FirstSamplePlanTime                              *string `canempty:"" UpEng:"BuyerEng"`
	FirstSampleCompleted                             *int8   `canempty:"" UpEng:"BuyerEng"`
	FirstSampleTime                                  *string `canempty:"" UpEng:"BuyerEng"`
	SampleProgress                                   *string `canempty:"" UpEng:"BuyerEng"`
	NeedKPC                                          *int8   `canempty:"" UpEng:"DesignerEng"`
	KPCPlanCompletionTime                            *string `canempty:"" UpEng:"DesignerEng"`
	KPCNumber                                        *string `canempty:"" UpEng:"DesignerEng"`
	KPCCompleted                                     *int8   `canempty:"" UpEng:"SystemEng"`
	KPCCompletionTime                                *string `canempty:"" UpEng:"DesignerEng"`
	DisposalMethod                                   *string `canempty:"" UpEng:"DesignerEng"`
	TestResults                                      *string `canempty:"" UpEng:"SQEEng"`
	DvNeed                                           *int8   `canempty:"" UpEng:"DesignerEng"`
	DVPlanCompletionTime                             *string `canempty:"" UpEng:"TestEng"`
	DVNumber                                         *string `canempty:"" UpEng:"TestEng"`
	DvCompleted                                      *int8   `canempty:"" UpEng:"TestEng"`
	DVCompletionTime                                 *string `canempty:"" UpEng:"TestEng"`
	AllCompletedPlanTime                             *string `canempty:"" UpEng:"SQEEng"`
	AllCompletedTime                                 *string `canempty:"" UpEng:"SQEEng"`
	MaturityState                                    *int8   `canempty:"" UpEng:"SQEEng"`
}
type inputUpdatePart_Cam3_4 struct {
	Token  *string `maxlen:"22" ignoreUpCheck:""`
	PartId *uint32 `ignoreUpCheck:""`

	NeedPv                            *int8   `canempty:"" UpEng:"DesignerEng"`
	PVPlanCompletionTime              *string `canempty:"" UpEng:"DesignerEng"`
	PVNumber                          *string `canempty:"" UpEng:"TestEng"`
	PvCompleted                       *int8   `canempty:"" UpEng:"TestEng"`
	PVCompletionTime                  *string `canempty:"" UpEng:"TestEng"`
	MeetTheRequirements               *int8   `canempty:"" UpEng:"CrefftauEng"`
	ProcessAnalysisFeasible           *int8   `canempty:"" UpEng:"DesignerEng"`
	ProcessAnalysisPlanCompletionTime *string `canempty:"" UpEng:"DesignerEng"`
	ProcessAnalysisCompletionTime     *string `canempty:"" UpEng:"DesignerEng"`
	M3PaymentDone                     *int8   `canempty:"" UpEng:"DesignerEng"`
	ChecklistPlanCompletionTime       *string `canempty:"" UpEng:"DesignerEng"`
	ChecklistCompleted                *int8   `canempty:"" UpEng:"DesignerEng"`
	ChecklistCompletionTime           *string `canempty:"" UpEng:"DesignerEng"`
	M3PlanCompletionTime              *string `canempty:"" UpEng:"SystemEng"`
	M3CompletionTime                  *string `canempty:"" UpEng:"DesignerEng"`
	NeedGDT                           *int8   `canempty:"" UpEng:"SizeEng"`
	GDTPlanCompletionTime             *string `canempty:"" UpEng:"SizeEng"`
	GDTNumber                         *string `canempty:"" UpEng:"SizeEng"`
	GDTCompleted                      *int8   `canempty:"" UpEng:"SizeEng"`
	GDTCompletionTime                 *string `canempty:"" UpEng:"SizeEng"`
	AllCompletedPlanTime              *string `canempty:"" UpEng:"SQEEng"`
	AllCompletedTime                  *string `canempty:"" UpEng:"SQEEng"`
	MaturityState                     *int8   `canempty:"" UpEng:"SQEEng"`
}
type inputUpdatePart_Cam4_5 struct {
	Token  *string `maxlen:"22" ignoreUpCheck:""`
	PartId *uint32 `ignoreUpCheck:""`

	BMapPlanCompletionTime                       *string `canempty:"" UpEng:"DesignerEng"`
	BMapNumber                                   *string `canempty:"" UpEng:"DesignerEng"`
	BMapPaymentDone                              *int8   `canempty:"" UpEng:"SystemEng"`
	BMapCompletionTime                           *string `canempty:"" UpEng:"DesignerEng"`
	OpenModeCommand                              *int8   `canempty:"" UpEng:"BuyerEng"`
	FirstModulePieces                            *int8   `canempty:"" UpEng:"BuyerEng"`
	FirstModulePiecesPlanCompletionTime          *string `canempty:"" UpEng:"BuyerEng"`
	NeedTestModel                                *int8   `canempty:"" UpEng:"BuyerEng"`
	ProgramConfirmed                             *int8   `canempty:"" UpEng:"BuyerEng"`
	TestModelcomplete                            *int8   `canempty:"" UpEng:"BuyerEng"`
	TestModelPlanCompletionTime                  *string `canempty:"" UpEng:"BuyerEng"`
	TestModelCompletionTime                      *string `canempty:"" UpEng:"BuyerEng"`
	InspectionPlanTime                           *string `canempty:"" UpEng:"SQEEng"`
	InspectionTime                               *string `canempty:"" UpEng:"SQEEng"`
	AcceptanceResults                            *int8   `canempty:"" UpEng:"SQEEng"`
	SupplierSelfChecklistPlanCompletionTime      *string `canempty:"" UpEng:"SQEEng"`
	SupplierSelfChecklistCompleted               *int8   `canempty:"" UpEng:"SQEEng"`
	SupplierSelfChecklistCompletionTime          *string `canempty:"" UpEng:"SQEEng"`
	OTSSamplingPlan                              *string `canempty:"" UpEng:"DesignerEng"`
	OTSTestResult                                *int8   `canempty:"" UpEng:"SQEEng"`
	OTSSamplingTime                              *string `canempty:"" UpEng:"SQEEng"`
	NeedDimensionApproval                        *int8   `canempty:"" UpEng:"DesignerEng"`
	DimensionApprovalPlanCompletionTime          *string `canempty:"" UpEng:"DesignerEng"`
	DimensionApprovalNumber                      *string `canempty:"" UpEng:"DesignerEng"`
	DimensionApprovalCompleted                   *int8   `canempty:"" UpEng:"SystemEng"`
	DimensionApprovalCompletionTime              *string `canempty:"" UpEng:"DesignerEng"`
	AssemblyApprovalPlanSendTime                 *string `canempty:"" UpEng:"CrefftauEng"`
	AssemblyApprovalPlanCompletionTime           *string `canempty:"" UpEng:"CrefftauEng"`
	AssemblyApprovalNumber                       *string `canempty:"" UpEng:"CrefftauEng"`
	AssemblyApprovalCompleted                    *int8   `canempty:"" UpEng:"CrefftauEng"`
	AssemblyApprovalCompletionTime               *string `canempty:"" UpEng:"CrefftauEng"`
	NeedExteriorApproval                         *int8   `canempty:"" UpEng:"DesignerEng"`
	ExteriorApprovalPlanSendTime                 *string `canempty:"" UpEng:"DesignerEng"`
	ExteriorApprovalPlanCompletionTime           *string `canempty:"" UpEng:"DesignerEng"`
	ExteriorApprovalNumber                       *string `canempty:"" UpEng:"DesignerEng"`
	ExteriorApprovalCompleted                    *int8   `canempty:"" UpEng:"SystemEng"`
	ExteriorApprovalCompletionTime               *string `canempty:"" UpEng:"DesignerEng"`
	NeedRegulatoryExteriorApproval               *int8   `canempty:"" UpEng:"DesignerEng"`
	RegulatoryExteriorApprovalPlanCompletionTime *string `canempty:"" UpEng:"DesignerEng"`
	RegulatoryExteriorApprovalNumber             *string `canempty:"" UpEng:"DesignerEng"`
	RegulatoryExteriorApprovalCompleted          *int8   `canempty:"" UpEng:"SystemEng"`
	RegulatoryExteriorApprovalCompletionTime     *string `canempty:"" UpEng:"DesignerEng"`
	NeedBanned                                   *int8   `canempty:"" UpEng:"DesignerEng"`
	BannedPlanSendTime                           *string `canempty:"" UpEng:"DesignerEng"`
	BannedSendCompleted                          *int8   `canempty:"" UpEng:"DesignerEng"`
	BannedPlanCompletionTime                     *string `canempty:"" UpEng:"DesignerEng"`
	BannedNumber                                 *string `canempty:"" UpEng:"DesignerEng"`
	BannedCompleted                              *int8   `canempty:"" UpEng:"SystemEng"`
	BannedCompletionTime                         *string `canempty:"" UpEng:"DesignerEng"`
	NeedMaterialTest                             *int8   `canempty:"" UpEng:"TestEng"`
	MaterialTestPlanCompletionTime               *string `canempty:"" UpEng:"TestEng"`
	MaterialTestNumber                           *string `canempty:"" UpEng:"TestEng"`
	MaterialTestCompleted                        *int8   `canempty:"" UpEng:"TestEng"`
	MaterialTestCompletionTime                   *string `canempty:"" UpEng:"TestEng"`
	NeedPerformanceTest                          *int8   `canempty:"" UpEng:"DesignerEng"`
	PerformanceTestPlanCompletionTime            *string `canempty:"" UpEng:"TestEng"`
	PerformanceTestNumber                        *string `canempty:"" UpEng:"TestEng"`
	PerformanceTestCompleted                     *int8   `canempty:"" UpEng:"TestEng"`
	PerformanceTestCompletionTime                *string `canempty:"" UpEng:"TestEng"`
	NeedFirstRoadTest                            *int8   `canempty:"" UpEng:"DesignerEng"`
	FirstRoadTestNumber                          *string `canempty:"" UpEng:"DesignerEng"`
	FirstRoadTestResult                          *int8   `canempty:"" UpEng:"TestEng"`
	FirstRoadTestCompletionTime                  *string `canempty:"" UpEng:"DesignerEng"`
	NeedSecondRoadTest                           *int8   `canempty:"" UpEng:"DesignerEng"`
	SecondRoadTestNumber                         *string `canempty:"" UpEng:"DesignerEng"`
	SecondRoadTestResult                         *int8   `canempty:"" UpEng:"TestEng"`
	SecondRoadTestCompletionTime                 *string `canempty:"" UpEng:"DesignerEng"`
	NeedMB                                       *int8   `canempty:"" UpEng:"NewqualityEng"`
	MB1PlanCompletionTime                        *string `canempty:"" UpEng:"NewqualityEng"`
	MB1Number                                    *string `canempty:"" UpEng:"NewqualityEng"`
	MB1CompletionTime                            *string `canempty:"" UpEng:"NewqualityEng"`
	MB1PlanCompletion                            *int8   `canempty:"" UpEng:"NewqualityEng"`
	MB2PlanCompletionTime                        *string `canempty:"" UpEng:"NewqualityEng"`
	MB2Number                                    *string `canempty:"" UpEng:"NewqualityEng"`
	MB2CompletionTime                            *string `canempty:"" UpEng:"NewqualityEng"`
	MB2PlanCompletion                            *int8   `canempty:"" UpEng:"NewqualityEng"`
	MB3PlanCompletionTime                        *string `canempty:"" UpEng:"NewqualityEng"`
	MB3Number                                    *string `canempty:"" UpEng:"NewqualityEng"`
	MB3CompletionTime                            *string `canempty:"" UpEng:"NewqualityEng"`
	MB3PlanCompletion                            *int8   `canempty:"" UpEng:"NewqualityEng"`
	NeedOTS                                      *int8   `canempty:"" UpEng:"DesignerEng"`
	OTSPlanCompletionTime                        *string `canempty:"" UpEng:"DesignerEng"`
	RecognizedForm                               *int8   `canempty:"" UpEng:"DesignerEng"`
	OTSReportNo                                  *string `canempty:"" UpEng:"DesignerEng"`
	OTSCompleted                                 *int8   `canempty:"" UpEng:"SystemEng"`
	Archiving                                    *int8   `canempty:"" UpEng:"DesignerEng"`
	OTSCompletionTime                            *string `canempty:"" UpEng:"DesignerEng"`
	AllCompletedPlanTime                         *string `canempty:"" UpEng:"SQEEng"`
	AllCompletedTime                             *string `canempty:"" UpEng:"SQEEng"`
	MaturityState                                *int8   `canempty:"" UpEng:"SQEEng"`
}
type inputUpdatePart_Cam5_8 struct {
	Token  *string `maxlen:"22" ignoreUpCheck:""`
	PartId *uint32 `ignoreUpCheck:""`

	TwoTPCompletedPlanTime *string `canempty:"" UpEng:"SQEEng"`
	TwoTPCompletedTime     *string `canempty:"" UpEng:"SQEEng"`
	PPAPCompletedPlanTime  *string `canempty:"" UpEng:"SQEEng"`
	PPAPCompletedTime      *string `canempty:"" UpEng:"SQEEng"`
}

// @Summary 修改零件某个阶段信息
// @Description
// @Param _ body controllers.PartCT.inputUpdatePart_CamHeader true
// @Success 200 {object} string
// @router /UpdatePart_Cam/ [post]
func (this *PartController) UpdatePart_Cam() {
	inHeader := inputUpdatePart_CamHeader{}
	if err := this.GetInputStruct(&inHeader); err != nil {
		return
	}
	//获得对应的输入结构
	updatePart, ok := regUpApi["UpdatePart_"+*inHeader.Cam]
	if !ok {
		this.BackInfo.Message = "不存在的UpdatePart!"
		return
	}
	in := reflect.New(reflect.TypeOf(updatePart).Elem()).Interface() //新建一个，否则需要加锁
	if err := this.GetInputStruct(in); err != nil {
		return
	}
	//检查每个字段的权限
	if !this.CheckUpdatePower(*inHeader.Token, in, *inHeader.PartId) {
		return
	}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	cam, ok := nameByCam[*inHeader.Cam]
	if !ok {
		this.BackInfo.Message = "不存在的阶段:" + *inHeader.Cam
		return
	}
	val := reflect.New(cam)
	val.Elem().FieldByName("Part").Set(reflect.ValueOf(&sqlmodel.Part{Id: *inHeader.PartId}))
	m := val.Interface()
	if err := o.Read(m, "Part"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的零件!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	o.LoadRelated(val.Elem().FieldByName("Part").Interface(), "Project")
	projectId := val.Elem().FieldByName("Part").Elem().FieldByName("Project").Elem().FieldByName("Id").Interface().(uint32)
	if !this.UpdateModel(o, projectId, *inHeader.PartId, m, in, *inHeader.Token) {
		return
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功!"
}
