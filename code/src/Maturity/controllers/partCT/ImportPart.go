package partCT

import (
	"Maturity/controllers"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
	"github.com/tealeg/xlsx"
	"os"
	"strconv"
	"strings"
)

const (
	colLen = 10
)

var rowTitle []string

// @Summary 导入零件
// @Description 导入零件;excel导入
// @Param Token formData string true "令牌"
// @Param ProjectId formData uint32 true "项目ID"
// @Param excel formData file true "excel模版文件"
// @Success 200 {object} string
// @router /ImportPart/ [post]
func (this *PartController) ImportPart() {
	defer func() {
		if !this.BackInfo.Status {
			this.BackInfo.Message = "[导入失败]:" + this.BackInfo.Message
		}
	}()
	rowTitle = make([]string, colLen)
	Token := this.GetString("Token")
	ProjectId, err := this.GetUint32("ProjectId")
	if err != nil {
		this.BackInfo.Message = "ProjectId错误" + err.Error()
		return
	}
	u, err := userMD.GetUser(Token)
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	if u == nil {
		this.BackInfo.Message = "令牌错误~"
		return
	}
	this.SetUserInfo(Token, u)

	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	defer o.Rollback()

	f, _, err := this.GetFile("excel")
	if err != nil {
		this.BackInfo.Message = "请上传正确的Excel文件"
		return
	}
	defer f.Close()
	Filename := "static/upload/" + controllers.UUID22()
	if err := this.SaveToFile("excel", Filename); err != nil { // 保存位置在 static/upload, 没有文件夹要先创建
		this.BackInfo.Message = "请上传正确的excel文件" + err.Error()
		return
	}
	defer os.Remove(Filename)

	xlFile, err := xlsx.OpenFile(Filename)
	if err != nil {
		this.BackInfo.Message = "请上传正确的excel文件" + err.Error()
		return
	}
	cnt := int64(0)
	for _, sheet := range xlFile.Sheets {
		for i, row := range sheet.Rows {
			f, _ := row.Cells[0].String()
			if f == "" { //第一列未空,则认为已经导入完所有数据
				break
			}
			if colLen != len(row.Cells) {
				this.BackInfo.Message = "第" + strconv.FormatInt(int64(i)+1, 10) + "行数据列数错误,请保证上传了正确的模版,并且没有增加列数!"
				return
			}
			if i == 0 {
				for j, v := range row.Cells {
					if rowTitle[j], err = v.String(); err != nil {
						this.ErrLog(err)
						this.BackInfo.Message = "服务器出错啦~"
						return
					}
				}
				continue
			}

			m, ok := rowToModel(this, row.Cells, i+1, o, ProjectId)
			if !ok {
				return
			}
			m.Project = &sqlmodel.Project{Id: ProjectId}
			if !this.addPart(o, m) {
				return
			}
			cnt++
		}
		break //暂时值读取第一个sheet
	}

	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功导入" + strconv.FormatInt(cnt, 10) + "个零件"
}

func rowToModel(this *PartController, row []*xlsx.Cell, rowid int, o orm.Ormer, ProjectId uint32) (m *sqlmodel.Part, status bool) {
	m = &sqlmodel.Part{}
	//所属专业
	str, ok := getField(this, row, rowid, 0)
	if !ok {
		return
	}
	//检查是否为这个专业的系统集成工程师
	SystemEng := sqlmodel.SystemEng{}
	if err := o.QueryTable(&sqlmodel.SystemEng{}).Filter("User", this.GetUserInfo(this.GetString("Token"))).Filter("Profession__Name", str).Filter("Project", ProjectId).One(&SystemEng); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行的[" + rowTitle[0] + "]:" + str + ",您没有导入该专业的权限!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	m.Profession = SystemEng.Profession
	//零件名称
	m.Name, ok = getField(this, row, rowid, 1)
	if !ok {
		return
	}
	//零件号
	m.PartCode, ok = getField(this, row, rowid, 2)
	if !ok {
		return
	}
	if !this.CheckPartCode(m.PartCode) {
		this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行的[" + rowTitle[2] + "]:" + m.PartCode + ",格式错误!"
		return
	}

	//箭头系统
	str, ok = getField(this, row, rowid, 3)
	if !ok {
		return
	}
	m.ArrowSystem = &sqlmodel.ArrowSystem{Name: str}
	if err := o.Read(m.ArrowSystem, "Name"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行的[" + rowTitle[3] + "]:" + str + ",该箭头系统不存在!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	//单车用量
	str, ok = getField(this, row, rowid, 4)
	if !ok {
		return
	}
	m.SingleUseNum = str
	////质量特性等级
	//str, ok = getField(this, row, rowid, 5)
	//if !ok {
	//	return
	//}
	//m.QualityLevel = &sqlmodel.QualityLevel{Name: str}
	//if err := o.Read(m.QualityLevel, "Name"); err != nil {
	//	if err == orm.ErrNoRows {
	//		this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行的[" + rowTitle[5] + "]:" + str + "非法!"
	//		return
	//	}
	//	this.ErrLog(err)
	//	this.BackInfo.Message = "服务器出错啦~"
	//	return
	//}
	////开发类型
	//str, ok = getField(this, row, rowid, 6)
	//if !ok {
	//	return
	//}
	//m.DevelopmentType = &sqlmodel.DevelopmentType{Name: str}
	//if err := o.Read(m.DevelopmentType, "Name"); err != nil {
	//	if err == orm.ErrNoRows {
	//		this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行的[" + rowTitle[6] + "]:" + str + "非法!"
	//		return
	//	}
	//	this.ErrLog(err)
	//	this.BackInfo.Message = "服务器出错啦~"
	//	return
	//}
	////供应商名称
	//m.SupplierName, ok = getField(this, row, rowid, 7)
	//if !ok {
	//	return
	//}
	////供应商代码
	//m.SupplierCode, ok = getField(this, row, rowid, 8)
	//if !ok {
	//	return
	//}
	////配置类型
	//m.Configuration, ok = getField(this, row, rowid, 9)
	//if !ok {
	//	return
	//}
	////技术集成工程师
	//if m.IntegreiddioTechnolegEng, ok = getEng(this, row, rowid, 10, o); !ok {
	//	return
	//}
	//设计工程师
	if m.DesignerEng, ok = getEng(this, row, rowid, 5, o); !ok {
		return
	}
	//采购工程师
	if m.BuyerEng, ok = getEng(this, row, rowid, 6, o); !ok {
		return
	}
	////供应商商务
	//if m.SupplierBusnes, ok = getField(this, row, rowid, 7); !ok {
	//	return
	//}
	////供应商技术
	//if m.SupplierTec, ok = getField(this, row, rowid, 8); !ok {
	//	return
	//}
	//SQE工程师
	if m.SQEEng, ok = getEng(this, row, rowid, 7, o); !ok {
		return
	}
	//工艺工程师
	if m.CrefftauEng, ok = getEng(this, row, rowid, 8, o); !ok {
		return
	}
	//试验工程师
	if m.TestEng, ok = getEng(this, row, rowid, 9, o); !ok {
		return
	}

	status = true
	return
}

func getField(this *PartController, row []*xlsx.Cell, rowid, colid int, canEmpty ...bool) (ret string, status bool) {
	ret, _ = row[colid].String()
	if len(canEmpty) > 0 && !canEmpty[0] && ret == "" {
		this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行第" + strconv.FormatInt(int64(colid+1), 10) + "列的[" + rowTitle[colid] + "]:" + ret + "不能为空!"
		return
	}
	status = true
	return
}

func getEng(this *PartController, row []*xlsx.Cell, rowid, colid int, o orm.Ormer) (u *sqlmodel.User, status bool) {
	str, ok := getField(this, row, rowid, colid)
	if !ok {
		return
	}
	u = &sqlmodel.User{JobNumber: strings.ToLower(str)}
	if err := o.Read(u, "JobNumber"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行的[" + rowTitle[colid] + "]:" + str + "非法!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	status = true
	return
}
