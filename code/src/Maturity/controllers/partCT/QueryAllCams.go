package partCT

import (
	"Maturity/models/userMD"
)
type inputQueryAllCams struct {
	Token *string `maxlen:"22"`
}

// @Summary 查询各个阶段零件的分类和所有字段信息
// @Description
// @Param _ body controllers.partCT.inputQueryAllCams true
// @Success 200 {object} []controllers.partCT.CamsList
// @router /QueryAllCams/ [post]
func (this *PartController) QueryAllCams() {
	in := inputQueryAllCams{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !userMD.CheckUserToken(*in.Token) {
		this.BackInfo.Message = "令牌错误"
		return
	}
	this.BackInfo.Data = CamsList
	this.BackInfo.Status = true
}

