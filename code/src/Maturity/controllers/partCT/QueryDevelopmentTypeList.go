package partCT

import (
	"Maturity/models/PartMD"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
)

type inputQueryDevelopmentTypeList struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`

	Secrch *string `canempty:""`
}

// @Summary 开发类型列表
// @Description 任何人可以查询(无需权限)
// @Param _ body controllers.partCT.inputQueryDevelopmentTypeList true
// @Success 200 {object} models.partMD.QueryDevelopmentTypeList
// @router /QueryDevelopmentTypeList/ [post]
func (this *PartController) QueryDevelopmentTypeList() {
	in := inputQueryDevelopmentTypeList{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !userMD.CheckUserToken(*in.Token) {
		this.BackInfo.Message = "令牌错误"
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable("DevelopmentType")
	if in.Secrch != nil && *in.Secrch != "" {
		qs = qs.Filter("Name__icontains", *in.Secrch)
	}
	dinfo := PartMD.QueryDevelopmentTypeList{}
	var err error
	if dinfo.RecordsTotal, err = qs.Count(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	elems := []sqlmodel.DevelopmentType{}
	if _, err = qs.Limit(*in.Length, *in.Start).OrderBy("Id").RelatedSel().All(&elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	for _, e := range elems {
		elem := PartMD.QueryDevelopmentTypeElem{}
		elem.Id = e.Id
		elem.Name = e.Name
		dinfo.Elems = append(dinfo.Elems, elem)
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
