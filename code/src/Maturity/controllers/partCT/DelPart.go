package partCT

import (
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
	"time"
)

type inputDelPart struct {
	Token  *string `maxlen:"22"`
	PartId *uint32
}

// @Summary 删除零件
// @Description
// @Param _ body controllers.PartCT.inputDelPart true
// @Success 200 {object} string
// @router /DelPart/ [post]
func (this *PartController) DelPart() {
	in := inputDelPart{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	m := sqlmodel.Part{Id: *in.PartId}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	if err := o.Read(&m, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的零件!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	//检查是否为这个专业的系统集成工程师
	if !this.CheckSystemEngProfession(*in.Token, m.Project.Id, m.Profession.Id) {
		return
	}
	if _, err := o.LoadRelated(&m, "Project"); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	//删除只是标记已经删除
	if _, err := o.QueryTable(&m).Filter("Id", m.Id).Update(orm.Params{"Deled": true}); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	//删除所有阶段
	for k,v := range nameByCam{
		if k == "Part"{ //跳过Part
			continue
		}
		//删除阶段
		if _, err := o.QueryTable(v.Name()).Filter("Part", m.Id).Update(orm.Params{"Deled": true}); err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	//添加删除记录
	u := this.GetUserInfo(*in.Token)
	uph := sqlmodel.UpdateHistory{
		Type:        2,
		ProjectId:   m.Project.Id,
		ProjectName: m.Project.Name,
		PartId:      m.Id,
		PartName:    m.Name,
		UpUser:      u.Name,
		UpJobNumber: u.JobNumber,
		Ip:          this.Ctx.Request.RemoteAddr,
		UpTime:      time.Now(),
	}
	if _, err := o.Insert(&uph); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
