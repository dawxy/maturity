package partCT
//
//import (
//	"Maturity/controllers"
//	"Maturity/models/sqlmodel"
//	"Maturity/models/userMD"
//	"github.com/astaxie/beego/orm"
//	"github.com/tealeg/xlsx"
//	"os"
//	"reflect"
//	"strconv"
//	"strings"
//	"time"
//)
//
//const (
//	colLenAll = 62
//)
//
//// @Summary __用于内部导出，导入零件
//// @Description 导入零件;excel导入
//// @Param Token formData string true "令牌"
//// @Param ProjectId formData uint32 true "项目ID"
//// @Param excel formData file true "excel模版文件"
//// @Success 200 {object} string
//// @router /ImportPartAll/ [post]
//func (this *PartController) ImportPartAll() {
//	defer func() {
//		if !this.BackInfo.Status {
//			this.BackInfo.Message = "[导入失败]:" + this.BackInfo.Message
//		}
//	}()
//	rowTitle = make([]string, colLenAll)
//	Token := this.GetString("Token")
//	//ProjectId, err := this.GetUint32("ProjectId")
//	//if err != nil {
//	//	this.BackInfo.Message = "ProjectId错误" + err.Error()
//	//	return
//	//}
//	u, err := userMD.GetUser(Token)
//	if err != nil {
//		this.ErrLog(err)
//		this.BackInfo.Message = "服务器出错啦~"
//		return
//	}
//	if u == nil {
//		this.BackInfo.Message = "令牌错误~"
//		return
//	}
//	this.SetUserInfo(Token, u)
//
//	o := orm.NewOrm()
//	if err := o.Begin(); err != nil {
//		this.ErrLog(err)
//		this.BackInfo.Message = "服务器出错啦~"
//		return
//	}
//	defer o.Rollback()
//
//	f, _, err := this.GetFile("excel")
//	if err != nil {
//		this.BackInfo.Message = "请上传正确的Excel文件"
//		return
//	}
//	defer f.Close()
//	Filename := "static/upload/" + controllers.UUID22()
//	if err := this.SaveToFile("excel", Filename); err != nil { // 保存位置在 static/upload, 没有文件夹要先创建
//		this.BackInfo.Message = "请上传正确的excel文件" + err.Error()
//		return
//	}
//	defer os.Remove(Filename)
//
//	xlFile, err := xlsx.OpenFile(Filename)
//	if err != nil {
//		this.BackInfo.Message = "请上传正确的excel文件" + err.Error()
//		return
//	}
//	cnt := int64(0)
//	for _, sheet := range xlFile.Sheets {
//		for i, row := range sheet.Rows {
//			f, _ := row.Cells[0].String()
//			if f == "" { //第一列未空,则认为已经导入完所有数据
//				break
//			}
//			if colLenAll != len(row.Cells) {
//				this.BackInfo.Message = "第" + strconv.FormatInt(int64(i)+1, 10) + "行数据列数错误,请保证上传了正确的模版,并且没有增加列数!"
//				return
//			}
//			if i == 0 {
//				for j, v := range row.Cells {
//					if rowTitle[j], err = v.String(); err != nil {
//						this.ErrLog(err)
//						this.BackInfo.Message = "服务器出错啦~"
//						return
//					}
//				}
//				continue
//			}
//
//			////添加基础信息
//			//m, ok := rowToModelBase(this, row.Cells, i+1, o)
//			//if !ok {
//			//	return
//			//}
//			////存在则忽略，不存在新建
//			//if o.QueryTable(m).Filter("PartCode", m.PartCode).Exist() {
//			//	continue
//			//} else {
//			//	m.Project = &sqlmodel.Project{Id: ProjectId}
//			//	if !this.addPart(o, m) {
//			//		return
//			//	}
//			//}
//
//			//导入cam1
//			m := &sqlmodel.Cam3{}
//			partCode, ok := getField(this, row.Cells, i+1, 0)
//			if !ok {
//				return
//			}
//			m.Part = &sqlmodel.Part{PartCode: partCode}
//			if err := o.Read(m.Part, "PartCode"); err != nil {
//				if err == orm.ErrNoRows {
//					this.BackInfo.Message = "不存在的PartCode：" + partCode
//					return
//				}
//				this.BackInfo.Message = err.Error()
//				return
//			}
//			if err := o.Read(m, "Part"); err != nil {
//				if err == orm.ErrNoRows {
//					this.BackInfo.Message = "不存在的Part：" + partCode
//					return
//				}
//				this.BackInfo.Message = err.Error()
//				return
//			}
//			if !rowToModelCam(this, row.Cells, i+1, o, m) {
//				return
//			}
//			if _, err := o.Update(m); err != nil {
//				this.ErrLog(err)
//				this.BackInfo.Message = "服务器出错啦~"
//				return
//			}
//
//			//partId := strconv.FormatUint(uint64(m.Id), 10)
//			//profession_id := strconv.FormatUint(uint64(m.Profession.Id), 10)
//			//project_id := strconv.FormatUint(uint64(m.Project.Id), 10)
//			//阶段一
//
//			cnt++
//		}
//		break //暂时值读取第一个sheet
//	}
//
//	if err := o.Commit(); err != nil {
//		this.ErrLog(err)
//		this.BackInfo.Message = "服务器出错啦~"
//		return
//	}
//	this.BackInfo.Status = true
//	this.BackInfo.Message = "成功导入" + strconv.FormatInt(cnt, 10) + "个零件"
//}
//
//func rowToModelBase(this *PartController, row []*xlsx.Cell, rowid int, o orm.Ormer) (m *sqlmodel.Part, status bool) {
//	m = &sqlmodel.Part{}
//	//所属专业
//	str, ok := getField(this, row, rowid, 0)
//	if !ok {
//		return
//	}
//	//检查是否为这个专业的系统集成工程师
//	m.Profession = &sqlmodel.Profession{Name: str}
//	if err := o.Read(m.Profession, "Name"); err != nil {
//		if err == orm.ErrNoRows {
//			this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行的[" + rowTitle[0] + "]:" + str + ",不存在!"
//			return
//		}
//		this.ErrLog(err)
//		this.BackInfo.Message = "服务器出错啦~"
//		return
//	}
//	//零件名称
//	m.Name, ok = getField(this, row, rowid, 1)
//	if !ok {
//		return
//	}
//	//零件号
//	m.PartCode, ok = getField(this, row, rowid, 2)
//	if !ok {
//		return
//	}
//	if !this.CheckPartCode(m.PartCode) {
//		this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行的[" + rowTitle[2] + "]:" + m.PartCode + ",格式错误!"
//		return
//	}
//
//	//箭头系统
//	str, ok = getField(this, row, rowid, 3)
//	if !ok {
//		return
//	}
//	m.ArrowSystem = &sqlmodel.ArrowSystem{Name: str}
//	if err := o.Read(m.ArrowSystem, "Name"); err != nil {
//		if err == orm.ErrNoRows {
//			this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行的[" + rowTitle[3] + "]:" + str + ",该箭头系统不存在!"
//			return
//		}
//		this.ErrLog(err)
//		this.BackInfo.Message = "服务器出错啦~"
//		return
//	}
//	//单车用量
//	str, ok = getField(this, row, rowid, 4)
//	if !ok {
//		return
//	}
//	m.SingleUseNum = str
//	//质量特性等级
//	str, ok = getField(this, row, rowid, 5)
//	if !ok {
//		return
//	}
//	m.QualityLevel = &sqlmodel.QualityLevel{Name: str}
//	if err := o.Read(m.QualityLevel, "Name"); err != nil {
//		if err == orm.ErrNoRows {
//			this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行的[" + rowTitle[5] + "]:" + str + "非法!"
//			return
//		}
//		this.ErrLog(err)
//		this.BackInfo.Message = "服务器出错啦~"
//		return
//	}
//	//开发类型
//	str, ok = getField(this, row, rowid, 6)
//	if !ok {
//		return
//	}
//	m.DevelopmentType = &sqlmodel.DevelopmentType{Name: str}
//	if err := o.Read(m.DevelopmentType, "Name"); err != nil {
//		if err == orm.ErrNoRows {
//			this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行的[" + rowTitle[6] + "]:" + str + "非法!"
//			return
//		}
//		this.ErrLog(err)
//		this.BackInfo.Message = "服务器出错啦~"
//		return
//	}
//	//供应商名称
//	if m.SupplierName, ok = getField(this, row, rowid, 7); !ok {
//		return
//	}
//	//供应商代码
//	if m.SupplierCode, ok = getField(this, row, rowid, 8); !ok {
//		return
//	}
//	////配置类型
//	//m.Configuration = &sqlmodel.Configuration{Id: 1}
//	//if !ok {
//	//	return
//	//}
//	//设计工程师
//	if m.DesignerEng, ok = getEngByName(this, row, rowid, 11, o); !ok {
//		return
//	}
//	//采购工程师
//	if m.BuyerEng, ok = getEngByName(this, row, rowid, 12, o); !ok {
//		return
//	}
//	////供应商商务
//	//if m.SupplierBusnes, ok = getField(this, row, rowid, 12); !ok {
//	//	return
//	//}
//	////供应商技术
//	//if m.SupplierTec, ok = getField(this, row, rowid, 13); !ok {
//	//	return
//	//}
//	//SQE工程师
//	if m.SQEEng, ok = getEngByName(this, row, rowid, 15, o); !ok {
//		return
//	}
//	//工艺工程师
//	if m.CrefftauEng, ok = getEngByName(this, row, rowid, 16, o); !ok {
//		return
//	}
//	//试验工程师
//	if m.TestEng, ok = getEngByName(this, row, rowid, 17, o); !ok {
//		return
//	}
//
//	status = true
//	return
//}
//
//func rowToModelCam(this *PartController, row []*xlsx.Cell, rowid int, o orm.Ormer, ret interface{}) (status bool) {
//	//检查所有Tag的限制
//	//names := reflect.TypeOf(ret).Elem()
//	values := reflect.ValueOf(ret).Elem()
//	colId := 1
//	for i := 4; i < values.NumField(); i++ {
//		if colId == colLenAll {
//			break
//		}
//		//k := names.Field(i)
//		v := values.Field(i)
//		//if colId == 29 {
//		//	s := sqlmodel.SupplyStatus{}
//		//	for i := 0; i < 7; i++ {
//		//		str, ok := getField(this, row, rowid, 1, true)
//		//		if !ok {
//		//			return
//		//		}
//		//		if str != "" {
//		//			s.Name = rowTitle[colId+i]
//		//			break
//		//		}
//		//	}
//		//	if err := o.Read(&s, "Name"); err != nil {
//		//		this.BackInfo.Message = "不存在的供货状态：" + s.Name
//		//		return
//		//	}
//		//	v.Set(reflect.ValueOf(&s))
//		//	colId += 7
//		//	continue
//		//}
//		switch v.Kind() {
//		case reflect.Int8:
//			val, ok := getBoolField(this, row, rowid, colId)
//			if !ok {
//				return
//			}
//			v.Set(reflect.ValueOf(val))
//		case reflect.String:
//			val, ok := getField(this, row, rowid, colId)
//			if !ok {
//				return
//			}
//			v.Set(reflect.ValueOf(val))
//		case reflect.Int32:
//			val, ok := getInt32Field(this, row, rowid, colId)
//			if !ok {
//				return
//			}
//			v.Set(reflect.ValueOf(val))
//		default:
//			val, ok := getTimeField(this, row, rowid, colId)
//			if !ok {
//				return
//			}
//			v.Set(reflect.ValueOf(val))
//		}
//		colId++
//	}
//	status = true
//	return
//}
//
//func getEngByName(this *PartController, row []*xlsx.Cell, rowid, colid int, o orm.Ormer) (u *sqlmodel.User, status bool) {
//	str, ok := getField(this, row, rowid, colid, true)
//	if !ok {
//		return
//	}
//	if str == "" {
//		u = &sqlmodel.User{Id: 1}
//		status = true
//		return
//	}
//	u = &sqlmodel.User{Name: strings.ToLower(str)}
//	if err := o.Read(u, "Name"); err != nil {
//		if err == orm.ErrNoRows {
//			this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行的[" + rowTitle[colid] + "]:" + str + "非法!"
//			return
//		}
//		this.ErrLog(err)
//		this.BackInfo.Message = "服务器出错啦~"
//		return
//	}
//	status = true
//	return
//}
//
//func getTimeField(this *PartController, row []*xlsx.Cell, rowid, colid int) (ret time.Time, status bool) {
//	str, ok := getField(this, row, rowid, colid, true)
//	if !ok {
//		return
//	}
//	if str == "" || str == "/" || str == "\\" {
//		status = true
//		return
//	}
//	ret, err := time.Parse("01-02-06", strings.Replace(str, "/", "-", -1))
//	if err != nil {
//		if ret, err = time.Parse("20060102", str); err != nil {
//			this.ErrLog(err)
//			this.BackInfo.Message = "第" + strconv.FormatInt(int64(rowid), 10) + "行的[" + rowTitle[colid] + "]:" + str + "非法!"
//			return
//		}
//	}
//	status = true
//	return
//}
//
//func getBoolField(this *PartController, row []*xlsx.Cell, rowid, colid int) (ret int8, status bool) {
//	str, ok := getField(this, row, rowid, colid, true)
//	if !ok {
//		return
//	}
//	if str == "" || str == "/" || str == "\\" {
//		status = true
//		return
//	}
//	if str == "Y" {
//		ret = 1
//	} else {
//		ret = 0
//	}
//	status = true
//	return
//}
//
//func getInt32Field(this *PartController, row []*xlsx.Cell, rowid, colid int) (ret int32, status bool) {
//	str, ok := getField(this, row, rowid, colid, true)
//	if !ok {
//		return
//	}
//	if str == "" || str == "/" || str == "\\" {
//		status = true
//		return
//	}
//	it, err := strconv.ParseInt(str, 10, 32)
//	if err != nil {
//		this.ErrLog(err)
//		return
//	}
//	ret = int32(it)
//	status = true
//	return
//}
