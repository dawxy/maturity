package partCT

import (
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputUpdatePart_Import struct {
	Token                    *string `maxlen:"22" ignoreUpCheck:""`
	PartId                   *uint32 `ignoreUpCheck:""`
	ProfessionId             *uint32 `canempty:"" UpEng:"SystemEng"`
	Name                     *string `canempty:"" UpEng:"SystemEng"`
	PartCode                 *string `canempty:"" UpEng:"SystemEng"`
	ArrowSystemId            *uint32 `canempty:"" UpEng:"SystemEng"`
	SingleUseNum             *string `canempty:"" UpEng:"SystemEng"`
	QualityLevelId           *uint32 `canempty:"" UpEng:"DesignerEng"`
	DevelopmentTypeId        *uint32 `canempty:"" UpEng:"DesignerEng"`
	Configuration            *string `canempty:"" UpEng:"DesignerEng"` //配置类型
	SupplierName             *string `canempty:"" UpEng:"BuyerEng"`    //供应商名称
	SupplierCode             *string `canempty:"" UpEng:"BuyerEng"`    //供应商代码
	SupplierBusnes           *string `canempty:"" UpEng:"BuyerEng"`    //供应商商务
	SupplierTec              *string `canempty:"" UpEng:"BuyerEng"`    //供应商技术
	DesignerEngId            *uint32 `canempty:"" UpEng:"SystemEng" MultipleP:"DesignerEng"`
	BuyerEngId               *uint32 `canempty:"" UpEng:"SystemEng" MultipleP:"BuyerEng"`
	SQEEngId                 *uint32 `canempty:"" UpEng:"SystemEng" MultipleP:"SQEEng"`
	CrefftauEngId            *uint32 `canempty:"" UpEng:"SystemEng" MultipleP:"CrefftauEng"`
	TestEngId                *uint32 `canempty:"" UpEng:"SystemEng" MultipleP:"TestEng"`
	SizeEngId                *uint32 `canempty:"" UpEng:"SystemEng" MultipleP:"SizeEng"`
	NewqualityEngId          *uint32 `canempty:"" UpEng:"SystemEng" MultipleP:"NewqualityEng"`
	SupplierPhone            *string `canempty:"" UpEng:"BuyerEng"`
	PartSource               *string `canempty:"" UpEng:"DesignerEng"`
	ChangePlanCompletionTime *string `canempty:"" UpEng:"DesignerEng"`
	ChangeCompletionTime     *string `canempty:"" UpEng:"DesignerEng"`
	Workshop                 *string `canempty:"" UpEng:"CrefftauEng"`
	Section                  *string `canempty:"" UpEng:"CrefftauEng"`
	Station                  *string `canempty:"" UpEng:"CrefftauEng"`
	TargetWeight             *string `canempty:"" UpEng:"DesignerEng"`
	EstimatedWeight          *string `canempty:"" UpEng:"DesignerEng"`
	ActualWeight             *string `canempty:"" UpEng:"DesignerEng"`
}

// @Summary 修改零件基础信息
// @Description 除了Token和PartId必填，其他不填不修改
// @Param _ body controllers.PartCT.inputUpdatePart_Import true
// @Success 200 {object} string
// @router /UpdatePart_Base/ [post]
func (this *PartController) UpdatePart_Base() {
	in := inputUpdatePart_Import{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	//检查每个字段的权限
	if !this.CheckUpdatePower(*in.Token, &in, *in.PartId) {
		return
	}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	//检查零件代码是否已经存在
	if in.PartCode != nil && o.QueryTable(&sqlmodel.Part{}).Filter("PartCode", *in.PartCode).Exist() {
		this.BackInfo.Message = "该零件名已经存在!"
		return
	}
	m := sqlmodel.Part{Id: *in.PartId}
	if err := o.Read(&m, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的零件!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if !this.UpdateModel(o, m.Project.Id, *in.PartId, &m, &in, *in.Token) {
		return
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功!"
}
