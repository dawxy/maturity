package partCT

import (
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
	"strconv"
)

type inputAddPart struct {
	Token         *string `maxlen:"22"`
	ProjectId     *uint32
	ProfessionId  *uint32
	Name          *string
	PartCode      *string
	ArrowSystemId *uint32
	SingleUseNum  *string
	DesignerEngId *uint32
	BuyerEngId    *uint32
	SQEEngId      *uint32
	CrefftauEngId *uint32
	TestEngId     *uint32
}

// @Summary 添加零件
// @Description
// @Param _ body controllers.PartCT.inputAddPart true
// @Success 200 {object} string
// @router /AddPart/ [post]
func (this *PartController) AddPart() {
	in := inputAddPart{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !this.CheckPartCode(*in.PartCode) {
		this.BackInfo.Message = "零件号格式错误,请参考《汽车零部件编号原则》!"
		return
	}
	//检查是否为这个专业的系统集成工程师
	if !this.CheckSystemEngProfession(*in.Token, *in.ProjectId, *in.ProfessionId) {
		return
	}
	m := sqlmodel.Part{
		Project:      &sqlmodel.Project{Id: *in.ProjectId},
		Profession:   &sqlmodel.Profession{Id: *in.ProfessionId},
		Name:         *in.Name,
		PartCode:     *in.PartCode,
		ArrowSystem:  &sqlmodel.ArrowSystem{Id: *in.ArrowSystemId},
		SingleUseNum: *in.SingleUseNum,
		DesignerEng:  &sqlmodel.User{Id: *in.DesignerEngId},
		BuyerEng:     &sqlmodel.User{Id: *in.BuyerEngId},
		SQEEng:       &sqlmodel.User{Id: *in.SQEEngId},
		CrefftauEng:  &sqlmodel.User{Id: *in.CrefftauEngId},
		TestEng:      &sqlmodel.User{Id: *in.TestEngId},
	}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	if !this.addPart(o, &m) {
		return
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}

func (this *PartController) addPart(o orm.Ormer, m *sqlmodel.Part) (ret bool) {
	if o.QueryTable(&sqlmodel.Part{}).Filter("PartCode", m.PartCode).Filter("Project", m.Project).Filter("Deled", 0).Exist() {
		this.BackInfo.Message = m.PartCode + ":该项目下已经存在该零件号"
		return
	}
	if _, err := o.Insert(m); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	sql, err := orm.NewQueryBuilder("mysql")
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	partId := strconv.FormatUint(uint64(m.Id), 10)
	profession_id := strconv.FormatUint(uint64(m.Profession.Id), 10)
	project_id := strconv.FormatUint(uint64(m.Project.Id), 10)
	//添加所有阶段表
	for k,v := range nameByCam{
		if k == "Part"{ //跳过Part
			continue
		}
		if _, err := o.Raw(sql.InsertInto(v.Name(), "part_id", "profession_id", "project_id").Values(partId, profession_id, project_id).String()).Exec(); err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
		sql, err = orm.NewQueryBuilder("mysql")
		if err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	return true
}
