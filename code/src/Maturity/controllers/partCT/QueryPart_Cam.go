package partCT

import (
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
	"reflect"
)

type inputQueryPart_Cam struct {
	Token  *string `maxlen:"22"`
	PartId *uint32
	Cam *string
}

// @Summary 查询零件某个阶段的信息
// @Description
// @Param _ body controllers.PartCT.inputQueryPart_Cam true
// @Success 200 {object} orm.Params
// @router /QueryPart_Cam/ [post]
func (this *PartController) QueryPart_Cam() {
	in := inputQueryPart_Cam{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !userMD.CheckUserToken(*in.Token) {
		this.BackInfo.Message = "令牌错误"
		return
	}
	o := orm.NewOrm()
	cam, ok := nameByCam[*in.Cam]
	if !ok{
		this.BackInfo.Message = "不存在的阶段:"+*in.Cam
		return
	}
	val := reflect.New(cam)
	val.Elem().FieldByName("Part").Set(reflect.ValueOf(&sqlmodel.Part{Id: *in.PartId}))
	m := val.Interface()
	if err := o.QueryTable(m).Filter("Part", *in.PartId).RelatedSel().One(m); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的零件"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}

	dinfo := make(orm.Params, 0)
	this.SetModelBackData(m, dinfo)
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
