package partCT

import (
	"Maturity/models/PartMD"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
)

type inputQuerySystemEngProfessionList struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`

	Secrch    *string `canempty:""`
	ProjectId *uint32
}

// @Summary 查询某个项目下用户有系统集成工程师权限的所有专业列表
// @Description
// @Param _ body controllers.partCT.inputQuerySystemEngProfessionList true
// @Success 200 {object} models.partMD.QueryProfessionList
// @router /QuerySystemEngProfessionList/ [post]
func (this *PartController) QuerySystemEngProfessionList() {
	in := inputQuerySystemEngProfessionList{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !userMD.CheckUserToken(*in.Token) {
		this.BackInfo.Message = "令牌错误"
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable(&sqlmodel.SystemEng{}).Filter("User__Token", *in.Token)
	qs = qs.Filter("Project", *in.ProjectId)
	if in.Secrch != nil && *in.Secrch != "" {
		qs = qs.Filter("Profession__Name__icontains", *in.Secrch)
	}
	dinfo := PartMD.QueryProfessionList{}
	var err error
	if dinfo.RecordsTotal, err = qs.Count(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	elems := []sqlmodel.SystemEng{}
	if _, err = qs.Limit(*in.Length, *in.Start).OrderBy("Id").RelatedSel("Profession").All(&elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	for _, e := range elems {
		elem := PartMD.ProfessionElem{}
		elem.Id = e.Profession.Id
		elem.Name = e.Profession.Name
		dinfo.Elems = append(dinfo.Elems, elem)
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
