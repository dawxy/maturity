package partCT

import (
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
	"github.com/tealeg/xlsx"
	"time"
)

// @Summary 下载最新版人员姓名工号对应表
// @Description
// @Param Token formData string true "令牌"
// @Success 200 {object} string
// @router /DownloadAllUser/ [get]
func (this *PartController) DownloadAllUser() {
	Token := this.GetString("Token")

	if !userMD.CheckUserToken(Token) {
		this.BackInfo.Message = "令牌错误"
		return
	}

	xlFile := xlsx.NewFile()
	xlFile.AddSheet("人员信息")
	//写入所有用户工号对应列表
	sheet := xlFile.Sheets[0]
	//查询所有未停用的人员工号和姓名
	o := orm.NewOrm()
	users := []sqlmodel.User{}
	if _, err := o.QueryTable(&sqlmodel.User{}).Filter("IsStop", false).All(&users, "JobNumber", "Name"); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	row := sheet.AddRow()
	cell := row.AddCell()
	cell.Value = "数据更新时间:" + time.Now().Format("2006-01-02 15:04:05")
	row = sheet.AddRow()
	cell = row.AddCell()
	cell.Value = "姓名"
	cell = row.AddCell()
	cell.Value = "工号"
	for _, v := range users {
		row = sheet.AddRow()
		cell = row.AddCell()
		cell.Value = v.Name
		cell = row.AddCell()
		cell.Value = v.JobNumber
	}

	this.Ctx.Output.Header("Accept-Ranges", "bytes")
	this.Ctx.Output.Header("Content-Disposition", "attachment; filename="+time.Now().String()+".xlsx") //文件名
	this.Ctx.Output.Header("Cache-Control", "must-revalidate, post-check=0, pre-check=0")
	this.Ctx.Output.Header("Pragma", "no-cache")
	this.Ctx.Output.Header("Expires", "0")
	xlFile.Write(this.Ctx.ResponseWriter)

	this.SetNotBackJson() //不返回json
}
