package partCT

import (
	"Maturity/models/PartMD"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
)

type inputQueryPartList struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`

	ProjectId         *uint32
	Secrch            *string `canempty:""`
	PartCode          *string `canempty:""`
	Name              *string `canempty:""`
	ProfessionId      *uint32 `canempty:""`
	DevelopmentTypeId *uint32 `canempty:""`
	DesignerEngId     *uint32 `canempty:""`
	BuyerEngId        *uint32 `canempty:""`
	SQEEngId          *uint32 `canempty:""`
	TestEngId         *uint32 `canempty:""`
	CrefftauEngId     *uint32 `canempty:""`
}

// @Summary 零件列表
// @Description 只显示有任意权限的零部件 Start`min:"0"` <br>Length`min:"1" max:"500"`;任何人可以查询(无需权限)
// @Param _ body controllers.PartCT.inputQueryPartList true
// @Success 200 {object} models.PartMD.PartList
// @router /QueryPartList/ [post]
func (this *PartController) QueryPartList() {
	in := inputQueryPartList{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !userMD.CheckUserToken(*in.Token) {
		this.BackInfo.Message = "令牌错误"
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable(&sqlmodel.Part{})

	//if in.Secrch != nil && *in.Secrch != "" {
	//	cond := orm.NewCondition()
	//	cond = cond.Or("name__icontains", *in.Secrch).Or("part_code__icontains", *in.Secrch).Or("DesignerEng__Name__icontains", *in.Secrch).Or("TestEng__Name__icontains", *in.Secrch)
	//	cond = cond.Or("BuyerEng__Name__icontains", *in.Secrch).Or("SQEEng__Name__icontains", *in.Secrch).Or("CrefftauEng__Name__icontains", *in.Secrch)
	//	qs = qs.SetCond(orm.NewCondition().AndCond(cond)) //所有cond需要包含And,否则会影响其他条件
	//}

	if in.Secrch != nil && *in.Secrch != "" {
		cond := orm.NewCondition()
		cond = cond.Or("name__icontains", *in.Secrch).Or("part_code__icontains", *in.Secrch)
		qs = qs.SetCond(orm.NewCondition().AndCond(cond)) //所有cond需要包含And,否则会影响其他条件
	}
	if in.PartCode != nil {
		qs = qs.Filter("PartCode__icontains", *in.PartCode)
	}
	if in.Name != nil {
		qs = qs.Filter("Name__icontains", *in.Name)
	}
	if in.ProfessionId != nil {
		qs = qs.Filter("Profession", *in.ProfessionId)
	}
	if in.DevelopmentTypeId != nil {
		qs = qs.Filter("DevelopmentType", *in.DevelopmentTypeId)
	}
	if in.DesignerEngId != nil {
		qs = qs.Filter("DesignerEng", *in.DesignerEngId)
	}
	if in.BuyerEngId != nil {
		qs = qs.Filter("BuyerEng", *in.BuyerEngId)
	}
	if in.SQEEngId != nil {
		qs = qs.Filter("SQEEng", *in.SQEEngId)
	}
	if in.TestEngId != nil {
		qs = qs.Filter("TestEng", *in.TestEngId)
	}
	if in.CrefftauEngId != nil {
		qs = qs.Filter("CrefftauEng", *in.CrefftauEngId)
	}
	qs = qs.Filter("Project", *in.ProjectId)
	qs = qs.Filter("Deled", 0)
	//qs = qs.SetCond(cond)
	dinfo := PartMD.PartList{}
	var err error
	if dinfo.RecordsTotal, err = qs.Count(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	elems := []sqlmodel.Part{}
	if _, err = qs.Limit(*in.Length, *in.Start).OrderBy("-Id").RelatedSel("Profession", "QualityLevel", "DevelopmentType", "DesignerEng", "BuyerEng", "SQEEng", "CrefftauEng", "TestEng").All(&elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	for _, e := range elems {
		elem := PartMD.PartElem{}
		elem.Id = e.Id
		elem.PartCode = e.PartCode
		elem.Name = e.Name
		if e.QualityLevel != nil {
			elem.QualityLevel = e.QualityLevel.Name
		}
		if e.DevelopmentType != nil {
			elem.DevelopmentType = e.DevelopmentType.Name
		}
		elem.Profession = e.Profession.Name
		elem.DesignerEng = e.DesignerEng.Name
		elem.BuyerEng = e.BuyerEng.Name
		elem.SQEEng = e.SQEEng.Name
		elem.CrefftauEng = e.CrefftauEng.Name
		elem.TestEng = e.TestEng.Name
		dinfo.Elems = append(dinfo.Elems, elem)
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
