package partCT

import (
	"Maturity/models/userMD"
)
type inputQueryAllPartField struct {
	Token *string `maxlen:"22"`

	Statistics *bool   `canempty:""`
	Table      *string `canempty:""`
}

// @Summary 查询所有零件的字段
// @Description
// @Param _ body controllers.partCT.inputQueryAllPartField true
// @Success 200 {object} []controllers.partCT.FieldInfo
// @router /QueryAllPartField/ [post]
func (this *PartController) QueryAllPartField() {
	in := inputQueryAllPartField{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !userMD.CheckUserToken(*in.Token) {
		this.BackInfo.Message = "令牌错误"
		return
	}
	this.BackInfo.Status = true
	if in.Statistics != nil && *in.Statistics {
		this.BackInfo.Data = StatisticsFieldList
	} else {
		this.BackInfo.Data = PartFieldList
	}
	if in.Table != nil {
		var tmp []FieldInfo
		for _, v := range StatisticsFieldList {
			if v.Table == *in.Table {
				tmp = append(tmp, v)
			}
		}
		this.BackInfo.Data = tmp
	}
}
