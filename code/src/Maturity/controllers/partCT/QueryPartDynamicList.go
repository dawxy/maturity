package partCT

import (
	"Maturity/controllers"
	"Maturity/models/PartMD"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
	"reflect"
	"strconv"
	"strings"
)

type InputWhere struct {
	Field string
	Val   interface{}
}
type inputQueryPartDynamicList struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"100000"`

	ProjectId    *uint32
	SelectFields []string     //需要返回哪些字段
	Wheres       []InputWhere //每个字段对应的筛选条件(字段必须是SelectFields出现的)
}

// @Summary 零件列表(可选任意字段信息)
// @Description 只显示有任意权限的零部件 Start`min:"0"` <br>Length`min:"1" max:"500"`;任何人可以查询(无需权限)
// @Param _ body controllers.PartCT.inputQueryPartDynamicList true
// @Success 200 {object} models.PartMD.QueryPartDynamicList
// @router /QueryPartDynamicList/ [post]
func (this *PartController) QueryPartDynamicList() {
	in := inputQueryPartDynamicList{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}
	if !userMD.CheckUserToken(*in.Token) {
		this.BackInfo.Message = "令牌错误"
		return
	}
	if len(in.SelectFields) < 1 {
		this.BackInfo.Message = "查询字段不能为空!"
		return
	}
	//join语句
	join, err := orm.NewQueryBuilder("mysql")
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	//需要查询那些表的哪些字段
	var queryTables = make(map[string]bool) //表格是否标记过
	queryTables["part"] = true
	var tbNum int64                               //记录外键表数量(防止part多个user表SelectFields同名)
	var SelectAllFileds = make(map[string]string) //原SelectFields对应的返回表.字段
	for i, v := range in.SelectFields {
		fi, ok := PartFieldName[v]
		if !ok {
			this.BackInfo.Message = "不存在的字段:" + v
			return
		}
		f := strings.Split(fi.Field, "__")
		tb := controllers.ToLower(f[0])
		field := controllers.ToLower(f[1])
		//log.Println(tb, field)
		if !queryTables[tb] { //没有标记过的表格,添加leftjoin到part表
			queryTables[tb] = true
			join.LeftJoin(tb).On(tb + ".part_id=`part`.id")
		}
		if fi.IsPtr {
			//找到对应的model表名
			//log.Println(reflect.ValueOf(fieldModel[fi.Field]).FieldByName(f[1]).Type().Elem().Name())
			mtb := controllers.ToLower(reflect.ValueOf(fieldModel[fi.Field]).FieldByName(f[1]).Type().Elem().Name())
			tnum := strconv.FormatInt(tbNum, 10)
			join.LeftJoin(mtb + " T" + tnum).On("`" + tb + "`." + field + "_id=" + "T" + tnum + ".id")
			in.SelectFields[i] = "T" + tnum + ".name as '" + fi.Field + "'"
			tbNum++
			SelectAllFileds[v] = "`" + tb + "`." + field + "_id"
		} else {
			in.SelectFields[i] = "`" + tb + "`." + field + " as '" + fi.Field + "'"
			SelectAllFileds[v] = "`" + tb + "`." + field
		}
	}
	//解析where条件
	where, err := orm.NewQueryBuilder("mysql")
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	extArgs := []interface{}{*in.ProjectId}
	sqlwhere := "`part`.project_id = ? AND `part`.deled = false"

	for _, v := range in.Wheres {
		field := v.Field
		val := v.Val
		fi, ok := SelectAllFileds[field]
		if !ok {
			this.BackInfo.Message = field + ":没有选择的返回字段"
			return
		}
		info := PartFieldName[field]
		switch info.Kind {
		case reflect.Int8, reflect.Ptr, reflect.Int32:
			sqlwhere += " AND " + fi + "=?"
			extArgs = append(extArgs, val)
		case reflect.String:
			sqlwhere += " AND " + fi + " like ?"
			extArgs = append(extArgs, "%"+val.(string)+"%")
		default:
			this.BackInfo.Message = field + ":非法条件"
			return
		}
	}
	where.Where(sqlwhere)

	//SQL语句
	selects, err := orm.NewQueryBuilder("mysql")
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	from, err := orm.NewQueryBuilder("mysql")
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}

	limit, err := orm.NewQueryBuilder("mysql")
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	selects.Select(in.SelectFields...)
	from.From("`part`")

	dinfo := PartMD.QueryPartDynamicList{}
	o := orm.NewOrm()
	//获取总的数量
	//log.Println("select count(*) "+from.String()+" "+join.String()+" "+where.String())
	if err = o.Raw("select count(*) "+from.String()+" "+join.String()+" "+where.String(), extArgs...).QueryRow(&dinfo.RecordsTotal); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	limit.OrderBy("part.id").Desc().Limit(int(*in.Length)).Offset(int(*in.Start))
	//log.Println(selects.String()+" "+from.String()+" "+join.String()+" "+where.String()+" "+limit.String())
	if _, err := o.Raw(selects.String()+" "+from.String()+" "+join.String()+" "+where.String()+" "+limit.String(), extArgs...).Values(&dinfo.Elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	//转换所有int8类型为中文
	for _, mp := range dinfo.Elems {
		for k, v := range mp {
			if PartFieldName[k].IsInt8 {
				if v != nil {
					tv, _ := strconv.ParseInt(v.(string), 10, 8)
					switch tv {
					case 0:
						mp[k] = "<span style=\"color: red\">否</span>"
					case 1:
						mp[k] = "<span style=\"color: green\">是</span>"
					default:
						mp[k] = int8Name[int8(tv)]
					}
				} else { //默认null字段的int8值都是'否'
					mp[k] = "<span style=\"color: red\">否</span>"
				}
			}
		}
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
