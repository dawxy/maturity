package partCT

import (
	"Maturity/models/PartMD"
	"strings"
)

type inputCheckUpFieldPower struct {
	Token   *string `maxlen:"22"`
	PartId  *uint32
	ApiName *string
}

// @Summary 检查某个用户对某个零件拥有某个注册过的修改接口对应的所有可以修改字段
// @Description
// @Param _ body controllers.PartCT.inputCheckUpFieldPower true
// @Success 200 {object} models.PartMD.CheckUpFieldPower
// @router /CheckUpFieldPower/ [post]
func (this *PartController) CheckUpFieldPower() {
	in := inputCheckUpFieldPower{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	m, ok := regUpApi[*in.ApiName]
	if !ok {
		this.BackInfo.Message = "不存在的API!"
		return
	}
	//获取可修改的字段
	dinfo := PartMD.CheckUpFieldPower{}
	var notAllowFields []string
	if !this.CheckUpdatePower(*in.Token, m, *in.PartId, &dinfo.Fields, &notAllowFields) {
		return
	}
	for _, v := range notAllowFields {
		str := strings.Split(v, "$$")
		dinfo.NotAllowFields = append(dinfo.NotAllowFields, PartMD.NotAllowFields{Field: str[0], UpEng: str[1]})
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
