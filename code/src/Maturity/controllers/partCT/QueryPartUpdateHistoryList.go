package partCT

import (
	"Maturity/models/PartMD"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
	"strconv"
	"strings"
	"time"
)

type inputQueryPartUpdateHistoryList struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`

	ProjectId       *uint32
	PartId          *uint32  `canempty:""`
	Type            *uint8   `canempty:""`
	Table           *string  `canempty:""`
	Field           []string `canempty:""`
	StartTime       *string  `canempty:""`
	EndTime         *string  `canempty:""`
	UpUserJobNumber *string  `canempty:""`
}

// @Summary 零件修改历史记录
// @Description ProjectId必填; PartId，Table，Field，StartTime，EndTime选填,StartTime，EndTime格式"2006-01-02 15:04:05"
// @Param _ body controllers.partCT.inputQueryPartUpdateHistoryList true
// @Success 200 {object} models.partMD.QueryPartUpdateHistoryList
// @router /QueryPartUpdateHistoryList/ [post]
func (this *PartController) QueryPartUpdateHistoryList() {
	in := inputQueryPartUpdateHistoryList{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !userMD.CheckUserToken(*in.Token) {
		this.BackInfo.Message = "令牌错误"
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable(&sqlmodel.UpdateHistory{}).Filter("ProjectId", *in.ProjectId)
	if in.PartId != nil {
		qs = qs.Filter("PartId", *in.PartId)
	}
	if in.Table != nil {
		qs = qs.Filter("Table", *in.Table)
	}
	if in.Type != nil {
		qs = qs.Filter("Type", *in.Type)
	}
	if in.UpUserJobNumber != nil {
		qs = qs.Filter("UpJobNumber", *in.UpUserJobNumber)
	}
	var tables []interface{}
	var fields []interface{}
	for _, fi := range in.Field {
		f := strings.Split(fi, "__")
		tb := f[0]
		field := f[1]
		tables = append(tables, tb)
		fields = append(fields, field)
	}
	if len(fields) > 0 {
		qs = qs.Filter("Field__in", fields...)
		qs = qs.Filter("Table__in", tables...)
	}
	if in.StartTime != nil && *in.StartTime != "" {
		t, err := time.ParseInLocation("2006-01-02 15:04:05", *in.StartTime, time.Local)
		if err != nil { //v.String() != "",允许设置为空时间
			this.BackInfo.Message = *in.StartTime + "不是正确的时间格式!"
			return
		}
		qs = qs.Filter("UpTime__gte", t)
	}
	if in.EndTime != nil && *in.EndTime != "" {
		t, err := time.ParseInLocation("2006-01-02 15:04:05", *in.EndTime, time.Local)
		if err != nil { //v.String() != "",允许设置为空时间
			this.BackInfo.Message = *in.EndTime + "不是正确的时间格式!"
			return
		}
		qs = qs.Filter("UpTime__lte", t)
	}
	dinfo := PartMD.QueryPartUpdateHistoryList{}
	var err error
	if dinfo.RecordsTotal, err = qs.Count(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	elems := []sqlmodel.UpdateHistory{}
	if _, err = qs.Limit(*in.Length, *in.Start).OrderBy("-Id").All(&elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	for _, e := range elems {
		elem := PartMD.QueryPartUpdateHistoryElem{}
		elem.Id = e.Id
		elem.ProjectId = e.ProjectId
		elem.ProjectName = e.ProjectName
		elem.PartId = e.PartId
		elem.PartName = e.PartName
		elem.Type = e.Type
		elem.OldValue = e.OldValue
		elem.Value = e.Value
		elem.UpJobNumber = e.UpJobNumber
		elem.UpUser = e.UpUser
		elem.Ip = e.Ip
		k := e.Table + "__" + e.Field
		fieldInfo := PartFieldName[k]
		if fieldInfo != nil {
			elem.TableName = fieldInfo.TableName
			elem.FieldName = fieldInfo.FieldName
			if fieldInfo.IsInt8 { //过滤int8值转换成中文
				oldtv, _ := strconv.ParseInt(e.OldValue, 10, 8)
				elem.OldValue = int8Name[int8(oldtv)]
				tv, _ := strconv.ParseInt(e.Value, 10, 8)
				elem.Value = int8Name[int8(tv)]
			}
		}
		elem.UpTime = e.UpTime.Format("2006-01-02 15:04:05")

		dinfo.Elems = append(dinfo.Elems, elem)
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
