package partCT

import (
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputBatchUpdateParts struct {
	Token   *string  `maxlen:"22" ignoreUpCheck:""`
	PartIds []uint32 `ignoreUpCheck:""`

	DesignerEngId *uint32 `canempty:"" UpEng:"SystemEng" MultipleP:"DesignerEng"`
	BuyerEngId    *uint32 `canempty:"" UpEng:"SystemEng" MultipleP:"BuyerEng"`
	SQEEngId      *uint32 `canempty:"" UpEng:"SystemEng" MultipleP:"SQEEng"`
	CrefftauEngId *uint32 `canempty:"" UpEng:"SystemEng" MultipleP:"CrefftauEng"`
	TestEngId     *uint32 `canempty:"" UpEng:"SystemEng" MultipleP:"TestEng"`
}

// @Summary 修改零件基础信息
// @Description 除了Token和PartId必填，其他不填不修改
// @Param _ body controllers.PartCT.inputBatchUpdateParts true
// @Success 200 {object} string
// @router /BatchUpdateParts/ [post]
func (this *PartController) BatchUpdateParts() {
	in := inputBatchUpdateParts{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	//检查每个字段的权限
	if len(in.PartIds) > 200 {
		this.BackInfo.Message = "单次批量修改零件数不能超过200！"
		return
	}
	for _, partId := range in.PartIds {
		if !this.CheckUpdatePower(*in.Token, &in, partId) {
			return
		}
	}

	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	for _, partId := range in.PartIds {
		m := sqlmodel.Part{Id: partId}
		if err := o.Read(&m, "Id"); err != nil {
			if err == orm.ErrNoRows {
				this.BackInfo.Message = "不存在的零件!"
				return
			}
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦!"
			return
		}
		if !this.UpdateModel(o, m.Project.Id, partId, &m, &in, *in.Token) {
			return
		}
	}

	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功!"
}
