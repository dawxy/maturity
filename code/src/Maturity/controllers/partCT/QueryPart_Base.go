package partCT

import (
	"Maturity/models/PartMD"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
)

type inputQueryPart_Import struct {
	Token  *string `maxlen:"22"`
	PartId *uint32
}

// @Summary 查询零件基础信息
// @Description 任何人都可以查询
// @Param _ body controllers.PartCT.inputQueryPart_Import true
// @Success 200 {object} models.PartMD.QueryPart_Import
// @router /QueryPart_Base/ [post]
func (this *PartController) QueryPart_Base() {
	in := inputQueryPart_Import{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !userMD.CheckUserToken(*in.Token) {
		this.BackInfo.Message = "令牌错误"
		return
	}
	o := orm.NewOrm()
	m := sqlmodel.Part{Id: *in.PartId}
	if err := o.QueryTable(&m).Filter("Id", *in.PartId).RelatedSel().One(&m); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的零件"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}

	dinfo := PartMD.QueryPart_Import{
		Name:                     m.Name,
		PartCode:                 m.PartCode,
		ArrowSystem:              m.ArrowSystem.Name,
		ArrowSystemId:            m.ArrowSystem.Id,
		SingleUseNum:             m.SingleUseNum,
		DesignerEngId:            m.DesignerEng.Id,
		DesignerEng:              m.DesignerEng.Name,
		DesignerEngJobNumber:     m.DesignerEng.JobNumber,
		BuyerEngId:               m.BuyerEng.Id,
		BuyerEng:                 m.BuyerEng.Name,
		BuyerEngJobNumber:        m.BuyerEng.JobNumber,
		SQEEngId:                 m.SQEEng.Id,
		SQEEng:                   m.SQEEng.Name,
		SQEEngJobNumber:          m.SQEEng.JobNumber,
		CrefftauEngId:            m.CrefftauEng.Id,
		CrefftauEng:              m.CrefftauEng.Name,
		CrefftauEngJobNumber:     m.CrefftauEng.JobNumber,
		TestEngId:                m.TestEng.Id,
		TestEng:                  m.TestEng.Name,
		TestEngJobNumber:         m.TestEng.JobNumber,
		CreatTime:                m.CreatTime.Format("2006-01-02 15:04:05"),
		ProfessionId:             m.Profession.Id,
		Profession:               m.Profession.Name,
		SupplierName:             m.SupplierName,
		SupplierCode:             m.SupplierCode,
		SupplierBusnes:           m.SupplierBusnes,
		SupplierTec:              m.SupplierTec,
		Deled:                    m.Deled,
		Configuration:            m.Configuration,
		PartSource:               m.PartSource,
		SupplierPhone:            m.SupplierPhone,
		ChangePlanCompletionTime: m.ChangePlanCompletionTime.Format("2006-01-02"),
		ChangeCompletionTime:     m.ChangeCompletionTime.Format("2006-01-02"),
		Workshop:                 m.Workshop,
		Section:                  m.Section,
		Station:                  m.Station,
		TargetWeight:             m.TargetWeight,
		EstimatedWeight:          m.EstimatedWeight,
		ActualWeight:             m.ActualWeight,
	}
	if m.SizeEng != nil {
		dinfo.SizeEngId = m.SizeEng.Id
		dinfo.SizeEng = m.SizeEng.Name
		dinfo.SizeEngJobNumber = m.SizeEng.JobNumber
	}
	if m.NewqualityEng != nil {
		dinfo.NewqualityEngId = m.NewqualityEng.Id
		dinfo.NewqualityEng = m.NewqualityEng.Name
		dinfo.NewqualityEngJobNumber = m.NewqualityEng.JobNumber
	}
	if m.QualityLevel != nil {
		dinfo.QualityLevelId = m.QualityLevel.Id
		dinfo.QualityLevel = m.QualityLevel.Name
	}
	if m.DevelopmentType != nil {
		dinfo.DevelopmentTypeId = m.DevelopmentType.Id
		dinfo.DevelopmentType = m.DevelopmentType.Name
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
