package partCT

import (
	"Maturity/controllers"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
	"reflect"
	"strings"
	"time"
	"encoding/json"
)

//零件相关
type PartController struct {
	controllers.BaseController
}

//==================信息初始化
var (
	nameByCam           = make(map[string]reflect.Type) //注册model用于QueryPart_Cam和AddPart
	regUpApi            = make(map[string]interface{})
	PartFieldList       []FieldInfo
	StatisticsFieldList []FieldInfo                    //需要统计的字段
	PartFieldName       = make(map[string]*FieldInfo)  //零件每个字段对应的字段中文名和字段所在阶段(表格)的中文名
	int8Name            = make(map[int8]string)        //每个int8值对应的中文描述(只是返回列表查询时用到)
	fieldModel          = make(map[string]interface{}) //字段全名对应的model
	CamsList            []CamInfo                      //每个阶段的字段信息
)

func init() {
	int8Name[0] = "否"
	int8Name[1] = "是"
	int8Name[-1] = "/"
	int8Name[2] = "未通过"
	int8Name[3] = "带条件通过"
	int8Name[4] = "通过"
	int8Name[5] = "临时"
	int8Name[6] = "正式"
	//注册修改权限和修改cam信息的结构,用于CheckUpFieldPower和UpdatePart_Cam
	regUpApi["UpdatePart_Part"] = &inputUpdatePart_Import{}
	regUpApi["UpdatePart_Cam0_1"] = &inputUpdatePart_Cam0_1{}
	regUpApi["UpdatePart_Cam1_2"] = &inputUpdatePart_Cam1_2{}
	regUpApi["UpdatePart_Cam2_3"] = &inputUpdatePart_Cam2_3{}
	regUpApi["UpdatePart_Cam3_4"] = &inputUpdatePart_Cam3_4{}
	regUpApi["UpdatePart_Cam4_5"] = &inputUpdatePart_Cam4_5{}
	regUpApi["UpdatePart_Cam5_8"] = &inputUpdatePart_Cam5_8{}

	//初始化每个字段信息和各个int8值对应的中文等，并注册model用于QueryPart_Cam和AddPart
	regPartModel(sqlmodel.Part{}, "零件基础信息")
	regPartModel(sqlmodel.Cam0_1{}, "P0-P1")
	regPartModel(sqlmodel.Cam1_2{}, "P1-P2")
	regPartModel(sqlmodel.Cam2_3{}, "P2-P3")
	regPartModel(sqlmodel.Cam3_4{}, "P3-P4")
	regPartModel(sqlmodel.Cam4_5{}, "P4-P5")
	regPartModel(sqlmodel.Cam5_8{}, "P5-P8")
}

type CamInfo struct {
	CamId   string    //每个阶段的表名
	CamName string    //阶段中文名
	Tag     []TagInfo //左侧二级分类
}
type TagInfo struct { //每个分类下所有字段的信息
	TagName string
	Fields  *[]FieldInfo
}
type Show struct{
	Field string
	Val string
}
type FieldInfo struct {
	Table     string
	TableName string
	Field     string
	FieldName string

	FieldType string //字段类型int8,string等
	Int8List  []int8 //int8类型对应的可选范围
	Int8ListName []string
	Show Show //当Field=Val时才显示为可修改

	//Filter    Filter       //筛选信息
	IsPtr  bool         `json:"-"` //是否为指针(也就是外键)
	IsInt8 bool         `json:"-"` //
	Kind   reflect.Kind `json:"-"` //
}

func GetInt8Name(v int8) string {
	return int8Name[v]
}
func regPartModel(m interface{}, TableName string) {
	table := reflect.TypeOf(m).Name()
	names := reflect.TypeOf(m)
	values := reflect.ValueOf(m)
	nameByCam[table] = reflect.TypeOf(m)
	canInfo := CamInfo{CamId: table, CamName: TableName}
	tmpTag := make(map[string]*[]FieldInfo)
	for i := 0; i < values.NumField(); i++ {
		k := names.Field(i)
		//v := values.Field(i)
		tag := k.Tag
		desc := tag.Get("desc")
		if desc != "" {
			name := table + "__" + k.Name
			if _, ok := PartFieldName[name]; ok {
				panic("存在重复的字段:" + name)
			}

			//println(name, desc)
			tmp := FieldInfo{Field: name, FieldName: desc, Table: table, TableName: TableName, Kind: k.Type.Kind()}
			tmp.FieldType = k.Type.String()
			tmp.IsPtr = strings.Index(tag.Get("orm"), "rel(fk)") >= 0
			tmp.IsInt8 = k.Type.Kind() == reflect.Int8
			if tmp.IsInt8{
				Int8List,ok := tag.Lookup("Int8List")
				if !ok{//默认为0-1值
					tmp.Int8List = append(tmp.Int8List, 0,1)
				}else{
					if err := json.Unmarshal([]byte(Int8List), &tmp.Int8List);err != nil{
						panic(err)
					}
				}
				for _,v := range tmp.Int8List{
					tmp.Int8ListName = append(tmp.Int8ListName, int8Name[v])
				}
			}
			Show,ok := tag.Lookup("Show")
			if ok{
				str := strings.Split(Show, "=")
				if len(str) != 2{
					panic(Show+",格式错误")
				}
				if _,ok := names.FieldByName(str[0]);!ok{
					panic(Show+",不存在的字段"+str[0])
				}
				tmp.Show.Field = str[0]
				tmp.Show.Val = str[1]
			}
			//把字段放入所在Tag
			strs := strings.Split(desc, "_")
			if len(strs) != 2 {
				panic("Cam描述信息格式错误:" + desc)
			}
			list, ok := tmpTag[strs[0]]
			if !ok {
				list = new([]FieldInfo)
				canInfo.Tag = append(canInfo.Tag, TagInfo{TagName: strs[0], Fields: list})
				tmpTag[strs[0]] = list
			}
			*list = append(*list, tmp)
			PartFieldList = append(PartFieldList, tmp)
			PartFieldName[name] = &tmp
			fieldModel[name] = m
			if _, ok := tag.Lookup("needStatistics"); ok {
				StatisticsFieldList = append(StatisticsFieldList, tmp)
			}
		}
	}
	CamsList = append(CamsList, canInfo)
}

//信息初始化================================END

//检查用户是否这个专业的系统集成工程师
func (this *PartController) CheckSystemEngProfession(Token string, ProjectId, ProfessionId uint32) (ok bool) {
	u, err := userMD.GetUser(Token)
	if err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	this.SetUserInfo(Token, u)
	if u == nil {
		this.BackInfo.Message = "令牌错误"
		return
	}
	if !orm.NewOrm().QueryTable(&sqlmodel.SystemEng{}).Filter("Project", ProjectId).Filter("Profession", ProfessionId).Filter("User", u).Exist() {
		this.BackInfo.Message = "您没有权限!"
		return
	}
	ok = true
	return
}

//查询该用户为系统集成工程师的所有专业
//func (this *PartController) AllSystemEngProfession(Token string) (ret []*sqlmodel.Profession) {
//	u, err := userMD.GetUser(Token)
//	if err != nil {
//		this.ErrLog(err)
//		this.BackInfo.Message = "服务器出错啦~"
//		return
//	}
//	if u == nil {
//		this.BackInfo.Message = "令牌错误"
//		return
//	}
//
//	SystemEngs := []sqlmodel.SystemEng{}
//	if _, err = orm.NewOrm().QueryTable("SystemEng").Filter("User", u).All(&SystemEngs); err != nil {
//		this.ErrLog(err)
//		this.BackInfo.Message = "服务器出错啦~"
//		return
//	}
//	for _, v := range SystemEngs {
//		ret = append(ret, v.Profession)
//	}
//	return
//}

func checkFieldPower(u *sqlmodel.User, UpEng string, cache map[string]bool, part *sqlmodel.Part) (ret bool) {
	if ret, ok := cache[UpEng]; ok {
		return ret
	}
	defer func() {
		cache[UpEng] = ret
	}()
	o := orm.NewOrm()
	if UpEng == "SystemEng" { //系统集成工程师特殊验证
		return o.QueryTable(&sqlmodel.SystemEng{}).Filter("Project", part.Project.Id).Filter("Profession", part.Profession.Id).Filter("User", u).Exist()
	} else if UpEng == "TecEng" { //验证技术集成工程师
		return o.QueryTable(&sqlmodel.TecEng{}).Filter("Project", part.Project).Filter("User", u).Exist()
	}
	ue := reflect.ValueOf(part).Elem().FieldByName(UpEng)
	if ue.IsNil(){ //工程师为nil
		return false
	}
	return u.Id == ue.Interface().(*sqlmodel.User).Id
}

//检查用户是否有对应字段的修改权限(UpEng标签来标记的权限字段)
func (this *PartController) CheckUpdatePower(Token string, in interface{}, partid uint32, allPowers ...*[]string) (ret bool) {
	u := this.GetUserInfo(Token)
	if u == nil {
		var err error
		u, err = userMD.GetUser(Token)
		if err != nil {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
		if u == nil {
			this.BackInfo.Message = "令牌错误"
			return
		}
		this.SetUserInfo(Token, u)
	}

	o := orm.NewOrm()
	//检查是否存在这个零件
	part := sqlmodel.Part{Id: partid, Deled: false}
	if err := o.Read(&part, "Id", "Deled"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的零件!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	getPowers := false
	var allPower *[]string //有权限的所有字段
	if len(allPowers) > 0 {
		getPowers = true
		allPower = allPowers[0]
	}
	var shouldPower *[]string //没有权限的字段所需要的权限名
	if len(allPowers) > 1 {
		shouldPower = allPowers[1]
	}
	cache := make(map[string]bool)
	names := reflect.TypeOf(in).Elem()
	values := reflect.ValueOf(in).Elem()
	for i := 0; i < values.NumField(); i++ {
		k := names.Field(i)
		v := values.Field(i)
		tag := k.Tag
		//是否需要验证
		_, ignoreUpCheck := tag.Lookup("ignoreUpCheck")
		if ignoreUpCheck {
			continue
		}
		if v.Kind() != reflect.Ptr {
			panic(names.Name() + "." + k.Name + ":必须为指针类型")
		}
		if !getPowers && v.IsNil() { //不输入则不会修改
			continue
		}
		UpEng := tag.Get("UpEng")
		if UpEng == "" {
			panic("请选择需要授权的工程师类型:" + k.Name)
		}
		hasPower := false //是否有权限
		//如果有MultipleP(目前只有协同开发小组字段有)
		MultipleP := tag.Get("MultipleP")
		if MultipleP != "" {
			//检查是否为对应工程师的协同开发管理员
			if o.QueryTable(&sqlmodel.CdtEng{}).Filter("User", u).Filter("EngName", MultipleP).Filter("Project", part.Project.Id).Exist() {
				hasPower = true
			}
		}
		if !hasPower && checkFieldPower(u, UpEng, cache, &part) {
			hasPower = true
		}

		if !hasPower {
			if getPowers {
				if shouldPower != nil {
					*shouldPower = append(*shouldPower, k.Name+"$$"+UpEng)
				}
				continue
			}
			this.BackInfo.Message = "您没有修改:" + k.Name + "的权限!"
			return
		} else {
			if getPowers {
				*allPower = append(*allPower, k.Name)
			}
		}

	}
	ret = true
	return
}

//把数据库model写入到返回查询
func (this *PartController) SetModelBackData(m interface{}, dinfo orm.Params) {
	names := reflect.TypeOf(m).Elem()
	values := reflect.ValueOf(m).Elem()
	for i := 0; i < values.NumField(); i++ {
		k := names.Field(i)
		v := values.Field(i)
		if v.Kind() != reflect.Ptr { //不是指针直接设置值
			t, ok := v.Interface().(time.Time)
			if ok { //是时间类型的话格式化成字符串
				if t.IsZero() {
					continue
				}
				dinfo[k.Name] = t.Format("2006-01-02")
			} else {
				dinfo[k.Name] = v.Interface()
			}
		} else {
			if v.IsNil() { //指针为空
				dinfo[k.Name+"Id"] = nil
				dinfo[k.Name] = ""
				continue
			}
			dinfo[k.Name+"Id"] = v.Elem().FieldByName("Id").Interface()
			dinfo[k.Name] = v.Elem().FieldByName("Name").Interface()
		}
	}
}

//检查零件号格式
func (this *PartController) CheckPartCode(code string) bool {
	str := strings.Split(code, "-")
	if len(str) < 2 || len(str[0]) == 0 || len(str[1]) < 7 {
		return false
	}
	return true
}
