package PowerCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
	"time"
)

type inputAddUserRole struct {
	Token  *string `maxlen:"22"`
	UserId *uint32
	RoleId *uint32
}

// @Summary 给用户添加角色
// @Description
// @Param _ body controllers.PowerCT.inputAddUserRole true
// @Success 200 {object} string
// @router /AddUserRole/ [post]
func (this *PowerController) AddUserRole() {
	in := inputAddUserRole{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.AddUserRole) {
		return
	}
	if CheckPower.AdminUser.Id == *in.UserId {
		this.BackInfo.Message = CheckPower.AdminUser.Name + "账号不能修改权限!"
		return
	}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	m := sqlmodel.UserRole{}
	u := &sqlmodel.User{Id: *in.UserId}
	role := &sqlmodel.Role{Id: *in.RoleId}
	m.User = u
	m.Role = role
	if o.QueryTable(&m).Filter("User", m.User).Filter("Role", m.Role).Exist() {
		this.BackInfo.Message = "该用户已经存在这个角色!"
		return
	}
	if err := o.Read(u, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的用户!"
			return
		}
	}
	if err := o.Read(role, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "不存在的权限角色!"
			return
		}
	}

	upu := this.GetUserInfo(*in.Token)
	m.CreatUser = upu
	if _, err := o.Insert(&m); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	//记录日志
	sysl := sqlmodel.SysLogs{Type: "用户添加权限", Value: role.Name, TargetUserJobNum: u.JobNumber, TargetUserJobName: u.Name, UserName: upu.Name, UpJobNumber: upu.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
	if _, err := o.Insert(&sysl); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
