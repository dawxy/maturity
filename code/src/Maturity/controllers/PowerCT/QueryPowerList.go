package PowerCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/PowerMD"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

type inputQueryPowerList struct {
	Token  *string `maxlen:"22"`
	Start  *int64  `min:"0"`
	Length *int64  `min:"1" max:"500"`

	Secrch *string `canempty:""`
}

// @Summary 用户权限列表
// @Description
// @Param _ body controllers.PowerCT.inputQueryPowerList true
// @Success 200 {object} models.PowerMD.QueryPowerList
// @router /QueryPowerList/ [post]
func (this *PowerController) QueryPowerList() {
	in := inputQueryPowerList{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.QueryPowerList) {
		return
	}
	o := orm.NewOrm()
	qs := o.QueryTable("UserRole")
	if in.Secrch != nil && *in.Secrch != "" {
		cond := orm.NewCondition()
		cond = cond.Or("User__name__icontains", *in.Secrch).Or("Role__name__icontains", *in.Secrch).Or("User__JobNumber__icontains", *in.Secrch)
		qs = qs.SetCond(orm.NewCondition().AndCond(cond))
	}

	dinfo := PowerMD.QueryPowerList{}
	var err error
	if dinfo.RecordsTotal, err = qs.Count(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	elems := []sqlmodel.UserRole{}
	if _, err = qs.Limit(*in.Length, *in.Start).OrderBy("-Id").RelatedSel().All(&elems); err != nil {
		if err != orm.ErrNoRows {
			this.ErrLog(err)
			this.BackInfo.Message = "服务器出错啦~"
			return
		}
	}
	for _, e := range elems {
		elem := PowerMD.QueryPowerElem{}
		elem.Id = e.Id
		elem.UserId = e.User.Id
		elem.JobNumber = e.User.JobNumber
		elem.UserName = e.User.Name
		elem.RoleId = e.Role.Id
		elem.RoleName = e.Role.Name
		if e.CreatUser != nil {
			elem.CreatUser = e.CreatUser.Name
		}
		elem.CreatTime = e.CreatTime.Format("2006-01-02 15:04:05")
		dinfo.Elems = append(dinfo.Elems, elem)
	}
	this.BackInfo.Status = true
	this.BackInfo.Data = dinfo
}
