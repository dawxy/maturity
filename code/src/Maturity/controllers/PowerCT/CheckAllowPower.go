package PowerCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
)

type inputCheckAllowPower struct {
	Token *string `maxlen:"22"`

	ProjectId *uint32 `canempty:""` //填写则查询某个项目下的所有工程师等角色(暂时只有管理员，技术集成和系统经理)
}

// @Summary 获取用户拥有的所有权限列表
// @Description
// @Param _ body controllers.PowerCT.inputCheckAllowPower true
// @Success 200 {object} []string
// @router /CheckAllowPower/ [post]
func (this *PowerController) CheckAllowPower() {
	in := inputCheckAllowPower{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	u, err := userMD.GetUser(*in.Token)
	if err != nil {
		this.SetBackInfo(false, "服务器出错啦!", nil, err)
		return
	}
	if u == nil {
		this.SetBackInfo(false, "您没有权限!", nil)
		return
	}
	o := orm.NewOrm()
	var dinfo []string
	//检查是否为系统管理员
	isSysAdmin := o.QueryTable(&sqlmodel.UserRole{}).Filter("User", u).Filter("Role", CheckPower.SystemAdmin).Exist()
	if in.ProjectId == nil {
		o.LoadRelated(u, "UserRoles")
		for _, v := range u.UserRoles {
			o.LoadRelated(v.Role, "RolePowers")
			for _, p := range v.Role.RolePowers {
				if _, err := o.LoadRelated(p, "Power"); err != nil {
					this.ErrLog(err)
					this.SetBackInfo(false, "服务器出错啦!", nil)
					return
				}
				dinfo = append(dinfo, p.Power.Name)
			}
		}
	} else {
		if isSysAdmin || o.QueryTable(&sqlmodel.Project{}).Filter("Id", *in.ProjectId).Filter("Admin", u.Id).Exist() {
			dinfo = append(dinfo, "Admin")
		}
		if isSysAdmin || o.QueryTable(&sqlmodel.TecEng{}).Filter("Project", *in.ProjectId).Filter("User", u.Id).Exist() {
			dinfo = append(dinfo, "TecEng")
		}
		SystemEngs := []sqlmodel.SystemEng{}
		if _, err := o.QueryTable(&sqlmodel.SystemEng{}).Filter("Project", *in.ProjectId).Filter("User", u.Id).RelatedSel("Profession").All(&SystemEngs); err != nil {
			this.ErrLog(err)
			this.SetBackInfo(false, "服务器出错啦!", nil, err)
			return
		}
		if len(SystemEngs) > 0 {
			dinfo = append(dinfo, "SystemEng")
		}
		for _, v := range SystemEngs {
			dinfo = append(dinfo, "SystemEng__"+v.Profession.Name)
		}

		CdtEngs := []sqlmodel.CdtEng{}
		if _, err := o.QueryTable(&sqlmodel.CdtEng{}).Filter("Project", *in.ProjectId).Filter("User", u.Id).All(&CdtEngs); err != nil {
			this.ErrLog(err)
			this.SetBackInfo(false, "服务器出错啦!", nil, err)
			return
		}
		if len(CdtEngs) > 0 {
			dinfo = append(dinfo, "CdtEng")
		}
		for _, v := range CdtEngs {
			dinfo = append(dinfo, "CdtEng__"+v.EngName)
		}

	}
	this.BackInfo.Data = dinfo
	this.BackInfo.Status = true
}
