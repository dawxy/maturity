package PowerCT

import (
	"Maturity/models/CheckPower"
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
	"time"
)

type inputDelUserRole struct {
	Token      *string `maxlen:"22"`
	UserRoleId *uint32
}

// @Summary 删除用户的某个角色
// @Description
// @Param _ body controllers.PowerCT.inputDelUserRole true
// @Success 200 {object} string
// @router /DelUserRole/ [post]
func (this *PowerController) DelUserRole() {
	in := inputDelUserRole{}
	if err := this.GetInputStruct(&in); err != nil {
		return
	}

	if !CheckPower.CheckPower(this, *in.Token, CheckPower.DelUserRole) {
		return
	}
	o := orm.NewOrm()
	if err := o.Begin(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	defer o.Rollback()
	UserRole := sqlmodel.UserRole{Id: *in.UserRoleId}
	if err := o.Read(&UserRole, "Id"); err != nil {
		if err == orm.ErrNoRows {
			this.BackInfo.Message = "该用户不存在这个角色!"
			return
		}
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}

	if CheckPower.AdminUser.Id == UserRole.User.Id {
		this.BackInfo.Message = CheckPower.AdminUser.Name + "账号不能修改权限!"
		return
	}
	if _, err := o.LoadRelated(&UserRole, "User"); err != nil {
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}
	if _, err := o.LoadRelated(&UserRole, "Role"); err != nil {
		this.BackInfo.Message = "服务器出错啦!"
		this.ErrLog(err)
		return
	}

	if _, err := o.Delete(&UserRole, "Id"); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦~"
		return
	}
	//记录日志
	upu := this.GetUserInfo(*in.Token)
	sysl := sqlmodel.SysLogs{Type: "用户删除权限", Value: UserRole.Role.Name, TargetUserJobNum: UserRole.User.JobNumber, TargetUserJobName: UserRole.User.Name, UserName: upu.Name, UpJobNumber: upu.JobNumber, Ip: this.Ctx.Request.RemoteAddr, Time: time.Now()}
	if _, err := o.Insert(&sysl); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	if err := o.Commit(); err != nil {
		this.ErrLog(err)
		this.BackInfo.Message = "服务器出错啦!"
		return
	}
	this.BackInfo.Status = true
	this.BackInfo.Message = "成功"
}
