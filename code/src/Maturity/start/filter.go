package start

import (
	"flag"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"os"
	"path"
	"strings"
)

var (
	EnableX_FORWARDED = flag.Bool("EnableX_FORWARDED", false, "是否开启代理服务(影响ip获取)")
)

//设置过滤器
func SetFilter() {
	EnableHttps, err := beego.AppConfig.Bool("EnableHttps")
	if err != nil {
		panic(err)
	}
	//自定义的路由规则(检查是否需要渲染,需要渲染对应内部URL加上"view","static"则不用渲染,对外都直接是根目录)
	// /api不加前缀
	var FilterRouters = func(ctx *context.Context) {
		ctx.Output.Header("Server", beego.BConfig.AppName)
		if ctx.Input.SubDomains() == "" { //不加子域名重定向到www
			ctx.Redirect(301, "http://www."+ctx.Input.Host()+ctx.Input.URI())
			return
		}
		//http请求强制重定向到https
		if EnableHttps && !ctx.Input.IsSecure() {
			ctx.Redirect(301, "https://"+ctx.Input.Host()+ctx.Input.URI())
			return
		}
		if *EnableX_FORWARDED { //启动代理
			ips := ctx.Input.Proxy()
			if len(ips) > 0 && ips[0] != "" {
				ctx.Request.RemoteAddr = ips[0]
			}
		}
		if strings.HasPrefix(ctx.Request.URL.Path, "/api/") { //api目录不过滤
			if ctx.Request.Method == "OPTIONS" { //js复杂跨域请求的OPTIONS认证
				ctx.Output.Header("Access-Control-Allow-Origin", "*") //允许任何客户端访问
				//ctx.Output.Header("Access-Control-Allow-Credentials", "true")
				ctx.Output.Header("Access-Control-Allow-Methods", "POST")         //api允许使用跨域执行
				ctx.Output.Header("Access-Control-Max-Age", "1728000")            //跨域认证有效时间(之后这段时间内不会有这个请求了)
				ctx.Output.Header("Access-Control-Allow-Headers", "Content-Type") //api允许使用跨域执行
				ctx.ResponseWriter.Status = 200
				ctx.WriteString("")
			} else {
				ctx.Output.Header("Access-Control-Allow-Origin", "*") //api允许使用跨域执行
			}
			return
		}
		nowurl := ctx.Request.URL.Path
		nowurl = nowurl[strings.LastIndex(nowurl, "/")+1:]
		pre := "static"
		f, err := os.Stat(beego.BConfig.WebConfig.ViewsPath + ctx.Request.URL.Path)
		if err != nil || os.IsNotExist(err) || f == nil { //不存在这个文件或目录
			ctx.Output.SetStatus(404)
			return
		}
		if f.IsDir() { //是个目录
			if nowurl != "" { //最后不是'/',则加上'/'后跳转(不跳转比如docs下就会出错)
				toURL := path.Clean(ctx.Request.URL.Path) + "/"
				if ctx.Request.URL.RawQuery != "" { //加上请求参数
					toURL += "?" + ctx.Request.URL.RawQuery
				}
				ctx.Redirect(302, toURL)
				return
			}
			pre = "views"
			ctx.Request.URL.Path += "index.html"
			f, err = os.Stat(beego.BConfig.WebConfig.ViewsPath + ctx.Request.URL.Path)
			if err != nil || os.IsNotExist(err) || f == nil { //不存在这个文件或目录
				ctx.Output.SetStatus(404)
				return
			}
		} else if beego.HasTemplateExt(nowurl) { //是模版后缀,则渲染模版
			pre = "views"
		}

		ctx.Request.URL.Path = "/" + pre + ctx.Request.URL.Path
		ctx.Request.RequestURI = "/" + pre + ctx.Request.RequestURI
	}
	beego.InsertFilter("/*", beego.BeforeStatic, FilterRouters)
}
