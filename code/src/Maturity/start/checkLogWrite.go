package start

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"os"
)

type EmptyWriter struct{}

func (*EmptyWriter) Write(p []byte) (n int, err error) {
	return 0, nil
}
func CheckLogWrite() {
	os.Mkdir("static/upload", os.ModePerm)
	beego.BeeLogger.Async(1e3)
	//线上模式自定义日志输出到文件，否则输出到控制台
	if beego.AppConfig.String("runmode") == "prod" {
		os.Mkdir("log", os.ModePerm)
		err := logs.SetLogger(logs.AdapterFile, `{"filename":"log/logfile","daily":false}`)
		if err != nil {
			panic(err)
		}
		beego.BeeLogger.DelLogger("console")
		logs.SetLevel(logs.LevelInfo)
	}
	logs.Async(1e3)                                  //设置异步打印日志
	logs.GetLogger("HTTP").SetOutput(&EmptyWriter{}) //取消HTTP错误输出
}
