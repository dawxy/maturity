package start

import (
	"context"
	"github.com/astaxie/beego"
	"log"
	"time"
)

//收到关闭信号后执行
func ShutDown() {
	ctx, _ := context.WithTimeout(context.Background(), time.Second*30)
	if err := beego.BeeApp.Server.Shutdown(ctx); err != nil {
		log.Println("Server.Shutdown ERR", err)
	}
	log.Println("Server.Shutdown")
}
