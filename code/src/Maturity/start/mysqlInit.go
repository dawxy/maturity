package start

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"net/url"
	"time"
)

func MysqlInit() {
	DB_USERNAME := beego.AppConfig.String("DB_USERNAME")
	DB_PASSWORD := beego.AppConfig.String("DB_PASSWORD")
	DB_IP := beego.AppConfig.String("DB_IP")
	DB_PORT := beego.AppConfig.String("DB_PORT")
	DB_NAME := beego.AppConfig.String("DB_NAME")
	DB_MaxOpenConns, err := beego.AppConfig.Int("DB_MaxOpenConns")
	if err != nil {
		panic("DB_MaxOpenConns错误:" + err.Error())
	}
	DB_MaxIdleConns, err := beego.AppConfig.Int("DB_MaxIdleConns")
	if err != nil {
		panic("DB_MaxIdleConns错误:" + err.Error())
	}
	DB_MaxLifetime, err := beego.AppConfig.Int64("DB_MaxLifetime")
	if err != nil {
		panic("DB_MaxLifetime错误:" + err.Error())
	}
	if err = orm.RegisterDriver("mysql", orm.DRMySQL); err != nil {
		panic(err)
	}
	err = orm.RegisterDataBase("default", "mysql", DB_USERNAME+":"+DB_PASSWORD+"@tcp("+DB_IP+":"+DB_PORT+")/"+DB_NAME+"?charset=utf8&loc="+url.QueryEscape("Asia/Shanghai"), DB_MaxIdleConns, DB_MaxOpenConns)
	if err != nil {
		panic(err)
	}
	db, err := orm.GetDB()
	if err != nil {
		panic(err)
	}
	//设置连接池的最大打开连接限制和最大缓存数量,以及连接最大存活时间
	db.SetConnMaxLifetime(time.Duration(DB_MaxLifetime) * time.Second)
	//测试mysql是否能连通,open()只是配置并不会连接
	if err := db.Ping(); err != nil {
		logs.Error("Connect database error:", err)
	} else {
		logs.Info("mysql数据库(%s:%s@%s)连接成功!", DB_IP, DB_PORT, DB_NAME)
	}
	//创建数据库
	// 数据库别名
	name := "default"
	// drop table 后再建表
	force, err := beego.AppConfig.Bool("force")
	if err != nil {
		panic(err)
	}
	// 打印执行过程
	verbose := true
	// 遇到错误立即返回
	err = orm.RunSyncdb(name, force, verbose)
	if err != nil {
		panic(err)
	}
}
