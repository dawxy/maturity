package sqlmodel

import (
	"github.com/astaxie/beego/orm"
	"time"
)

func init() {
	//[注意]在这里注册的所有类型不能和自动文档生成的结构重名,否则会引用到这里注册的model
	orm.RegisterModel(new(User))
	orm.RegisterModel(new(Project))
	orm.RegisterModel(new(Department))
	orm.RegisterModel(new(Role))
	orm.RegisterModel(new(UserRole))
	orm.RegisterModel(new(Power))
	orm.RegisterModel(new(RolePower))
	orm.RegisterModel(new(Profession))
	orm.RegisterModel(new(QualityLevel))
	orm.RegisterModel(new(DevelopmentType))
	orm.RegisterModel(new(ArrowSystem))
	orm.RegisterModel(new(Part))
	orm.RegisterModel(new(SystemEng))
	orm.RegisterModel(new(SupplyStatus))
	orm.RegisterModel(new(TecEng))
	orm.RegisterModel(new(CdtEng))
	orm.RegisterModel(new(UpdateHistory))
	orm.RegisterModel(new(SysLogs))
	orm.RegisterModel(new(Cam0_1))
	orm.RegisterModel(new(Cam1_2))
	orm.RegisterModel(new(Cam2_3))
	orm.RegisterModel(new(Cam3_4))
	orm.RegisterModel(new(Cam4_5))
	orm.RegisterModel(new(Cam5_8))
}

const (
	TypeBool = iota
	TypeInt
	TypeString
	TypeEnum
)

type User struct {
	Id             uint32      `orm:"pk;auto;"`
	Token          string      `orm:"unique;size(22)"`
	JobNumber      string      `orm:"unique;size(8)"`
	Password       string      `orm:"size(128)"`
	Name           string      `index;orm:"size(64)"`
	Email          string      `orm:"null;index;size(128)"`
	Phone          string      `orm:"null;size(32)"`
	Department     *Department `orm:"index;rel(fk)"`
	RegTime        time.Time   `orm:"auto_now_add"`
	RegIP          string      `orm:"size(64)"`
	IsStop         bool        `orm:"default(false)"`
	IsInitPassword bool        `orm:"default(true)"` //是否为初始密码

	UserRoles []*UserRole `orm:"reverse(many)"` //对应的角色(使用LoadRelated默认最多显示1000)
}

//部门
type Department struct {
	Id        uint32      `orm:"pk;auto;"`
	Name      string      `orm:"unique;size(64)"`    //部门名称
	Father    *Department `orm:"null;index;rel(fk)"` //所属部门
	CreatTime time.Time   `orm:"auto_now_add"`
}

//角色
type Role struct {
	Id        uint32    `orm:"pk;auto;"`
	Name      string    `orm:"unique;size(64)"`
	CreatTime time.Time `orm:"auto_now_add"`

	RolePowers []*RolePower `orm:"reverse(many)"` //对应的权限(使用LoadRelated默认最多显示1000)
}

//权限
type Power struct {
	Id        uint32    `orm:"pk;auto;"`
	Name      string    //权限名称
	CreatTime time.Time `orm:"auto_now_add"`
}

//角色和权限的对应关系
type RolePower struct {
	Id        uint32    `orm:"pk;auto;"`
	Role      *Role     `orm:"index;rel(fk)"`
	Power     *Power    `orm:"index;rel(fk)"`
	CreatTime time.Time `orm:"auto_now_add"`
}

// 多字段唯一键
func (u *RolePower) TableUnique() [][]string {
	return [][]string{
		{"Role", "Power"},
	}
}

//用户拥有的角色
type UserRole struct {
	Id        uint32    `orm:"pk;auto;"`
	User      *User     `orm:"index;rel(fk)"`
	Role      *Role     `orm:"index;rel(fk)"`
	CreatUser *User     `orm:"null;index;rel(fk)"`
	CreatTime time.Time `orm:"auto_now_add"`
}

// 多字段唯一键
func (u *UserRole) TableUnique() [][]string {
	return [][]string{
		{"User", "Role"},
	}
}

type Project struct {
	Id      uint32    `orm:"pk;auto;"`
	Name    string    `orm:"index;size(64)"` //项目名称
	Code    string    `orm:"size(128)"`      //项目代码
	Admin   *User     `orm:"index;rel(fk)"`  //项目管理员
	Creator *User     `orm:"index;rel(fk)"`  //项目创建者
	RegTime time.Time `orm:"auto_now_add"`
}

//专业
type Profession struct {
	Id   uint32 `orm:"pk;auto;"`
	Name string `orm:"unique;size(64)"`
}

//质量等级
type QualityLevel struct {
	Id   uint32 `orm:"pk;auto;"`
	Name string `orm:"unique;size(64)"`
}

//开发类型
type DevelopmentType struct {
	Id   uint32 `orm:"pk;auto;"`
	Name string `orm:"unique;size(64)"`
}

//系统集成经理
type SystemEng struct {
	Id         uint32      `orm:"pk;auto;"`
	Project    *Project    `orm:"index;rel(fk)" desc:"所属项目"`
	User       *User       `orm:"index;rel(fk)" desc:"用户"`
	Profession *Profession `orm:"index;rel(fk)" desc:"专业"`
}

// 多字段唯一键
func (*SystemEng) TableUnique() [][]string {
	return [][]string{
		{"Project", "User", "Profession"},
	}
}

//技术集成工程师列表
type TecEng struct {
	Id      uint32   `orm:"pk;auto;"`
	User    *User    `orm:"index;rel(fk)" desc:"用户"`
	Project *Project `orm:"index;rel(fk)" desc:"项目"`
}

// 多字段唯一键
func (*TecEng) TableUnique() [][]string {
	return [][]string{
		{"User", "Project"},
	}
}

//协同开发小组管理员列表
type CdtEng struct {
	Id      uint32   `orm:"pk;auto;"`
	User    *User    `orm:"index;rel(fk)" desc:"用户"`
	EngName string   `orm:"index;size(63)" desc:"工程师种类"`
	Project *Project `orm:"index;rel(fk)" desc:"项目"`
}

// 多字段唯一键
func (*CdtEng) TableUnique() [][]string {
	return [][]string{
		{"User", "EngName", "Project"},
	}
}

//箭头系统
type ArrowSystem struct {
	Id   uint32 `orm:"pk;auto;"`
	Name string `orm:"unique;" desc:"名称"`
}

//零件
type Part struct {
	Id                       uint32           `orm:"pk;auto;"`
	Name                     string           `desc:"基础信息_零件名称"`
	PartCode                 string           `desc:"基础信息_零件号"`
	ArrowSystem              *ArrowSystem     `orm:"index;rel(fk)" desc:"基础信息_箭头系统"`
	SingleUseNum             string           `desc:"基础信息_单车用量"`
	QualityLevel             *QualityLevel    `orm:"null;index;rel(fk)" desc:"基础信息_质量特性等级"`
	DevelopmentType          *DevelopmentType `orm:"null;index;rel(fk)" desc:"基础信息_开发类型"`
	Configuration            string           `orm:"null" desc:"基础信息_配置信息"`
	Profession               *Profession      `orm:"index;rel(fk)" desc:"基础信息_所在设计专业"`
	PartSource               string           `orm:"null" desc:"基础信息_零部件来源"`
	SupplierName             string           `orm:"null" desc:"供应商_供应商名称"`
	SupplierCode             string           `orm:"null" desc:"供应商_供应商代码"`
	SupplierBusnes           string           `orm:"null" desc:"供应商_供应商商务"`
	SupplierTec              string           `orm:"null" desc:"供应商_供应商技术"`
	SupplierPhone            string           `orm:"null" desc:"供应商_供应商联系方式"`
	ChangePlanCompletionTime time.Time        `orm:"null" desc:"设变情况_设变计划完成时间"`
	ChangeCompletionTime     time.Time        `orm:"null" desc:"设变情况_实际完成时间"`
	Workshop                 string           `orm:"null" desc:"装配信息_车间"`
	Section                  string           `orm:"null" desc:"装配信息_工段"`
	Station                  string           `orm:"null" desc:"装配信息_工位"`

	TargetWeight    string `orm:"null" desc:"重量管理_目标重量"`
	EstimatedWeight string `orm:"null" desc:"重量管理_预估重量"`
	ActualWeight    string `orm:"null" desc:"重量管理_实际重量"`

	DesignerEng *User `orm:"index;rel(fk)" desc:"协同开发小组_设计工程师"`
	BuyerEng    *User `orm:"index;rel(fk)" desc:"协同开发小组_采购工程师"`
	SQEEng      *User `orm:"index;rel(fk)" desc:"协同开发小组_SQE工程师"`
	CrefftauEng *User `orm:"index;rel(fk)" desc:"协同开发小组_工艺工程师"`
	TestEng     *User `orm:"index;rel(fk)" desc:"协同开发小组_试验工程师"`
	SizeEng     *User `orm:"null;index;rel(fk)" desc:"协同开发小组_尺寸工程师"`
	NewqualityEng *User `orm:"null;index;rel(fk)" desc:"协同开发小组_新品质量工程师"`

	CreatTime time.Time `orm:"auto_now_add"`
	Project   *Project  `orm:"index;rel(fk)"`
	Deled     bool      `orm:"index;default(0)"`
}

// 多字段索引
func (*Part) TableIndex() [][]string {
	return [][]string{
		{"Id", "Deled"},
		{"PartCode", "Project"},
	}
}

//供货状态
type SupplyStatus struct {
	Id   uint32 `orm:"pk;auto;"`
	Name string
}

//零件修改历史记录
type UpdateHistory struct {
	Id          uint32 `orm:"pk;auto;"`
	ProjectId   uint32 `orm:"index" desc:"项目id"`
	ProjectName string `desc:"项目名称"`
	PartId      uint32 `orm:"index" desc:"零件id"`
	PartName    string `desc:"零件名称"`
	Type        int8   `orm:"index" desc:"记录操作类型,1-修改,2-删除等"`
	Table       string `orm:"index;size(15)" desc:"表名"`
	Field       string `orm:"index;size(63)" desc:"字段名称"`
	OldValue    string `orm:"size(511)" desc:"修改前的数据,格式:数据的string表示"`
	Value       string `orm:"size(511)" desc:"修改后的数据,格式:数据的string表示"`

	UpJobNumber string    `orm:"index;size(8)" desc:"修改人工号"`
	UpUser      string    `desc:"修改人姓名"`
	Ip          string    `orm:"size(63)" desc:"修改时的ip"`
	UpTime      time.Time `orm:"index;" desc:"修改时间"`
}

//系统日志
type SysLogs struct {
	Id                uint32 `orm:"pk;auto;"`
	Type              string `orm:"index;size(63)" desc:"记录操作类型"`
	ProjectId         uint32
	ProjectName       string
	TargetUserJobNum  string `orm:"size(8)" desc:"目标用户(被修改用户工号)"`
	TargetUserJobName string `desc:"目标用户(被修改用户名)"`
	Value             string `orm:"size(511)" desc:"操作后的数据,格式:数据的string表示"`

	UpJobNumber string    `orm:"index;size(8)" desc:"操作人工号"`
	UserName    string    `desc:"操作人姓名"`
	Ip          string    `orm:"size(63)" desc:"操作时的ip"`
	Time        time.Time `orm:"index;" desc:"操作时间"`
}

////阶段信息
//type Stage struct {
//	Id   uint32 `orm:"pk;auto;"`
//	Name string
//}
//
////字段组
//type FieldGroup struct {
//	Id   uint32 `orm:"pk;auto;"`
//	Name string
//}
//
////字段类型
//type FieldType struct {
//	Id   uint32 `orm:"pk;auto;"`
//	Name string `orm:"unique" `
//}
//
////零件字段信息
//type PartField struct {
//	Id         uint32      `orm:"pk;auto;"`
//	Name       string      `orm:"unique" `                              //字段名称
//	Stage      *Stage      `orm:"index;rel(fk)" `                       //所属阶段
//	UpRole     *[]Role     `orm:"rel(m2m);rel_table(part_field_role)" ` //可修改的权限
//	FieldGroup *FieldGroup `orm:"index;rel(fk)" `                       //字段组
//	FieldType  *FieldType  `orm:"index;rel(fk)" `                       //字段数据类型
//	Parent     *PartField  `orm:"index;rel(fk)" `                       //父亲结点(需要填写完父亲结点才能填写)
//}

/*
阶段TAG说明：
desc：中文描述，TAG_Name的格式，TAG是分类，Name是名称，界面和其他查询修改接口会根据这个格式生成。
needStatistics：标记需要统计的字段(会出现在统计页面内),needStatistics:""表示没有依赖，否则needStatistics:"field"，表示field为1时才会算上此字段
Int8List: 内容为一个数组，表示当前int8类型的字段所有可选的值，没有Int8List标记的int8类型字段默认是[0,1]
Show: 格式: field=val,表示当field=val是界面才显示为可修改
*/
//P0-1
type Cam0_1 struct {
	Part       *Part       `orm:"pk;unique;rel(fk)" `
	Deled      bool        `orm:"index;default(0)"`
	Profession *Profession `orm:"index;rel(fk)"`
	Project    *Project    `orm:"index;rel(fk)"`

	NeedSOR                             int8      `orm:"null" desc:"长周期关键零部件SOR/商务_是否需要SOR"`
	LongCycleType                       string    `orm:"null" desc:"长周期关键零部件SOR/商务_长周期件类型" Show:"NeedSOR=1"`
	SORPlanCompletion                   time.Time `orm:"null" desc:"长周期关键零部件SOR/商务_SOR计划完成时间" Show:"NeedSOR=1"`
	SORCode                             string    `orm:"null" desc:"长周期关键零部件SOR/商务_SOR编号" Show:"NeedSOR=1"`
	SORCompleted                        int8      `orm:"null" desc:"长周期关键零部件SOR/商务_SOR是否完成" Show:"NeedSOR=1"`
	SORCompletionTime                   time.Time `orm:"null" desc:"长周期关键零部件SOR/商务_SOR完成时间" Show:"NeedSOR=1"`
	NeedTargetCost                      int8      `orm:"null" desc:"目标成本_是否有目标成本"`
	TargetCostPlanCompletion            time.Time `orm:"null" desc:"目标成本_目标成本计划完成时间" Show:"NeedTargetCost=1"`
	CostAnalysisReportNumber            string    `orm:"null" desc:"目标成本_成本分析报告编号" Show:"NeedTargetCost=1"`
	TargetCostCompleted                 int8      `orm:"null" desc:"目标成本_目标成本是否完成" Show:"NeedTargetCost=1"`
	TargetCostCompletionTime            time.Time `orm:"null" desc:"目标成本_目标成本实际完成时间" Show:"NeedTargetCost=1"`
	NeedFeasibilityAnalysis             int8      `orm:"null" desc:"产品技术可行性分析_是否需要产品技术可行性分析"`
	FeasibilityAnalysisPlanCompletion   time.Time `orm:"null" desc:"产品技术可行性分析_产品技术可行性分析计划完成时间" Show:"NeedFeasibilityAnalysis=1"`
	FeasibilityAnalysisReportNumber     string    `orm:"null" desc:"产品技术可行性分析_产品技术可行性分析报告编号" Show:"NeedFeasibilityAnalysis=1"`
	FeasibilityAnalysisCompleted        int8      `orm:"null" desc:"产品技术可行性分析_是否完成产品技术可行性分析" Show:"NeedFeasibilityAnalysis=1"`
	FeasibilityAnalysisCompletionTime   time.Time `orm:"null" desc:"产品技术可行性分析_产品技术可行性分析完成时间" Show:"NeedFeasibilityAnalysis=1"`
	NeedComplianceAnalysis              int8      `orm:"null" desc:"第一轮法规符合性分析_是否需要法规符合性分析"`
	ComplianceAnalysisPlanCompletion    time.Time `orm:"null" desc:"第一轮法规符合性分析_法规符合性分析计划完成时间" Show:"NeedComplianceAnalysis=1"`
	ComplianceAnalysisReportNumber      string    `orm:"null" desc:"第一轮法规符合性分析_法规符合性分析报告编号" Show:"NeedComplianceAnalysis=1"`
	ComplianceAnalysisCompleted         int8      `orm:"null" desc:"第一轮法规符合性分析_法规符合性分析报告是否完成" Show:"NeedComplianceAnalysis=1"`
	ComplianceAnalysisCompletionTime    time.Time `orm:"null" desc:"第一轮法规符合性分析_法规符合性分析报告实际完成时间" Show:"NeedComplianceAnalysis=1"`
	DesignConceptBookPlanCompletionTime time.Time `orm:"null" desc:"设计构想书_设计构想书计划完成时间"`
	DesignConceptBookNumber             string    `orm:"null" desc:"设计构想书_设计构想书编号"`
	DesignConceptBookCompleted          int8      `orm:"null" desc:"设计构想书_设计构想书是否完成"`
	DesignConceptBookCompletionTime     time.Time `orm:"null" desc:"设计构想书_设计构想书完成时间"`
	NeedArrangeFeasibilityReport        int8      `orm:"null" desc:"M0数据_是否需要布置可行性报告"`
	ClothOrderPlanCompletion            time.Time `orm:"null" desc:"M0数据_布点单计划时间"`
	ClothOrderPlanCompleted             int8      `orm:"null" desc:"M0数据_布点单是否完成"`
	M0ReleasePlanCompletion             time.Time `orm:"null" desc:"M0数据_M0发布计划时间"`
	M0Version                           string    `orm:"null" desc:"M0数据_M0版本"`
	M0Release                           int8      `orm:"null" desc:"M0数据_M0是否发布"`
	M0CompletionTime                    time.Time `orm:"null" desc:"M0数据_M0发布实际时间"`
}

// 多字段索引
func (*Cam0_1) TableIndex() [][]string {
	return [][]string{
		{"Part", "Deled"},
	}
}

//P1-2
type Cam1_2 struct {
	Part       *Part       `orm:"pk;unique;rel(fk)" `
	Deled      bool        `orm:"index;default(0)"`
	Profession *Profession `orm:"index;rel(fk)"`
	Project    *Project    `orm:"index;rel(fk)"`

	ArrangementFrozen                      int8      `orm:"null" desc:"M1数据_布置方案是否冻结"`
	M1ReleasePlanCompletion                time.Time `orm:"null" desc:"M1数据_M1发布计划时间"`
	M1Version                              string    `orm:"null" desc:"M1数据_M1版本"`
	M1Release                              int8      `orm:"null" desc:"M1数据_M1是否发布"`
	M1CompletionTime                       time.Time `orm:"null" desc:"M1数据_M1发布实际时间"`
	CTSElectronicVersionPlanCompletionTime time.Time `orm:"null" desc:"CTS_电子版计划完成时间"`
	CTSNumber                              string    `orm:"null" desc:"CTS_CTS编号"`
	CTSElectronicVersionCompleted          int8      `orm:"index;null" desc:"CTS_电子版是否完成" needStatistics:""`
	CTSElectronicVersionCompletionTime     time.Time `orm:"null" desc:"CTS_电子版实际完成时间"`
	CTSSignaturePlanCompletionTime         time.Time `orm:"null" desc:"CTS_签字版计划完成时间"`
	CTSSignatureCompleted                  int8      `orm:"index;null" desc:"CTS_签字版是否完成" needStatistics:""`
	CTSSignatureCompletionTime             time.Time `orm:"null" desc:"CTS_签字版实际完成时间"`
	DecisionPlanCompletionTime             time.Time `orm:"null" desc:"决裁与商务协议_决裁计划完成时间"`
	DecisionBookNumber                     string    `orm:"null" desc:"决裁与商务协议_决裁书编号"`
	DecisionIsCompleted                    int8      `orm:"index;null" desc:"决裁与商务协议_决裁是否完成" needStatistics:""`
	DecisionCompletionTime                 time.Time `orm:"null" desc:"决裁与商务协议_决策完成时间"`
	BusinessAgreementCompleted             int8      `orm:"index;null" desc:"决裁与商务协议_商务协议完成" needStatistics:""`
	BusinessAgreementNumber                string    `orm:"null" desc:"决裁与商务协议_商务协议号"`
	BusinessAgreementFilingCompletionTime  time.Time `orm:"null" desc:"决裁与商务协议_商务协议归档完成时间"`
}

// 多字段索引
func (*Cam1_2) TableIndex() [][]string {
	return [][]string{
		{"Part", "Deled"},
	}
}

//P2-3
type Cam2_3 struct {
	Part       *Part       `orm:"pk;unique;rel(fk)" `
	Deled      bool        `orm:"index;default(0)"`
	Profession *Profession `orm:"index;rel(fk)"`
	Project    *Project    `orm:"index;rel(fk)"`

	TechnicalAgreementPlanCompletionTime             time.Time     `orm:"null" desc:"技术协议_技术协议计划完成时间"`
	TechnicalAgreementNumber                         string        `orm:"null" desc:"技术协议_技术协议号"`
	TechnicalAgreementCompleted                      int8          `orm:"index;null" desc:"技术协议_技术协议完成" needStatistics:""`
	TechnicalAgreementFilingCompletionTime           time.Time     `orm:"null" desc:"技术协议_技术协议归档完成时间"`
	DesignLift                                       string        `orm:"null" desc:"技术协议_设计寿命"`
	QualityAgreementPlanCompletionTime               time.Time     `orm:"null" desc:"质量协议_质量协议计划完成时间"`
	QualityNumber                                    string        `orm:"null" desc:"质量协议_质量协议编号"`
	QualityAgreementCompleted                        int8          `orm:"index;null" desc:"质量协议_质量协议完成" needStatistics:""`
	QualityAgreementFilingCompletionTime             time.Time     `orm:"null" desc:"质量协议_质量协议归档完成时间"`
	ThreePacksOfTime                                 string        `orm:"null" desc:"质量协议_三包期限"`
	IPTV                                             string        `orm:"null" desc:"质量协议_IPTV"`
	PPM                                              string        `orm:"null" desc:"质量协议_PPM"`
	DFMEAElectronicVersionPlanCompletionTime         time.Time     `orm:"null" desc:"DFMEA_电子版计划完成时间"`
	DFMEANumber                                      string        `orm:"null" desc:"DFMEA_DFMEA编号"`
	DFMEAlectronicVersionCompleted                   int8          `orm:"index;null" desc:"DFMEA_电子版是否完成" needStatistics:""`
	DFMEAElectronicVersionCompletionTime             time.Time     `orm:"null" desc:"DFMEA_电子版实际完成时间"`
	DFMEASignaturePlanCompletionTime                 time.Time     `orm:"null" desc:"DFMEA_签字版计划完成时间"`
	DFMEASignatureCompleted                          int8          `orm:"null" desc:"DFMEA_签字版是否完成"`
	DFMEASignatureCompletionTime                     time.Time     `orm:"null" desc:"DFMEA_签字版实际完成时间"`
	DVPElectronicVersionPlanCompletionTime           time.Time     `orm:"null" desc:"DVP_电子版计划完成时间"`
	DVPNumber                                        string        `orm:"null" desc:"DVP_DFMEA编号"`
	DVPlectronicVersionCompleted                     int8          `orm:"index;null" desc:"DVP_电子版是否完成" needStatistics:""`
	DVPElectronicVersionCompletionTime               time.Time     `orm:"null" desc:"DVP_电子版实际完成时间"`
	DVPSignaturePlanCompletionTime                   time.Time     `orm:"null" desc:"DVP_签字版计划完成时间"`
	DVPSignatureCompleted                            int8          `orm:"null" desc:"DVP_签字版是否完成"`
	DVPSignatureCompletionTime                       time.Time     `orm:"null" desc:"DVP_签字版实际完成时间"`
	NeedAssemblyAdjustmentInstructions               int8          `orm:"null" desc:"装配调整说明书_是否需要装配调整说明书"`
	AssemblyAdjustmentInstructionsPlanCompletionTime time.Time     `orm:"null" desc:"装配调整说明书_装配调整说明书计划完成时间" Show:"NeedAssemblyAdjustmentInstructions=1"`
	AssemblyAdjustmentInstructionsNumber             string        `orm:"null" desc:"装配调整说明书_装配调整说明书编号" Show:"NeedAssemblyAdjustmentInstructions=1"`
	AssemblyAdjustmentInstructionsCompleted          int8          `orm:"null" desc:"装配调整说明书_装配调整说明书是否完成" Show:"NeedAssemblyAdjustmentInstructions=1"`
	AssemblyAdjustmentInstructionsCompletionTime     time.Time     `orm:"null" desc:"装配调整说明书_装配调整说明书实际完成时间" Show:"NeedAssemblyAdjustmentInstructions=1"`
	DigitalMoldFreezingPlanCompletionTime            time.Time     `orm:"null" desc:"M2数据_P数模冻结计划完成时间"`
	ChecklistPlanCompletionTime                      time.Time     `orm:"null" desc:"M2数据_checklist计划完成时间"`
	ChecklistCompleted                               int8          `orm:"null" desc:"M2数据_checklist是否完成"`
	ChecklistCompletionTime                          time.Time     `orm:"null" desc:"M2数据_checklist实际完成时间"`
	DigitalMoldFreezing                              int8          `orm:"index;null" desc:"M2数据_P数模冻结" needStatistics:""`
	M2DataVersion                                    string        `orm:"null" desc:"M2数据_M2数据版本"`
	DigitalMoldFreezingCompletionTime                time.Time     `orm:"null" desc:"M2数据_P数据冻结实际完成时间"`
	TwoDPlanCompletionTime                           time.Time     `orm:"null" desc:"M2数据_2D计划完成时间"`
	TwoDNumber                                       string        `orm:"null" desc:"M2数据_2D图编号"`
	TwoDCompleted                                    int8          `orm:"index;null" desc:"M2数据_2D是否完成" needStatistics:""`
	TwoDCompletionTime                               time.Time     `orm:"null" desc:"M2数据_2D实际完成时间"`
	NeedGDT                                          int8          `orm:"null" desc:"第一轮GDT_是否需要GDT"`
	GDTPlanCompletionTime                            time.Time     `orm:"null" desc:"第一轮GDT_GDT计划完成时间" Show:"NeedGDT=1"`
	GDTNumber                                        string        `orm:"null" desc:"第一轮GDT_GDT编号" Show:"NeedGDT=1"`
	GDTCompleted                                     int8          `orm:"null" desc:"第一轮GDT_GDT是否完成" Show:"NeedGDT=1"`
	GDTCompletionTime                                time.Time     `orm:"null" desc:"第一轮GDT_GDT实际完成时间" Show:"NeedGDT=1"`
	SupplyStatus                                     *SupplyStatus `orm:"index;null;rel(fk)" desc:"首套件供货状态_供货状态"`
	FirstSamplePlanTime                              time.Time     `orm:"null" desc:"首次样件到件计划_计划到件时间"`
	FirstSampleCompleted                             int8          `orm:"null" desc:"首次样件到件计划_是否到件"`
	FirstSampleTime                                  time.Time     `orm:"null" desc:"首次样件到件计划_实际到件时间"`
	SampleProgress                                   string        `orm:"null" desc:"首次样件到件计划_进展描述"`
	NeedKPC                                          int8          `orm:"null" desc:"样件检测_是否需要KPC"`
	KPCPlanCompletionTime                            time.Time     `orm:"null" desc:"样件检测_KPC计划完成时间" Show:"NeedKPC=1"`
	KPCNumber                                        string        `orm:"null" desc:"样件检测_KPC编号" Show:"NeedKPC=1"`
	KPCCompleted                                     int8          `orm:"index;null" desc:"样件检测_KPC是否完成" needStatistics:"NeedKPC" Show:"NeedKPC=1"`
	KPCCompletionTime                                time.Time     `orm:"null" desc:"样件检测_KPC实际完成时间" Show:"NeedKPC=1"`
	DisposalMethod                                   string        `orm:"null" desc:"样件检测_检测方式"`
	TestResults                                      string        `orm:"null" desc:"样件检测_检测结果"`
	DvNeed                                           int8          `orm:"null" desc:"零部件DV试验_是否需要"`
	DVPlanCompletionTime                             time.Time     `orm:"null" desc:"零部件DV试验_DV计划完成时间" Show:"DvNeed=1"`
	DVNumber                                         string        `orm:"null" desc:"零部件DV试验_DV编号" Show:"DvNeed=1"`
	DvCompleted                                      int8          `orm:"index;null" desc:"零部件DV试验_DV是否完成" needStatistics:"DvNeed" Show:"DvNeed=1"`
	DVCompletionTime                                 time.Time     `orm:"null" desc:"零部件DV试验_DV实际完成时间" Show:"DvNeed=1"`
	AllCompletedPlanTime                             time.Time     `orm:"null" desc:"APQP一阶段_计划完成时间"`
	AllCompletedTime                                 time.Time     `orm:"null" desc:"APQP一阶段_实际完成时间"`
	MaturityState                                    int8          `orm:"null;default(-1)" desc:"APQP一阶段_审核结论" Int8List:"[-1,2,3,4]"`
}

// 多字段索引
func (*Cam2_3) TableIndex() [][]string {
	return [][]string{
		{"Part", "Deled"},
	}
}

//P3-4
type Cam3_4 struct {
	Part       *Part       `orm:"pk;unique;rel(fk)" `
	Deled      bool        `orm:"index;default(0)"`
	Profession *Profession `orm:"index;rel(fk)"`
	Project    *Project    `orm:"index;rel(fk)"`

	NeedPv                            int8      `orm:"null" desc:"零部件PV试验_是否需要"`
	PVPlanCompletionTime              time.Time `orm:"null" desc:"零部件PV试验_PV计划完成时间" Show:"NeedPv=1"`
	PVNumber                          string    `orm:"null" desc:"零部件PV试验_PV编号" Show:"NeedPv=1"`
	PvCompleted                       int8      `orm:"index;null" desc:"零部件PV试验_PV是否完成" needStatistics:"NeedPv" Show:"NeedPv=1"`
	PVCompletionTime                  time.Time `orm:"null" desc:"零部件PV试验_PV实际完成时间" Show:"NeedPv=1"`
	MeetTheRequirements               int8      `orm:"index;null;default(-1)" desc:"装车验证_试制验证是否满足要求" needStatistics:"" Int8List:"[-1,0,1]"`
	ProcessAnalysisFeasible           int8      `orm:"null;default(-1)" desc:"工艺可行性分析_工艺分析是否可行" Int8List:"[-1,0,1]"`
	ProcessAnalysisPlanCompletionTime time.Time `orm:"null" desc:"工艺可行性分析_计划完成时间"`
	ProcessAnalysisCompletionTime     time.Time `orm:"null" desc:"工艺可行性分析_实际完成时间"`
	M3PaymentDone                     int8      `orm:"index;null" desc:"M3发放_是否完成" needStatistics:""`
	ChecklistPlanCompletionTime       time.Time `orm:"null" desc:"M3发放_checklist计划完成时间"`
	ChecklistCompleted                int8      `orm:"null" desc:"M3发放_checklist是否完成"`
	ChecklistCompletionTime           time.Time `orm:"null" desc:"M3发放_checklist实际完成时间"`
	M3PlanCompletionTime              time.Time `orm:"null" desc:"M3发放_计划完成时间"`
	M3CompletionTime                  time.Time `orm:"null" desc:"M3发放_实际完成时间"`
	NeedGDT                           int8      `orm:"null" desc:"第二轮GDT_是否需要GDT"`
	GDTPlanCompletionTime             time.Time `orm:"null" desc:"第二轮GDT_GDT计划完成时间" Show:"NeedGDT=1"`
	GDTNumber                         string    `orm:"null" desc:"第二轮GDT_GDT编号" Show:"NeedGDT=1"`
	GDTCompleted                      int8      `orm:"null" desc:"第二轮GDT_GDT是否完成" Show:"NeedGDT=1"`
	GDTCompletionTime                 time.Time `orm:"null" desc:"第二轮GDT_GDT实际完成时间" Show:"NeedGDT=1"`
	AllCompletedPlanTime              time.Time `orm:"null" desc:"APQP二阶段_计划完成时间"`
	AllCompletedTime                  time.Time `orm:"null" desc:"APQP二阶段_实际完成时间"`
	MaturityState                     int8      `orm:"null;default(-1)" desc:"APQP二阶段_审核结论" Int8List:"[-1,2,3,4]"`
}

// 多字段索引
func (*Cam3_4) TableIndex() [][]string {
	return [][]string{
		{"Part", "Deled"},
	}
}

//P4-5
type Cam4_5 struct {
	Part       *Part       `orm:"pk;unique;rel(fk)" `
	Deled      bool        `orm:"index;default(0)"`
	Profession *Profession `orm:"index;rel(fk)"`
	Project    *Project    `orm:"index;rel(fk)"`

	BMapPlanCompletionTime                       time.Time `orm:"null" desc:"B图发放_B图完成计划时间"`
	BMapNumber                                   string    `orm:"null" desc:"B图发放_B图完成计划时间"`
	BMapPaymentDone                              int8      `orm:"index;null" desc:"B图发放_B图发放是否完成" needStatistics:""`
	BMapCompletionTime                           time.Time `orm:"null" desc:"B图发放_B图完成实际时间"`
	OpenModeCommand                              int8      `orm:"index;null" desc:"模具管理_开模指令是否下发" needStatistics:""`
	FirstModulePieces                            int8      `orm:"index;null" desc:"模具管理_首模件是否到件" needStatistics:""`
	FirstModulePiecesPlanCompletionTime          time.Time `orm:"null" desc:"模具管理_首模件计划到件时间"`
	NeedTestModel                                int8      `orm:"index;null" desc:"检具_是否需做检具" needStatistics:""`
	ProgramConfirmed                             int8      `orm:"index;null" desc:"检具_方案是否确认" needStatistics:"NeedTestModel" Show:"NeedTestModel=1"`
	TestModelcomplete                            int8      `orm:"index;null" desc:"检具_检具制作是否完成" needStatistics:"NeedTestModel" Show:"NeedTestModel=1"`
	TestModelPlanCompletionTime                  time.Time `orm:"null" desc:"检具_检具制作计划完成时间" Show:"NeedTestModel=1"`
	TestModelCompletionTime                      time.Time `orm:"null" desc:"检具_检具制作实际完成时间" Show:"NeedTestModel=1"`
	InspectionPlanTime                           time.Time `orm:"null" desc:"检具_检具验收计划完成时间" Show:"NeedTestModel=1"`
	InspectionTime                               time.Time `orm:"null" desc:"检具_检具验收实际完成时间" Show:"NeedTestModel=1"`
	AcceptanceResults                            int8      `orm:"index;null;default(-1)" desc:"检具_验收结果" needStatistics:"NeedTestModel" Int8List:"[-1,0,1]"  Show:"NeedTestModel=1"`
	SupplierSelfChecklistPlanCompletionTime      time.Time `orm:"null" desc:"供应商自查表_计划完成时间"`
	SupplierSelfChecklistCompleted               int8      `orm:"null" desc:"供应商自查表_是否完成"`
	SupplierSelfChecklistCompletionTime          time.Time `orm:"null" desc:"供应商自查表_实际完成时间"`
	OTSSamplingPlan                              time.Time `orm:"null" desc:"OTS取样_OTS取样计划"`
	OTSTestResult                                int8      `orm:"index;null;default(-1)" desc:"OTS取样_检测结果" needStatistics:"" Int8List:"[-1,0,1]"`
	OTSSamplingTime                              time.Time `orm:"null" desc:"OTS取样_实际取样时间"`
	NeedDimensionApproval                        int8      `orm:"null" desc:"尺寸认可_是否需要尺寸认可"`
	DimensionApprovalPlanCompletionTime          time.Time `orm:"null" desc:"尺寸认可_计划完成时间"  Show:"NeedDimensionApproval=1"`
	DimensionApprovalNumber                      string    `orm:"null" desc:"尺寸认可_尺寸认可报告编号" Show:"NeedDimensionApproval=1"`
	DimensionApprovalCompleted                   int8      `orm:"index;null" desc:"尺寸认可_是否完成" needStatistics:"NeedDimensionApproval" Show:"NeedDimensionApproval=1"`
	DimensionApprovalCompletionTime              time.Time `orm:"null" desc:"尺寸认可_实际完成时间" Show:"NeedDimensionApproval=1"`
	AssemblyApprovalPlanSendTime                 time.Time `orm:"null" desc:"装配性认可_计划送样时间"`
	AssemblyApprovalPlanCompletionTime           time.Time `orm:"null" desc:"装配性认可_计划完成时间"`
	AssemblyApprovalNumber                       time.Time `orm:"null" desc:"装配性认可_装配性认可报告编号"`
	AssemblyApprovalCompleted                    int8      `orm:"index;null" desc:"装配性认可_是否完成" needStatistics:""`
	AssemblyApprovalCompletionTime               time.Time `orm:"null" desc:"装配性认可_实际完成时间"`
	NeedExteriorApproval                         int8      `orm:"index;null" desc:"外观认可_是否外观认可件" needStatistics:""`
	ExteriorApprovalPlanSendTime                 time.Time `orm:"null" desc:"外观认可_计划送样时间" Show:"NeedExteriorApproval=1"`
	ExteriorApprovalPlanCompletionTime           time.Time `orm:"null" desc:"外观认可_认可计划完成时间" Show:"NeedExteriorApproval=1"`
	ExteriorApprovalNumber                       string    `orm:"null" desc:"外观认可_外观认可报告编号" Show:"NeedExteriorApproval=1"`
	ExteriorApprovalCompleted                    int8      `orm:"index;null" desc:"外观认可_是否完成" needStatistics:"NeedExteriorApproval" Show:"NeedExteriorApproval=1"`
	ExteriorApprovalCompletionTime               time.Time `orm:"null" desc:"外观认可_认可实际完成时间" Show:"NeedExteriorApproval=1"`
	NeedRegulatoryExteriorApproval               int8      `orm:"index;null" desc:"法规件外观标示认可_是否法规件" needStatistics:""`
	RegulatoryExteriorApprovalPlanCompletionTime time.Time `orm:"null" desc:"法规件外观标示认可_计划完成时间" Show:"NeedRegulatoryExteriorApproval=1"`
	RegulatoryExteriorApprovalNumber             string    `orm:"null" desc:"法规件外观标示认可_法规件外观标示认可报告编号" Show:"NeedRegulatoryExteriorApproval=1"`
	RegulatoryExteriorApprovalCompleted          int8      `orm:"index;null" desc:"法规件外观标示认可_法规件认可是否完成" needStatistics:"NeedRegulatoryExteriorApproval" Show:"NeedRegulatoryExteriorApproval=1"`
	RegulatoryExteriorApprovalCompletionTime     time.Time `orm:"null" desc:"法规件外观标示认可_实际完成时间" Show:"NeedRegulatoryExteriorApproval=1"`
	NeedBanned                                   int8      `orm:"index;null" desc:"禁用物质认可_是否第三方检测" needStatistics:""`
	BannedPlanSendTime                           time.Time `orm:"null" desc:"禁用物质认可_计划送报告/样时间"`
	BannedSendCompleted                          int8      `orm:"index;null" desc:"禁用物质认可_送报告/样是否完成" needStatistics:"NeedBanned"`
	BannedPlanCompletionTime                     time.Time `orm:"null" desc:"禁用物质认可_禁用物质认可计划完成时间"`
	BannedNumber                                 string    `orm:"null" desc:"禁用物质认可_禁用物质认可报告编号"`
	BannedCompleted                              int8      `orm:"index;null" desc:"禁用物质认可_是否完成认可" needStatistics:"NeedBanned"`
	BannedCompletionTime                         time.Time `orm:"null" desc:"禁用物质认可_禁用物质认可实际完成时间"`
	NeedMaterialTest                             int8      `orm:"index;null" desc:"材料试验_是否等效" needStatistics:""`
	MaterialTestPlanCompletionTime               time.Time `orm:"null" desc:"材料试验_计划完成时间" Show:"NeedMaterialTest=0"`
	MaterialTestNumber                           time.Time `orm:"null" desc:"材料试验_材料认试验告编号" Show:"NeedMaterialTest=0"`
	MaterialTestCompleted                        int8      `orm:"index;null" desc:"材料试验_是否完成" needStatistics:"NeedMaterialTest" Show:"NeedMaterialTest=0"`
	MaterialTestCompletionTime                   time.Time `orm:"null" desc:"材料试验_材料试验实际完成时间" Show:"NeedMaterialTest=0"`
	NeedPerformanceTest                          int8      `orm:"index;null" desc:"性能认可_是否进行性能认可" needStatistics:""`
	PerformanceTestPlanCompletionTime            time.Time `orm:"null" desc:"性能认可_计划认可完成时间" Show:"NeedPerformanceTest=1"`
	PerformanceTestNumber                        string    `orm:"null" desc:"性能认可_性能认可报告编号" Show:"NeedPerformanceTest=1"`
	PerformanceTestCompleted                     int8      `orm:"index;null" desc:"性能认可_性能认可是否完成" needStatistics:"NeedPerformanceTest" Show:"NeedPerformanceTest=1"`
	PerformanceTestCompletionTime                time.Time `orm:"null" desc:"性能认可_性能认可实际完成时间" Show:"NeedPerformanceTest=1"`
	NeedFirstRoadTest                            int8      `orm:"index;null" desc:"第1轮路试搭载_第1轮路试是否搭载" needStatistics:""`
	FirstRoadTestNumber                          string    `orm:"index;null" desc:"第1轮路试搭载_第1轮路试搭载报告编号" needStatistics:"" Show:"NeedFirstRoadTest=1"`
	FirstRoadTestResult                          int8      `orm:"index;null;default(-1)" desc:"第1轮路试搭载_搭载是否通过" needStatistics:"NeedFirstRoadTest" Int8List:"[-1,0,1]" Show:"NeedFirstRoadTest=1"`
	FirstRoadTestCompletionTime                  time.Time `orm:"index;null;default(-1)" desc:"第1轮路试搭载_搭载完成时间" Show:"NeedFirstRoadTest=1"`
	NeedSecondRoadTest                           int8      `orm:"index;null" desc:"第2轮路试搭载_第2轮路试是否搭载" needStatistics:""`
	SecondRoadTestNumber                         string    `orm:"index;null" desc:"第2轮路试搭载_第2轮路试搭载报告编号" needStatistics:"" Show:"NeedSecondRoadTest=1"`
	SecondRoadTestResult                         int8      `orm:"index;null;default(-1)" desc:"第2轮路试搭载_搭载是否通过" needStatistics:"NeedFirstRoadTest" Int8List:"[-1,0,1]" Show:"NeedSecondRoadTest=1"`
	SecondRoadTestCompletionTime                 time.Time `orm:"index;null;default(-1)" desc:"第2轮路试搭载_搭载完成时间" Show:"NeedSecondRoadTest=1"`
	NeedMB                                       int8      `orm:"index;null" desc:"MB匹配_是否MB件" needStatistics:""`
	MB1PlanCompletionTime                        time.Time `orm:"null" desc:"MB匹配_MB1计划完成时间" Show:"NeedMB=1"`
	MB1Number                                    string    `orm:"null" desc:"MB匹配_MB1报告编号" Show:"NeedMB=1"`
	MB1CompletionTime                            time.Time `orm:"null" desc:"MB匹配_MB1实际完成时间" Show:"NeedMB=1"`
	MB1PlanCompletion                            int8      `orm:"index;null" desc:"MB匹配_MB1是否完成" needStatistics:"NeedMB" Show:"NeedMB=1"`
	MB2PlanCompletionTime                        time.Time `orm:"null" desc:"MB匹配_MB2计划完成时间" Show:"NeedMB=1"`
	MB2Number                                    string    `orm:"null" desc:"MB匹配_MB2报告编号" Show:"NeedMB=1"`
	MB2CompletionTime                            time.Time `orm:"null" desc:"MB匹配_MB2实际完成时间" Show:"NeedMB=1"`
	MB2PlanCompletion                            int8      `orm:"index;null" desc:"MB匹配_MB2是否完成" needStatistics:"NeedMB" Show:"NeedMB=1"`
	MB3PlanCompletionTime                        time.Time `orm:"null" desc:"MB匹配_MB3计划完成时间" Show:"NeedMB=1"`
	MB3Number                                    string    `orm:"null" desc:"MB匹配_MB3报告编号" Show:"NeedMB=1"`
	MB3CompletionTime                            time.Time `orm:"null" desc:"MB匹配_MB3实际完成时间" Show:"NeedMB=1"`
	MB3PlanCompletion                            int8      `orm:"index;null" desc:"MB匹配_MB3是否完成" needStatistics:"NeedMB" Show:"NeedMB=1"`
	NeedOTS                                      int8      `orm:"index;null" desc:"OTS认可汇总_是否需要OTS认可" needStatistics:""`
	OTSPlanCompletionTime                        time.Time `orm:"null" desc:"OTS认可汇总_计划完成时间"`
	RecognizedForm                               int8      `orm:"null;default(-1)" desc:"OTS认可汇总_认可形式" Int8List:"[-1,5,6]"`
	OTSReportNo                                  string    `orm:"null" desc:"OTS认可汇总_报告编号"`
	OTSCompleted                                 int8      `orm:"index;null" desc:"OTS认可汇总_是否完成" needStatistics:"NeedOTS"`
	Archiving                                    int8      `orm:"index;null" desc:"OTS认可汇总_认可是否归档" needStatistics:"NeedOTS"`
	OTSCompletionTime                            time.Time `orm:"null" desc:"OTS认可汇总_实际完成时间"`
	AllCompletedPlanTime              time.Time `orm:"null" desc:"APQP三阶段_计划完成时间"`
	AllCompletedTime                  time.Time `orm:"null" desc:"APQP三阶段_实际完成时间"`
	MaturityState                     int8      `orm:"null;default(-1)" desc:"APQP三阶段_审核结论" Int8List:"[-1,2,3,4]"`
}

// 多字段索引
func (*Cam4_5) TableIndex() [][]string {
	return [][]string{
		{"Part", "Deled"},
	}
}

//P5-8
type Cam5_8 struct {
	Part       *Part       `orm:"pk;unique;rel(fk)" `
	Deled      bool        `orm:"index;default(0)"`
	Profession *Profession `orm:"index;rel(fk)"`
	Project    *Project    `orm:"index;rel(fk)"`

	TwoTPCompletedPlanTime              time.Time `orm:"null" desc:"2TP_计划完成时间"`
	TwoTPCompletedTime                  time.Time `orm:"null" desc:"2TP_实际完成时间"`
	PPAPCompletedPlanTime              time.Time `orm:"null" desc:"PPAP_计划完成时间"`
	PPAPCompletedTime                  time.Time `orm:"null" desc:"PPAP_实际完成时间"`
}

// 多字段索引
func (*Cam5_8) TableIndex() [][]string {
	return [][]string{
		{"Part", "Deled"},
	}
}