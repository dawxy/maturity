package userMD

//LoginModel 用户登录返回
type LoginModel struct {
	Token string
}

type QueryUserInfo struct {
	Id             uint32 `orm:"pk;auto;"`
	Token          string `orm:"unique;size(22)"`
	JobNumber      string `orm:"unique;size(8)"`
	Email          string
	Name           string `orm:"null;size(64)"`
	Phone          string `orm:"null;size(32)"`
	Department     string `orm:"index;rel(fk)"`
	DepartmentID   uint32
	RegTime        string `orm:"auto_now_add"`
	RegIP          string `orm:"size(64)"`
	IsStop         bool   `orm:"default(false)"`
	IsInitPassword bool
}

type UserElem struct {
	Id         uint32
	JobNumber  string
	Name       string
	Department string
	Phone      string
	IsStop     bool
}

type UserList struct {
	RecordsTotal int64
	Elems        []UserElem
}

type ResetPassword struct {
	NewPassword string
}

type RegModel struct {
	JobNumber string
	Password  string
}
