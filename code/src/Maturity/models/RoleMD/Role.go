package RoleMD

type RoleElem struct {
	Id   uint32
	Name string
}

type QueryRoleList struct {
	RecordsTotal int64
	Elems        []RoleElem
}

type PowerElem struct {
	Id   uint32
	Name string
}

type QueryRoleAllPower struct {
	RecordsTotal int64
	Elems        []PowerElem
}

type PowerInfo struct {
	Id        uint32
	RoleName  string
	CreatTime string
}
