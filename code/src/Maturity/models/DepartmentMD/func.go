package DepartmentMD

import (
	"Maturity/models/sqlmodel"
	"github.com/astaxie/beego/orm"
)

func HasDepartment(Id uint32) (ret bool, err error) {
	o := orm.NewOrm()
	d := sqlmodel.Department{}
	if err = o.QueryTable(&d).Filter("Id", Id).One(&d, "Id"); err != nil {
		if err == orm.ErrNoRows {
			err = nil
			return
		}
		return
	}
	ret = true
	return
}
