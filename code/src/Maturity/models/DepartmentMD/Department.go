package DepartmentMD

type DepartmentElem struct {
	Id        uint32
	Name      string
	Father    string
	ParentId  uint32
	CreatTime string
}

type QueryDepartmentList struct {
	RecordsTotal int64
	Elems        []DepartmentElem
}

type QueryDepartmentInfo struct {
	Id        uint32
	Name      string
	FatherID  uint32
	Father    string
	CreatTime string
}
