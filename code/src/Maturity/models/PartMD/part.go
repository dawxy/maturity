package PartMD

import (
	"github.com/astaxie/beego/orm"
)

type PartElem struct {
	Id              uint32
	PartCode        string //零件号
	Name            string //零件名称
	QualityLevel    string //质量等级
	DevelopmentType string //开发类型
	Profession      string //专业

	DesignerEng string
	BuyerEng    string
	SQEEng      string
	CrefftauEng string
	TestEng     string
}

type PartList struct {
	RecordsTotal int64
	Elems        []PartElem
}

type ProfessionElem struct {
	Id   uint32
	Name string
}

type QueryProfessionList struct {
	RecordsTotal int64
	Elems        []ProfessionElem
}

type QueryArrowSystemElem struct {
	Id   uint32
	Name string
}

type QueryArrowSystemList struct {
	RecordsTotal int64
	Elems        []QueryArrowSystemElem
}

type QueryConfigurationElem struct {
	Id   uint32
	Name string
}

type QueryConfigurationList struct {
	RecordsTotal int64
	Elems        []QueryConfigurationElem
}

type QueryQualityLevelElem struct {
	Id   uint32
	Name string
}

type QueryQualityLevelList struct {
	RecordsTotal int64
	Elems        []QueryQualityLevelElem
}

type QueryDevelopmentTypeElem struct {
	Id   uint32
	Name string
}

type QueryDevelopmentTypeList struct {
	RecordsTotal int64
	Elems        []QueryDevelopmentTypeElem
}

type QuerySupplyStatusElem struct {
	Id   uint32
	Name string
}

type QuerySupplyStatusList struct {
	RecordsTotal int64
	Elems        []QuerySupplyStatusElem
}
type QueryPart_Import struct {
	Name                     string //零件名称
	PartCode                 string //零件号
	ArrowSystem              string //箭头系统
	ArrowSystemId            uint32
	SingleUseNum             string //单车用量
	QualityLevel             string //质量特性等级
	QualityLevelId           uint32
	DevelopmentType          string //开发类型
	DevelopmentTypeId        uint32
	Configuration            string //配置类型
	ConfigurationId          uint32
	SupplierName             string //供应商名称
	SupplierCode             string //供应商代码
	SupplierBusnes           string //供应商商务
	SupplierTec              string //供应商技术
	PartSource               string
	SupplierPhone            string `orm:"null" desc:"供应商_供应商联系方式"`
	ChangePlanCompletionTime string `orm:"null" desc:"设变情况_设变计划完成时间"`
	ChangeCompletionTime     string `orm:"null" desc:"设变情况_实际完成时间"`
	Workshop                 string `orm:"null" desc:"装配信息_车间"`
	Section                  string `orm:"null" desc:"装配信息_工段"`
	Station                  string `orm:"null" desc:"装配信息_工位"`
	TargetWeight             string `orm:"null" desc:"重量管理_目标重量"`
	EstimatedWeight          string `orm:"null" desc:"重量管理_预估重量"`
	ActualWeight             string `orm:"null" desc:"重量管理_实际重量"`

	IntegreiddioTechnolegEngId        uint32 //技术集成工程师
	IntegreiddioTechnolegEng          string
	IntegreiddioTechnolegEngJobNumber string
	DesignerEngId                     uint32 //设计工程师
	DesignerEng                       string
	DesignerEngJobNumber              string
	BuyerEngId                        uint32 //采购工程师
	BuyerEng                          string
	BuyerEngJobNumber                 string
	SQEEngId                          uint32 //SQE工程师
	SQEEng                            string
	SQEEngJobNumber                   string
	CrefftauEngId                     uint32 //工艺工程师
	CrefftauEng                       string
	CrefftauEngJobNumber              string
	TestEngId                         uint32 //试验工程师
	TestEng                           string
	TestEngJobNumber                  string
	SizeEngId                         uint32 //尺寸工程师
	SizeEng                           string
	SizeEngJobNumber                  string
	NewqualityEngId                   uint32 //新品质量工程师
	NewqualityEng                     string
	NewqualityEngJobNumber            string

	CreatTime    string
	ProfessionId uint32 //所在设计专业
	Profession   string
	Deled        bool
}

type QueryPart_Base struct {
	Name              string
	PartCode          string
	QualityLevel      string //质量特性等级
	QualityLevelId    uint32
	DevelopmentType   string //开发类型
	DevelopmentTypeId uint32
	Configuration     string //配置类型
	SupplierName      string //供应商名称
	SupplierCode      string //供应商代码
	SupplierBusnes    string //供应商商务
	SupplierTec       string //供应商技术
}

type NotAllowFields struct {
	Field string
	UpEng string
}
type CheckUpFieldPower struct {
	Fields         []string
	NotAllowFields []NotAllowFields
}

type QueryPartUpdateHistoryElem struct {
	Id          uint32
	ProjectId   uint32 `orm:"index" desc:"项目id"`
	ProjectName string `desc:"项目名称"`
	PartId      uint32 `orm:"index" desc:"零件id"`
	PartName    string `desc:"零件名称"`
	Type        int8   `orm:"index" desc:"记录操作类型,修改,删除等"`
	TableName   string
	FieldName   string
	OldValue    string `orm:"size(511)" desc:"修改前的数据,格式:数据的string表示"`
	Value       string `orm:"size(511)" desc:"修改后的数据,格式:数据的string表示"`
	UpJobNumber string `desc:"修改人工号"`
	UpUser      string `desc:"修改人姓名"`
	Ip          string `orm:"size(63)" desc:"修改时的ip"`
	UpTime      string `desc:"修改时间"`
}

type QueryPartUpdateHistoryList struct {
	RecordsTotal int64
	Elems        []QueryPartUpdateHistoryElem
}

type QueryPartDynamicList struct {
	RecordsTotal int64
	Elems        []orm.Params
}
