package StatisticsMD

import (
	"github.com/astaxie/beego/orm"
)

type CompletionRate struct {
	Elems []orm.Params
}

type QuerySystemLogsElem struct {
	Id                uint32 `orm:"pk;auto;"`
	Type              string `orm:"index;size(63)" desc:"记录操作类型"`
	ProjectId         uint32
	ProjectName       string
	TargetUserJobNum  string `orm:"size(8)" desc:"目标用户(被修改用户工号)"`
	TargetUserJobName string
	Value             string `orm:"size(511)" desc:"操作后的数据,格式:数据的string表示"`
	UpJobNumber       string `orm:"index;size(8)" desc:"操作人工号"`
	UserName          string `desc:"操作人姓名"`
	Ip                string `orm:"size(63)" desc:"操作时的ip"`
	Time              string `orm:"index;" desc:"操作时间"`
}

type QuerySystemLogs struct {
	RecordsTotal int64
	Elems        []QuerySystemLogsElem
}
