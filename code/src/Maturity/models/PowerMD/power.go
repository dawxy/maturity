package PowerMD

type QueryPowerElem struct {
	Id        uint32
	UserId    uint32
	JobNumber string
	UserName  string
	RoleId    uint32
	RoleName  string
	CreatUser string
	CreatTime string
}

type QueryPowerList struct {
	RecordsTotal int64
	Elems        []QueryPowerElem
}
