package CheckPower

import (
	"Maturity/models/sqlmodel"
)

type regPower struct {
	Id   *uint32
	Name string
}

var (
	SystemAdmin         *sqlmodel.Role                                     //系统管理员角色(拥有所有权限的角色,不能修改)
	AdminUser           = sqlmodel.User{JobNumber: "admin", Name: "系统管理员"} //admin账号(最大权限,必须拥有SystemAdmin权限)
	RootDepartmentName  = "浙江合众新能源汽车"                                      //根部门名称(不能修改)
	powerNameToModel    = make(map[string]*regPower)                       //每个权限字段的string类型映射到对应的结构
	allPower            = make(map[*regPower]bool)
	RegUser             = &regPower{Name: "注册用户"}
	AddDepartment       = &regPower{Name: "添加部门"}
	UpdateInfo          = &regPower{Name: "修改所有人的用户信息"}
	QueryUserInfo       = &regPower{Name: "查询任何人的用户信息"}
	QueryUserList       = &regPower{Name: "查询用户列表"}
	ResetPassword       = &regPower{Name: "重置用户密码"}
	RoleOrPowerList     = &regPower{Name: "查询角色以及对应权限列表"}
	AddRoleOrPower      = &regPower{Name: "添加角色以及对应的权限"}
	UpdateRole          = &regPower{Name: "修改角色信息"}
	DelRoleOrPower      = &regPower{Name: "删除角色以及对应的权限"}
	QueryDepartmentList = &regPower{Name: "查询部门信息"}
	UpdateDepartment    = &regPower{Name: "修改部门列表"}
	DelDepartment       = &regPower{Name: "删除部门"}
	QueryPowerList      = &regPower{Name: "查询权限列表"}
	AddUserRole         = &regPower{Name: "给用户添加角色"}
	DelUserRole         = &regPower{Name: "删除用户的某个角色"}
	AddProject          = &regPower{Name: "添加项目"}
	DelProject          = &regPower{Name: "删除项目"}
	UpdateProject       = &regPower{Name: "修改项目信息"}
	QuerySysLogs        = &regPower{Name: "查询系统日志"}
)

func init() {
	allPower[RegUser] = true
	allPower[AddDepartment] = true
	allPower[AddProject] = true
	allPower[DelProject] = true
	allPower[UpdateInfo] = true
	allPower[QueryUserInfo] = true
	allPower[QueryUserList] = true
	allPower[ResetPassword] = true
	allPower[RoleOrPowerList] = true
	allPower[AddRoleOrPower] = true
	allPower[DelRoleOrPower] = true
	allPower[QueryDepartmentList] = true
	allPower[UpdateDepartment] = true
	allPower[DelDepartment] = true
	allPower[QueryPowerList] = true
	allPower[AddUserRole] = true
	allPower[DelUserRole] = true
	allPower[UpdateRole] = true
	allPower[UpdateProject] = true
	allPower[QuerySysLogs] = true

	for k := range allPower { //映射string->model
		//logs.Debug(k.Name)
		powerNameToModel[k.Name] = k
	}
}
