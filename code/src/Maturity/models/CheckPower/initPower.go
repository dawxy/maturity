package CheckPower

import (
	"Maturity/controllers"
	"Maturity/models/sqlmodel"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
)

func roleAddPower(role *sqlmodel.Role, power *regPower) {
	o := orm.NewOrm()
	if _, _, err := o.ReadOrCreate(&sqlmodel.RolePower{Role: role, Power: &sqlmodel.Power{Id: *power.Id}}, "Role", "Power"); err != nil {
		panic(err)
	}
}
func InitPower() {
	o := orm.NewOrm()
	//注册所有权限列表
	for k := range allPower {
		p := &sqlmodel.Power{Name: k.Name}
		if _, _, err := o.ReadOrCreate(p, "Name"); err != nil {
			panic(err)
		}
		k.Id = &p.Id
	}

	//系统管理员(拥有所有权限)
	SystemAdmin = &sqlmodel.Role{Id: 1, Name: "系统管理员"}
	if _, _, err := o.ReadOrCreate(SystemAdmin, "Name"); err != nil {
		panic(err)
	}
	for k := range allPower {
		roleAddPower(SystemAdmin, k)
	}
	//初始化各个工程师角色
	engRoleNames := []string{"设计工程师", "采购工程师", "SQE工程师", "工艺工程师", "试验工程师", "尺寸工程师", "新品质量工程师"}
	for i, v := range engRoleNames {
		if _, _, err := o.ReadOrCreate(&sqlmodel.Role{Id: uint32(i + 2), Name: v}, "Id", "Name"); err != nil {
			panic(err)
		}
	}

	//初始化根部门
	rootDepartment := sqlmodel.Department{Name: RootDepartmentName}
	if _, _, err := o.ReadOrCreate(&rootDepartment, "Name"); err != nil {
		panic(err)
	}
	//初始化admnin账号
	AdminUser.Token = controllers.UUID22()
	AdminUser.Department = &rootDepartment
	AdminUser.Password = userMD.GetUserPassword(AdminUser.Token, "123456")
	if _, _, err := o.ReadOrCreate(&AdminUser, "JobNumber"); err != nil {
		panic(err)
	}
	//给admin系统管理员权限
	if _, _, err := o.ReadOrCreate(&sqlmodel.UserRole{User: &AdminUser, Role: SystemAdmin}, "User", "Role"); err != nil {
		panic(err)
	}

	//初始化零件所属专业列表
	ProfessionName := []string{"底盘", "电器", "车身", "系统集成", "内外饰"}
	for _, v := range ProfessionName {
		if _, _, err := o.ReadOrCreate(&sqlmodel.Profession{Name: v}, "Name"); err != nil {
			panic(err)
		}
	}
	//初始化零件质量等级
	QualityLevelName := []string{"关键件", "重要件", "一般件"}
	for _, v := range QualityLevelName {
		if _, _, err := o.ReadOrCreate(&sqlmodel.QualityLevel{Name: v}, "Name"); err != nil {
			panic(err)
		}
	}
	//初始化开发类型
	DevelopmentTypeName := []string{"新开发", "修改件", "借用件", "沿用件"}
	for _, v := range DevelopmentTypeName {
		if _, _, err := o.ReadOrCreate(&sqlmodel.DevelopmentType{Name: v}, "Name"); err != nil {
			panic(err)
		}
	}
	//初始化供货状态
	SupplyStatusName := []string{"手工件", "快速样件", "硅胶模", "软模件", "半工装", "全序工装", "OTS件"}
	for _, v := range SupplyStatusName {
		if _, _, err := o.ReadOrCreate(&sqlmodel.SupplyStatus{Name: v}, "Name"); err != nil {
			panic(err)
		}
	}
	//初始化箭头系统
	ArrowSystemName := []string{"2Y", "3Y", "4Y", "2", "3", "5Y", "1", "1Y", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "6Y", "7Y", "8Y", "9Y", "10Y", "11Y", "12Y", "13Y", "14Y", "15Y", "16Y"}
	for _, v := range ArrowSystemName {
		if _, _, err := o.ReadOrCreate(&sqlmodel.ArrowSystem{Name: v}, "Name"); err != nil {
			panic(err)
		}
	}
	////初始化配置类型
	//ConfigurationName := []string{"舒适型", "豪华型", "舒适型与豪华型", "选配"}
	//for _, v := range ConfigurationName {
	//	if _, _, err := o.ReadOrCreate(&sqlmodel.Configuration{Name: v}, "Name"); err != nil {
	//		panic(err)
	//	}
	//}

}
