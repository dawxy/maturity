package CheckPower

import (
	"Maturity/controllers"
	"Maturity/models/userMD"
	"github.com/astaxie/beego/orm"
)

func CheckPowerByString(this controllers.JSONOutPutInterface, token string, mpower ...string) bool {
	cnt := len(mpower)
	if cnt < 1 {
		this.SetBackInfo(false, "请至少输入一项权限", nil)
		return false
	}
	power := mpower[0]
	_power, ok := powerNameToModel[power]
	if !ok {
		this.SetBackInfo(false, "不存在的权限:"+power, nil)
		return false
	}
	if cnt > 1 {
		var _mpower []*regPower
		for i := 1; i < cnt; i++ {
			v := mpower[i]
			p, ok := powerNameToModel[v]
			if !ok {
				this.SetBackInfo(false, "不存在的权限:"+v, nil)
				return false
			}
			_mpower = append(_mpower, p)
		}
		return CheckPower(this, token, _power, _mpower...)
	}
	return CheckPower(this, token, _power)
}

//检查用户是否有power之一的权限(power为空则只有系统管理员权限)
func CheckPower(this controllers.JSONOutPutInterface, token string, power *regPower, mpower ...*regPower) (ok bool) {
	u, err := userMD.GetUser(token)
	if err != nil {
		this.SetBackInfo(false, "服务器出错啦!", nil, err)
		return
	}
	if u == nil {
		this.SetBackInfo(false, "您没有权限!", nil)
		return
	}
	this.SetUserInfo(token, u)
	o := orm.NewOrm()
	o.LoadRelated(u, "UserRoles")
	hasPower := make(map[uint32]bool)
	if power.Id == nil {
		panic("权限" + power.Name + "还未注册")
	}
	hasPower[*power.Id] = true
	for _, p := range mpower {
		if p.Id == nil {
			panic("权限" + p.Name + "还未注册")
		}
		hasPower[*p.Id] = true
	}
	for _, v := range u.UserRoles {
		o.LoadRelated(v.Role, "RolePowers")
		for _, p := range v.Role.RolePowers {
			if hasPower[p.Power.Id] {
				this.SetUserInfo(u.Token, u)
				return true
			}
		}
	}
	this.SetBackInfo(false, "您没有权限!", nil)
	return
}
