package projectMD

type ProjectElem struct {
	Id      uint32
	Name    string
	Code    string
	Admin   string
	RegTime string
}

type ProjectList struct {
	RecordsTotal int64
	Elems        []ProjectElem
}

type ProjectInfo struct {
	Id             uint32
	Name           string
	Code           string
	AdminId        uint32
	Admin          string
	AdminJobNumber string
	RegTime        string
}

type QuerySystemEngListElem struct {
	Id         uint32
	Name       string
	JobNumber  string
	Profession string
}

type QuerySystemEngList struct {
	RecordsTotal int64
	Elems        []QuerySystemEngListElem
}

type QueryTecEngListElem struct {
	Id        uint32
	Name      string
	JobNumber string
	Project   string
}

type QueryTecEngList struct {
	RecordsTotal int64
	Elems        []QueryTecEngListElem
}

type QueryCdtEngListElem struct {
	Id        uint32
	Name      string
	JobNumber string
	EngName   string
}

type QueryCdtEngList struct {
	RecordsTotal int64
	Elems        []QueryCdtEngListElem
}
