// @APIVersion 0.1.0
// @Title 成熟度表项目-API接口
// @Description 成熟度表项目
// @Contact 97687341@qq.com
package routers

import (
	"Maturity/controllers"
	"Maturity/controllers/DepartmentCT"
	"Maturity/controllers/PowerCT"
	"Maturity/controllers/ProjectCT"
	"Maturity/controllers/RoleCT"
	"Maturity/controllers/StatisticsCT"
	"Maturity/controllers/partCT"
	"Maturity/controllers/userCT"
	"github.com/astaxie/beego"
)

func init() {
	beego.DelStaticPath("static")
	beego.Router("/views/*", &controllers.ViewsController{})          //需要渲染的页面
	beego.SetStaticPath("/static", beego.BConfig.WebConfig.ViewsPath) //注册静态页面
	ns :=
		beego.NewNamespace("/api",
			beego.NSNamespace("/User", //用户相关接口
				beego.NSInclude(
					&userCT.UserController{},
				),
			),
			beego.NSNamespace("/Project", //项目相关接口
				beego.NSInclude(
					&ProjectCT.ProjectController{},
				),
			),
			beego.NSNamespace("/Department",
				beego.NSInclude(
					&DepartmentCT.DepartmentController{},
				),
			),
			beego.NSNamespace("/Role",
				beego.NSInclude(
					&RoleCT.RoleController{},
				),
			),
			beego.NSNamespace("/Power",
				beego.NSInclude(
					&PowerCT.PowerController{},
				),
			),
			beego.NSNamespace("/Part",
				beego.NSInclude(
					&partCT.PartController{},
				),
			),
			beego.NSNamespace("/Statistics",
				beego.NSInclude(
					&StatisticsCT.StatisticsController{},
				),
			),
		)
	beego.AddNamespace(ns)
}
